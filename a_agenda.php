<?php include 'head.php'; ?>
<link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css" />
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <!-- BEGIN HEADER -->
    <?php include 'nav-top.php'; ?>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php include 'nav-left.php'; ?>
        <!-- END SIDEBAR -->
        <?php
        include 'conexao/config.php';

        if (isset($_POST['agd_id'])) {
            $agd_id =  utf8_decode($_POST['agd_id']);
        }else{
            $agd_id = '';
        }



        $query = "SELECT * FROM agendamento where agd_id = '$agd_id'";
        $result = mysqli_query($conn, $query);
        $row = mysqli_fetch_array($result);

        $queryPac = "SELECT * FROM paciente where pac_status = 'A' order by pac_nome asc";
        $resultPac = mysqli_query($conn, $queryPac);

        $queryProf = "SELECT * FROM profissional  where prof_tipo = 0 order by prof_nome asc";
        $resultProf = mysqli_query($conn, $queryProf);

        ?>
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE BAR -->
                <?php include 'breadcrumb.php'; ?>
                <!-- END PAGE BAR -->
                <!-- BEGIN PAGE TITLE-->
                <h3 class="page-title"> AGENDAMENTO DE PACIENTES
                </h3>
                <!-- END PAGE TITLE-->
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h3>Geral</h3> <br>

                        <form class="horizontal-form" id="form_sample_1" method="POST" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="nome" class="control-label"><b>Paciente</b></label>
                                <div class="input-group select2-bootstrap-prepend">
                                    <select id="agd_id_pac" class="form-control select2" name="agd_id_pac" disabled>
                                        <option disabled> -- Informe o nome do paciente -- </option>
                                        <?php while($rowPac = mysqli_fetch_array($resultPac)){ ?>
                                            <option value="<?php echo $rowPac['pac_id']; ?>"
                                             <?php if($row['agd_id_pac'] == $rowPac['pac_id']){echo 'selected="selected"';} ?>>
                                             <?php echo utf8_encode($rowPac['pac_nome']); ?>
                                         </option>    
                                         <?php } ?>                            
                                     </select>
                               <!--  <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-select2-open="nome">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span> -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><b>Celular</b></label>
                                    <input type="text" name="agd_tel_celulcar" id="agd_tel_celulcar" class="form-control mask_phone" placeholder="Infome o celular do paciente" value="<?php echo $row['agd_tel_celulcar']; ?>">
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><b>Residencial</b></label>
                                    <input type="text" id="agd_tel_residencia" name="agd_tel_residencia" class="form-control mask_tel" placeholder="Infome o telefone do paciente" value="<?php echo $row['agd_tel_residencia']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="convenio" class="control-label"><b>Cônvenio</b></label>
                                    <div class="input-group select2-bootstrap-prepend">
                                        <select id="convenio" class="form-control select2" name="agd_id_convenio">
                                            <option disabled selected> -- Informe o cônvenio do paciente -- </option>
                                        </select>
                                        <!-- <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" data-select2-open="convenio">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>
                                        </span> -->
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="plano" class="control-label"><b>Plano</b></label>
                                    <div class="input-group select2-bootstrap-prepend">
                                        <select id="plano" class="form-control select2" name="agd_id_plano">
                                            <option disabled selected> -- Informe o plano do paciente -- </option>
                                        </select>
                                       <!--  <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" data-select2-open="plano">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>
                                        </span> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nome" class="control-label"><b>Profissional de saúde</b></label>
                            <div class="input-group select2-bootstrap-prepend">
                                <select id="agd_id_prof" class="form-control select2" name="agd_id_prof" >
                                    <option disabled selected> -- Informe o Profissional para o agendamento -- </option>
                                    <?php while($rowProf = mysqli_fetch_array($resultProf)){ ?>
                                        <option value="<?php echo $rowProf['prof_id'] ?>"
                                            <?php if($row['agd_id_prof'] == $rowProf['prof_id']){echo 'selected="selected"';} ?>>
                                            <?php echo $rowProf['prof_nome'] ?>
                                        </option>
                                        <?php } ?>
                                    </select>
                               <!--  <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-select2-open="nome">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span> -->
                            </div>
                        </div>
                        <div class="note note-success">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><b>Data</b></label>
                                        <div class="input-group date dateAgenda">
                                            <input type="text" readonly name="agd_data" class="form-control dateAgenda" value="<?php echo $data = implode("/",array_reverse(explode("-",$row['agd_data']))); ?>">
                                            <span class="input-group-btn">
                                                <button class="btn default date-set" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                            <!-- /input-group -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><b>Horário inicial</b></label>
                                        <div class="input-group">
                                            <input type="text"  readonly name="agd_hora_inicial" class="form-control timepicker timepicker-24" value="<?php echo $row['agd_hora_inicial']; ?>">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-clock-o"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><b>Horário final</b></label>
                                        <div class="input-group">
                                            <input type="text" readonly name="agd_hora_final" class="form-control timepicker timepicker-24final" value="<?php echo $row['agd_hora_final']; ?>">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-clock-o"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label"><b>Observações</b></label>
                                    <textarea class="form-control" rows="4" name="agd_obs"><?php echo $row['agd_obs']; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="tipoForm" value="I"> 
                        <input type="hidden" name="agd_status" value="R"> 
                        <input type="hidden" name="agd_id_remarcada" value="<?php echo $agd_id; ?>"> 
                        <input type="hidden" name="agd_id_pac" value="<?php echo $row['agd_id_pac']; ?>"> 
                        <input type="hidden" name="agd_id" value="<?php echo $agd_id; ?>"> 

                    </form>
                    <div class="form-actions right">
                        <button type="button" class="btn default" onclick="concluido()">Cancelar</button>
                        <button type="submit" class="btn blue" onclick="enviarForm();">
                            <i class="fa fa-check"></i> Salvar</button>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->

                    <!-- Modal -->
                    <div class="modal fade" id="Mcadastrado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">
                                AGENDAMENTO DE PACIENTES  
                            </h4>
                        </div>
                        <div class="modal-body" align="center">
                            <h2>Agendamento marcado com sucesso !</h2>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn green" id="irPerfil"">Pefil Paciente</button>  
                            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="concluido()">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include 'footer.php'; ?>
<script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="assets/apps/scripts/form-input-mask.js" type="text/javascript"></script>
<script src="assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="js/calendario/moment.js" type="text/javascript"></script>
<script src="js/calendario/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="js/calendario/traducao.js" type="text/javascript"></script>
<script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<script src="assets/apps/scripts/traducao.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="js/model/agendamento.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {

     $(".nav-item").removeClass('start active open');
     $("#m_agenda").addClass('start active open');
     $(".dateAgenda").datetimepicker({
        language:'pt-br',
        pickTime: false
    });
     $('.timepicker-24final').timepicker({
        autoclose: true,
        minuteStep: 30,
        showSeconds: false,
        showMeridian: false
    });

     $('#agd_id_pac').change(function(){
        $('#agd_tel_celulcar').val('');
        $('#agd_tel_residencia').val('');

        var id_pac = $(this).val();

        $.ajax({

            url: "includes/carregar_campos_agd.php",
            type: "POST",
            data: {pac_id:id_pac},
            success: function(data) {
                var data = $.parseJSON(data);

                $('#agd_tel_celulcar').val(data[0].pac_tel_celular)
                $('#agd_tel_residencia').val(data[0].pac_tel_residencia)

            },
            error: function(data) {
                console.log(data + 'Erro')
            }, 
            beforeSend: function () {                       
            }
        });
    });

     $( "#irPerfil" ).on( "click", function() {
        window.location.href = "perfil_paciente.php?paciente=<?php echo $row['agd_id_pac'] ?>";
     });

 });

    function enviarForm(){
        $('#form_sample_1').submit();
    }
    function concluido() {
        window.location.href = "p_agenda.php";
    }
</script>
</body>
</html>