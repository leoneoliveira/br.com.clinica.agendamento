<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
           
      <div class="menu-toggler sidebar-toggler"> </div>
  </div>
  <!-- END LOGO -->
  <!-- BEGIN RESPONSIVE MENU TOGGLER -->
  <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
  <!-- END RESPONSIVE MENU TOGGLER -->
  <!-- BEGIN TOP NAVIGATION MENU -->
  <div class="top-menu">
    <ul class="nav navbar-nav pull-right">
        <!-- BEGIN NOTIFICATION DROPDOWN -->
        <?php if ($_SESSION['prof_img'] == null OR $_SESSION['prof_img'] == "") {
            $img = 'assets/pages/img/avatars/male.png';
        } else {
            $img = 'uploads_foto_perfil/' . $_SESSION['prof_img'];
        }                                                    
        ?> 
        <!-- BEGIN USER LOGIN DROPDOWN -->
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
        <li class="dropdown dropdown-user">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <img alt="" class="img-circle" src="<?php echo $img; ?>" />
                <span class="username username-hide-on-mobile"> <?php echo $_SESSION["prof_nome"]; ?> </span>
                <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
                <li>
                    <a href="a_profissional_saude.php?prof_id=<?php echo $_SESSION['prof_id']; ?>">
                        <i class="icon-user"></i> Perfil </a>
                    </li>
                    <li>
                        <a href="app_calendar.html">
                            <i class="icon-calendar"></i> Agenda </a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="destroy.php">
                                <i class="icon-key"></i> Sair </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END HEADER INNER -->
    </div>