<div class="tab-pane" id="tab_1_3">
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="portlet box blue-chambray">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-calendar"></i> Histórico de consultas do paciente
                        </div>
                        <div class="tools">
                            <button class="btn btn-xs default" type="button" data-toggle="modal" data-target="#atenderProntAgd">
                            <i class="fa fa-plus-circle"></i>
                            &nbsp;<b>CRIAR PRONTUÁRIO</b>
                            </button>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="panel-group accordion" id="accordion3">
                            <?php $cont = 1;
                            while($rowLaudo = mysqli_fetch_array($resultLaudo)){ ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                    <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#accordion1_<?php echo $cont; ?>"><b> <?php echo $cont; ?>.
                                        <?php
                                        echo  $data = implode("/",array_reverse(explode("-",$rowLaudo['agd_data']))) .' '.
                                        substr($rowLaudo['agd_hora_inicial'], 0, 5) .' às '. substr($rowLaudo['agd_hora_final'], 0, 5);
                                        ?>
                                    </b></a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse" id="accordion1_<?php echo $cont; ?>">
                                    <div class="panel-body">
                                        <div id="imprimir_<?php echo $cont; ?>">
                                            <i class="fa fa-heartbeat" aria-hidden="true"></i>&nbsp;Dr(a).
                                            <?php echo $rowLaudo['prof_nome']?> <br>
                                            <br>
                                            <?php echo $rowLaudo['pront_laudo']?>
                                            <br><br>
                                        </div>
                                        <div class="btn-group" style="float: right;">
                                            <button type="button" class="btn btn-default" onclick="PrintElem('#imprimir_<?php echo $cont; ?>')">
                                            <i class="fa fa-print"></i> Imprimir
                                            </button>
                                            <button type="button" class="btn btn-default cEditLaudo" data-toggle="modal" data-target="#LaudoEdt"
                                            data-cod="<?php echo $rowLaudo['pront_id']; ?>"
                                            data-mConsulta="<?php echo $data; ?>">
                                            <i class="fa fa-edit"></i> Alterar
                                            </button>
                                            <button type="button" class="btn btn-default excluir_laudo"
                                            data-cod="<?php echo $rowLaudo['pront_id']; ?>">
                                            <i class="fa  fa-trash-o"></i>  Excluir
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $cont++; }?>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="LaudoEdt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                        <div class="modal-dialog" role="document" style="width: 800px;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">
                                    ATENDIMENTO DE PACIENTE
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <b>PACIENTE:</b> <span><?php echo utf8_encode($row['pac_nome']); ?></span>
                                    <br>
                                    <b>CONSULTA:</b> <span id="mConsultaLaudo"></span>
                                    <br><br>
                                    <div id="carregandoLaudo">
                                        <br>
                                        <img src="img/reload.gif" height="15" width="15" alt="">
                                        Carregando informações  ...
                                    </div> <br>
                                    <form class="horizontal-form" id="form_laudoEdt" method="POST" enctype="multipart/form-data">
                                        <input type="hidden"  id="agd_id_laudo" name="pront_id"  value="">
                                        <input type="hidden" name="tipoForm" value="MUSER">
                                        <div id="mostraLaudo"></div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal" >Fechar</button>
                                    <button type="button" class="btn blue" onclick="enviarFormLaudoEdt();">Salvar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="mEditadoLaudo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">
                                    ATENDIMENTO DE PACIENTE
                                    </h4>
                                </div>
                                <div class="modal-body" align="center">
                                    <h2>Prontuário médico Atualizado com sucesso.</h2>
                                </div>
                                <div class="modal-footer">
                                    <a href="perfil_paciente.php?control=3&paciente=<?php echo $pac_id; ?>" type="button" class="btn btn-danger">Fechar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="Mpergunta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">
                                    PACIENTES AGENDADOS
                                    </h4>
                                </div>
                                <div class="modal-body" align="center">
                                    <h2>Deseja excluir esse agendamento ?</h2>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn green" data-dismiss="modal" id="enviaForm">Sim</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Não</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="Malterado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">
                                    PACIENTES AGENDADOS
                                    </h4>
                                </div>
                                <div class="modal-body" align="center">
                                    <h2>Agendamento cancelado com sucesso !</h2>
                                </div>
                                <div class="modal-footer">
                                    <a href="perfil_paciente.php?control=3&paciente=<?php echo $pac_id; ?>" type="button" class="btn btn-danger">Fechar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="atenderProntAgd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                        <div class="modal-dialog" role="document" style="width: 800px;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">
                                    ATENDIMENTO DE PACIENTE
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <b>PACIENTE:</b> <span><?php echo utf8_encode($row['pac_nome']); ?></span>
                                    <br>
                                    <b>CONSULTA:</b> <span> <?php echo date("d/m/Y H:i:s"); ?></span>
                                    <br><br>
                                    <form class="horizontal-form" id="form_laudoAgd" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="pac_id"             value="<?php echo $row['pac_id']; ?>">
                                        <input type="hidden" name="pac_tel_celular"    value="<?php echo $row['pac_tel_celular']; ?>">
                                        <input type="hidden" name="pac_tel_residencia" value="<?php echo $row['pac_tel_residencia']; ?>">
                                        <input type="hidden" name="pac_id_convenio"    value="<?php echo $row['pac_id_convenio']; ?>">
                                        <input type="hidden" name="pac_id_plano"       value="<?php echo $row['pac_id_plano']; ?>">
                                        
                                        <textarea id="pront_laudoAgd" name="pront_laudo"></textarea>
                                        <input type="hidden" name="tipoForm" value="I">
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal" >Fechar</button>
                                    <button type="button" class="btn blue" onclick="enviarFormLaudoAgd();">Salvar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Modal -->
                    <div class="modal fade" id="McadastradoLaudoAgd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">
                                    ATENDIMENTO DE PACIENTE
                                    </h4>
                                </div>
                                <div class="modal-body" align="center">
                                    <h2>Prontuário médico cadastrado com sucesso.</h2>
                                </div>
                                <div class="modal-footer">
                                    <a href="perfil_paciente.php?control=3&paciente=<?php echo $pac_id; ?>" type="button" class="btn btn-danger">Fechar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>