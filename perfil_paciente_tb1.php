<div class="tab-pane" id="tab_1_1">
    <div class="row">
        <div class="col-md-3">
            <ul class="list-unstyled profile-nav">
                <?php if ($row['pac_img'] == null OR $row['pac_img'] == "") {
                    $img = 'assets/pages/img/avatars/male.png';
                } else {
                    $img = 'uploads_foto_perfil/' . $row['pac_img'];
                }
                ?>
                <li>
                    <img src="<?php echo $img; ?>" class="img-responsive pic-bordered" alt="" />
                </li>
                <li>
                    <a href="javascript:;"> Atendimentos
                        <span> 00 </span>                   
                    </a>
                </li>
                <li>
                    <a href="javascript:;"> Agendamentos
                        <span> 00 </span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;"> Faltas
                        <span> 00 </span></a>                    
                    </li>
                </ul>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-8 profile-info">
                        <h1 class="font-green sbold uppercase"><?php echo utf8_encode($row['pac_nome']); ?></h1>
                        <p><?php echo utf8_encode($row['pac_obs']); ?></p>
                        <ul class="list-inline">
                            <li>
                                <i class="fa fa-calendar"></i>
                                  <?php echo $row['pac_nasc'];  ?> - <?php calcula_idade($row['pac_nasc']) ?>
                            </li><br>
                            <li>
                                <i class="fa fa-phone"></i> 
                                <?php echo $row['pac_tel_celular']; ?>
                            </li> <br>
                            <li>
                                <i class="fa fa-envelope-o">                                    
                                </i> <?php echo $row['pac_email']; ?>
                            </li><br>
                            <li>
                                <i class="fa fa-medkit"></i> 
                                <?php echo utf8_encode($row['pac_convenio']) .' - '. utf8_encode($row['pac_plano']); ?>
                            </li>
                        </ul>
                    </div>
                    <!--end col-md-8-->
                    <div class="col-md-4">
                        <div class="portlet sale-summary">
                            <div class="portlet-title" >
                                <div class="caption font-yellow sbold " style="float: right;"> 
                                    <form action="c_agenda.php" method="post" accept-charset="utf-8">
                                        <input type="hidden" name="pac_id" value="<?php echo $row['pac_id']; ?>"> 
                                        <button type="submit" class="btn green btn-outline">
                                            <i class="fa fa-calendar"></i>&nbsp;
                                            Marcar Consulta
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end col-md-4-->
                </div>
                <!--end row-->
                <div class="tabbable-line tabbable-custom-profile">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1_11" data-toggle="tab"> Histórico de Consultas </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1_11">
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <i class="fa fa-calendar"></i> Horarios </th>
                                                <th class="hidden-xs">
                                                    <i class="fa fa-stethoscope"></i> Profissional de saúde </th>
                                                    <th>
                                                        <i class="fa fa-bookmark"></i> Situação </th>
                                                        <th> </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php while($rowAgenda = mysqli_fetch_array($resultAgenda)){ ?>
                                                        <tr>
                                                            <td>
                                                                <?php 
                                                                echo  $data = implode("/",array_reverse(explode("-",$rowAgenda['agd_data']))) .' '.
                                                                substr($rowAgenda['agd_hora_inicial'], 0, 5) .' às '. substr($rowAgenda['agd_hora_final'], 0, 5);
                                                                ?>
                                                            </td>
                                                            <td class="hidden-xs"> <?php echo $rowAgenda['prof_nome']?> </td>
                                                            <td align="center">
                                                                <?php situacao($rowAgenda['agd_status']); ?>
                                                            </td>
                                                            <td align="center">
                                                                <div class="caption font-yellow sbold " align="center"> 
                                                                    <button type="button" class="btn red btn-outline cAtender" data-toggle="modal" data-target="#atenderM"
                                                                    data-cod="<?php echo $rowAgenda['agd_id']; ?>"
                                                                    data-mConsulta="<?php echo $data; ?>">
                                                                    Atender Paciente
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <!-- Modal -->
                                <div class="modal fade" id="atenderM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                                  <div class="modal-dialog" role="document" style="width: 800px;">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">
                                            ATENDIMENTO DE PACIENTE
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        <b>PACIENTE:</b> <span><?php echo utf8_encode($row['pac_nome']); ?></span> 
                                        <br>
                                        <b>CONSULTA:</b> <span id="mConsulta"></span>
                                        <br><br>
                                        <form class="horizontal-form" id="form_laudo" method="POST" enctype="multipart/form-data">
                                            <input type="hidden"  id="controle" name="controle"  value="">               
                                            <input type="hidden"  id="agd_id" name="pront_agd_id"  value="">                            
                                            <input type="hidden" name="tipoForm" value="I"> 
                                            <textarea id="pront_laudo" name="pront_laudo" class="mLaudo"></textarea>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" >Fechar</button>
                                        <button type="button" class="btn blue" onclick="enviarFormLaudo();">Salvar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="McadastradoLaudo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">
                                            ATENDIMENTO DE PACIENTE
                                        </h4>
                                    </div>
                                    <div class="modal-body" align="center">
                                        <h2>Prontuário médico cadastrado com sucesso.</h2>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="perfil_paciente.php?control=3&paciente=<?php echo $pac_id; ?>" type="button" class="btn green">Verificar histórico do paciente</a>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="concluidoAlterar()">Fechar</button>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>