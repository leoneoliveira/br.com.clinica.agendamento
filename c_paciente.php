<?php include 'head.php'; ?>
<link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <!-- BEGIN HEADER -->
    <?php include 'nav-top.php'; ?>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php include 'nav-left.php';
        include 'conexao/config.php'; 

        $queryConv = "SELECT * FROM convenio where conv_status = 'A' order by 1 asc";
        $resultConv = mysqli_query($conn, $queryConv);

        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE BAR -->
                <?php include 'breadcrumb.php'; ?>
                <!-- END PAGE BAR -->
                <!-- BEGIN PAGE TITLE-->
                <h3 class="page-title"> ADICIONAR PACIENTE
                </h3>
                <!-- END PAGE TITLE-->
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <form class="horizontal-form" id="form_sample_1" method="POST" enctype="multipart/form-data">
                            <div class="form-body">
                                <h3 class="form-section">Dados do Paciente</h3><br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label"><b>Nome</b></label>
                                            <input type="text" id="pac_nome" name="pac_nome" class="form-control" placeholder="Infome o nome do Pacinete">
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label"><b>Data de Nascimento</b></label>
                                                    <input class="form-control" id="mask_date" name="pac_nasc" type="text" placeholder="__/__/____" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label"><b>Sexo</b></label>
                                                    <div class="radio-list">
                                                        <label class="radio-inline">
                                                            <input type="radio" name="pac_sexo" id="optionsRadios1" value="M" checked> Masculino </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="pac_sexo" id="optionsRadios2" value="F"> Feminino </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="nome" class="control-label"><b>Convênio</b></label>
                                                            <div class="input-group select2-bootstrap-prepend">
                                                                <select id="pac_id_convenio" class="form-control select2" name="pac_id_convenio" >
                                                                    <option disabled selected>Convênio do paciente</option>
                                                                    <?php while($rowConv = mysqli_fetch_array($resultConv)){ ?>
                                                                    <option value="<?php echo $rowConv['conv_id']; ?>"                                                                        >
                                                                        <?php echo utf8_encode($rowConv['conv_nome']); ?>
                                                                    </option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="nome" class="control-label"><b>Plano</b></label>
                                                            <div class="input-group select2-bootstrap-prepend">
                                                                <select id="pac_id_plano" class="form-control select2" name="pac_id_plano" >
                                                                    <option disabled selected>Plano do paciente</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group ">
                                                    <div class="col-md-9">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 168px;">
                                                                <img src="assets/pages/img/avatars/male.png" height="600" width="600" alt=""> </div>
                                                                <div>
                                                                    <span class="btn red btn-outline btn-file">
                                                                        <span class="fileinput-new"> Imagem de perfil </span>
                                                                        <span class="fileinput-exists"> Alterar </span>
                                                                        <input type="file" name="file_imagem" id="file_imagem"> </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remover </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"><b>CPF</b></label>
                                                            <input type="text" id="mask_cpf" name="pac_cpf" class="form-control" placeholder="Infome o CPF do paciente">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"><b>Email</b></label>
                                                            <input type="text" id="firstName" name="pac_email" class="form-control" placeholder="Infome o email do paciente">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label"><b>Observações <span><small>(Informações sobre o paciente)</small></span></b></label>
                                                            <textarea class="form-control" rows="4" name="pac_obs"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h3>Telefones</h3> <br>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"><b>Celular</b></label>
                                                            <input type="text" name="pac_tel_celular" class="form-control mask_phone" placeholder="Infome o celular do paciente">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"><b>Residencial</b></label>
                                                            <input type="text" id="firstName" name="pac_tel_residencia" class="form-control mask_tel" placeholder="Infome o telefone do paciente">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"><b>Trabalho</b></label>
                                                            <input type="text" id="firstName" name="pac_tel_trabalho" class="form-control mask_tel" placeholder="Infome o telefone empresarial do paciente">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"><b>Ramal</b></label>
                                                            <input type="text" id="firstName" name="pac_tel_ramal" class="form-control" placeholder="Infome o ramal do paciente">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                                <h3 class="form-section">Endereço</h3> <br>
                                                <div class="row">
                                                    <div class="col-md-6 ">
                                                        <div class="form-group">
                                                            <label><b>CEP</b></label>
                                                            <input type="text" class="form-control cep" id="mask_cep" name="pac_end_cep">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <div id="carregandoCep">
                                                            <br>
                                                            <img src="img/reload.gif" height="15" width="15" alt="">
                                                            Carregando informações do CEP ...
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 ">
                                                        <div class="form-group">
                                                            <label><b>Endereço</b></label>
                                                            <input type="text" class="form-control" id="endereco" name="pac_end_endereco"> </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label><b>Numero</b></label>
                                                                <input type="text" class="form-control" name="pac_end_numero"> </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label><b>Complemento</b></label>
                                                                    <input type="text" class="form-control" name="pac_end_complemento"> </div>
                                                                </div>
                                                                <!--/span-->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label><b>Cidade</b></label>
                                                                        <input type="text" class="form-control" id="cidade" name="pac_end_cidade"> </div>
                                                                    </div>
                                                                    <!--/span-->
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label><b>Bairro</b></label>
                                                                            <input type="text" class="form-control" id="bairro" name="pac_end_bairo"> </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                    <!--/row-->
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label><b>Estado</b></label>
                                                                                <input type="text" class="form-control" id="uf" name="pac_end_estado"> </div>
                                                                            </div>
                                                                            <!--/span-->
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label><b>Pais</b></label>
                                                                                    <select class="form-control" name="pac_end_pais">
                                                                                        <?php include 'includes/paises.php'; ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <!--/span-->
                                                                        </div>
                                                                    </div>
                                                                    <input type="hidden" name="tipoForm" value="I">
                                                                </form>
                                                                <br><br>
                                                                <div class="form-actions right">
                                                                    <button type="button" class="btn default" onclick="concluido()">
                                                                        Cancelar
                                                                    </button>
                                                                    <button type="button" class="btn blue" onclick="enviarForm()">
                                                                        <i class="fa fa-check"></i> Salvar</button>
                                                                    </div>
                                                                    <!-- Modal -->
                                                                    <div class="modal fade" id="Mcadastrado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                                                                        <div class="modal-dialog" role="document">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <h4 class="modal-title" id="myModalLabel">
                                                                                        PACIENTE
                                                                                    </h4>
                                                                                </div>
                                                                                <div class="modal-body" align="center">
                                                                                    <h2>Cadastrado com sucesso !</h2>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn green"  onclick="novoPaciente()">Adicionar paciente</button>
                                                                                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="concluido()">Fechar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- END PAGE HEADER-->
                                                        </div>
                                                        <!-- END CONTENT BODY -->
                                                    </div>
                                                    <!-- END CONTENT -->
                                                </div>
                                                <!-- END CONTAINER -->
                                                <?php include 'footer.php'; ?>
                                                <script src="assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
                                                <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
                                                <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
                                                <script src="js/table-datatables.js" type="text/javascript"></script>
                                                <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                                                <script src="js/model/paciente.js" type="text/javascript"></script>
                                                <script src="includes/busca_cep.js" type="text/javascript"></script>
                                                <script src="assets/apps/scripts/form-input-mask.js" type="text/javascript"></script>
                                                <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
                                                <script src="assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
                                            </body>
                                            <script>
                                                $(document).ready(function() {
                                                    $(".nav-item").removeClass('start active open');
                                                    $("#m_paciente").addClass('start active open');
                                                    $("#carregandoCep").hide();

                                                    $("#pac_id_plano").prop("disabled", true);


                                                    $( "#pac_id_convenio" ).change(function() {
                                                      var pac_id_conv = $(this).val();

                                                      $.ajax({
                                                          method: "POST",
                                                          url: "includes/carregar_campos_plano.php",
                                                          data: { pac_id_conv: pac_id_conv},
                                                          success: function( data ) {

                                                            $("#pac_id_plano").prop("disabled", false); 
                                                            var data = $.parseJSON(data);
                                                            $.each(data, function(index) {
                                                                $('#pac_id_plano').append("<option value="+data[index].plan_id+">"+data[index].plan_nome+"</option>")
                                                            });
                                                        },
                                                        error: function (){
                                                        }
                                                    }); 
                                                  });


                                                });
                                                function enviarForm(){
                                                    $('#form_sample_1').submit();
                                                }
                                                function concluido(){
                                                    window.location.href = "paciente.php";
                                                }
                                                function novoPaciente(){
                                                    window.location.href = "c_paciente.php";
                                                }
                                            </script>
                                            </html>