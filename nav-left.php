<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" >
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper hide">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler"> </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            <li class="sidebar-search-wrapper">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
            </li>
            <li class="nav-item" id="m_home">
                <a href="home.php" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Home</span>
                    <span class="selected"></span>
                </a>
            </li>
            
            
            <li class="nav-item  " id="m_agenda">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-calendar"></i>
                    <span class="title">Agenda</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  " id="m_agenda_l">
                        <a href="agenda.php" class="nav-link ">
                            <span class="title">Agenda</span>
                        </a>
                    </li>
                    <li class="nav-item  " id="p_agenda">
                        <a href="p_agenda.php" class="nav-link ">
                            <span class="title">Pacientes agendados</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item" id="m_paciente">
                <a href="paciente.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Pacientes</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item" id="m_usuario">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-user-md"></i>
                    <span class="title">Usuários</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item" id="m_profissional">
                        <a href="profissional_saude.php" class="nav-link ">
                            <span class="title">Profissional de saúde</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item  " id="m_usuarios">
                        <a href="usuarios.php" class="nav-link ">
                            <span class="title">Recepcionista</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item" id="m_convenio">
                <a href="convenio.php" class="nav-link nav-toggle">
                     <i class="fa fa-medkit"></i>
                    <span class="title">Convênios</span>
                    <span class="selected"></span>
                </a>
            </li>
<!--             <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">Relatório</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="layout_blank_page.html" class="nav-link ">
                            <span class="title">Blank Page</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="layout_classic_page_head.html" class="nav-link ">
                            <span class="title">Classic Page Head</span>
                        </a>
                    </li>
                </ul>
            </li> -->
            
        </ul>
    </li>
</ul>
<!-- END SIDEBAR MENU -->
<!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->
</div>