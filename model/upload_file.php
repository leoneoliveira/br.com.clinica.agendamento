<?php
// Pasta onde o arquivo vai ser salvo
$_UP['pasta'] = '../uploads_foto_perfil/';

// Tamanho máximo do arquivo (em Bytes)
$_UP['tamanho'] = 1024 * 1024 * 5; // 5Mb

// Array com as extensões permitidas
$_UP['extensoes'] = array('jpg', 'png');

// Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
$_UP['renomeia'] = true;

// Array com os tipos de erros de upload do PHP
$_UP['erros'][0] = 'Não houve erro';
$_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
$_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
$_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
$_UP['erros'][4] = 'Não foi feito o upload do arquivo';

// Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
if ($_FILES['file_imagem']['error'] != 0) {
    die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['file_imagem']['error']]);
    exit; // Para a execução do script
}

// Faz a verificação da extensão do arquivo
$extensao = explode('.', $_FILES['file_imagem']['name']);
$extensao = end($extensao);
$extensao = strtolower($extensao);
if (array_search($extensao, $_UP['extensoes']) === false) {
    echo "Por favor, envie arquivos com as seguintes extensões: jpg ou png;";
    exit;
}

// Faz a verificação do tamanho do arquivo
if ($_UP['tamanho'] < $_FILES['file_imagem']['size']) {
    echo "O arquivo enviado é muito grande, envie arquivos de até 5Mb.";
    exit;
}

// Primeiro verifica se deve trocar o nome do arquivo
if ($_UP['renomeia'] == true) {
    // Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
    $nome_final = md5(time()).'.'.$extensao;   //'.jpg';
} else {
    // Mantém o nome original do arquivo
    $nome_final = $_FILES['file_imagem']['name'];
}

// Depois verifica se é possível mover o arquivo para a pasta escolhida
if (move_uploaded_file($_FILES['file_imagem']['tmp_name'], $_UP['pasta'] . $nome_final)) {
    $img_perfil = $nome_final;
} else {
    $img_perfil = '';
    echo "Não foi possível enviar o arquivo, tente novamente";
}