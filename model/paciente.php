<?php 
include '../verifica.php';
include '../conexao/config.php';

if (isset($_POST['pac_id'])) {
    $pac_id =  utf8_decode($_POST['pac_id']);
}else{
    $pac_id = '';
}

if (isset($_POST['pac_nome'])) {
    $pac_nome =  utf8_decode($_POST['pac_nome']);
}else{
    $pac_nome = '';
}

if (isset($_POST['pac_nasc'])) {
    $pac_nasc =  utf8_decode($_POST['pac_nasc']);
}else{
    $pac_nasc = '';
}

if (isset($_POST['pac_cpf'])) {
    $pac_cpf =  utf8_decode($_POST['pac_cpf']);
}else{
    $pac_cpf = '';
}

if (isset($_POST['pac_email'])) {
    $pac_email =  utf8_decode($_POST['pac_email']);
}else{
    $pac_email = '';
}

if (isset($_POST['pac_sexo'])) {
    $pac_sexo =  utf8_decode($_POST['pac_sexo']);
}else{
    $pac_sexo = '';
}

if (isset($_POST['pac_obs'])) {
    $pac_obs =  utf8_decode($_POST['pac_obs']);
}else{
    $pac_obs = '';
}

if (isset($_POST['pac_tel_celular'])) {
    $pac_tel_celular =  utf8_decode($_POST['pac_tel_celular']);
}else{
    $pac_tel_celular = '';
}

if (isset($_POST['pac_tel_residencia'])) {
    $pac_tel_residencia =  utf8_decode($_POST['pac_tel_residencia']);
}else{
    $pac_tel_residencia = '';
}

if (isset($_POST['pac_tel_trabalho'])) {
    $pac_tel_trabalho =  utf8_decode($_POST['pac_tel_trabalho']);
}else{
    $pac_tel_trabalho = '';
}

if (isset($_POST['pac_tel_ramal'])) {
    $pac_tel_ramal =  utf8_decode($_POST['pac_tel_ramal']);
}else{
    $pac_tel_ramal = '';
}

if (isset($_POST['pac_end_cep'])) {
    $pac_end_cep =  utf8_decode($_POST['pac_end_cep']);
}else{
    $pac_end_cep = '';
}

if (isset($_POST['pac_end_endereco'])) {
    $pac_end_endereco =  utf8_decode($_POST['pac_end_endereco']);
}else{
    $pac_end_endereco = '';
}

if (isset($_POST['pac_end_numero'])) {
    $pac_end_numero =  utf8_decode($_POST['pac_end_numero']);
}else{
    $pac_end_numero = '';
}

if (isset($_POST['pac_end_complemento'])) {
    $pac_end_complemento =  utf8_decode($_POST['pac_end_complemento']);
}else{
    $pac_end_complemento = '';
}

if (isset($_POST['pac_end_cidade'])) {
    $pac_end_cidade =  utf8_decode($_POST['pac_end_cidade']);
}else{
    $pac_end_cidade = '';
}

if (isset($_POST['pac_end_bairo'])) {
    $pac_end_bairo =  utf8_decode($_POST['pac_end_bairo']);
}else{
    $pac_end_bairo = '';
}

if (isset($_POST['pac_end_estado'])) {
    $pac_end_estado =  utf8_decode($_POST['pac_end_estado']);
}else{
    $pac_end_estado = '';
}

if (isset($_POST['pac_end_pais'])) {
    $pac_end_pais =  utf8_decode($_POST['pac_end_pais']);
}else{
    $pac_end_pais = '';
}

if (isset($_POST['pac_id_convenio'])) {
    $pac_id_convenio =  utf8_decode($_POST['pac_id_convenio']);
}else{
    $pac_id_convenio = '';
}

if (isset($_POST['pac_id_plano'])) {
    $pac_id_plano =  utf8_decode($_POST['pac_id_plano']);
}else{
    $pac_id_plano = '';
}
 
if(isset($_FILES['file_imagem'])){
    if (!empty($_FILES['file_imagem']['name'])) {
        include 'upload_file.php';
    }else{ $img_perfil = ''; }
} else {
    $img_perfil = '';
}

if (isset($_POST['pac_status'])) {
    $pac_status =  utf8_decode($_POST['pac_status']);
}else{
    $pac_status = '';
}

if (isset($_POST['tipoForm'])) {
    $tipoForm =  utf8_decode($_POST['tipoForm']);
}else{
    $tipoForm = '';
}

$pac_user_add = $_SESSION['prof_id'];
$pac_user_alt = $_SESSION['prof_id'];

 $sqlInsert = "INSERT INTO paciente(    pac_nome
									  , pac_nasc
									  , pac_cpf
									  , pac_email
									  , pac_sexo
									  , pac_img
									  , pac_obs
									  , pac_tel_celular
									  , pac_tel_residencia
									  , pac_tel_trabalho
									  , pac_tel_ramal
									  , pac_end_cep
									  , pac_end_endereco
									  , pac_end_numero
									  , pac_end_complemento
									  , pac_end_cidade
									  , pac_end_bairo
									  , pac_end_estado
									  , pac_end_pais
									  , pac_id_convenio
                                      , pac_id_plano
                                      , pac_dt_add
									  , pac_user_add
									  , pac_status
									  ) VALUES (
									    '$pac_nome'
									  , '$pac_nasc'
									  , '$pac_cpf'
									  , '$pac_email'
									  , '$pac_sexo'
									  , '$img_perfil'
									  , '$pac_obs'
									  , '$pac_tel_celular'
									  , '$pac_tel_residencia'
									  , '$pac_tel_trabalho'
									  , '$pac_tel_ramal'
									  , '$pac_end_cep'
									  , '$pac_end_endereco'
									  , '$pac_end_numero'
									  , '$pac_end_complemento'
									  , '$pac_end_cidade'
									  , '$pac_end_bairo'
									  , '$pac_end_estado'
									  , '$pac_end_pais'
									  , '$pac_id_convenio'
                                      , '$pac_id_plano'
                                      ,  NOW()
									  , '$pac_user_add'
									  , 'A')";



$sqlStatus = "UPDATE paciente SET pac_status = '$pac_status'
                               , pac_dt_alt = NOW() 
                               , pac_user_alt = '$pac_user_alt' 
                                 WHERE pac_id = '$pac_id'";

$sqlUpdate = "UPDATE paciente SET    pac_nome = '$pac_nome' 
                     , pac_nasc = '$pac_nasc' 
                     , pac_cpf = '$pac_cpf' 
                     , pac_email = '$pac_email' 
                     , pac_sexo = '$pac_sexo' 
                     , pac_img = '$img_perfil' 
                     , pac_obs = '$pac_obs' 
                     , pac_tel_celular = '$pac_tel_celular'
                     , pac_tel_residencia = '$pac_tel_residencia' 
                     , pac_tel_trabalho = '$pac_tel_trabalho' 
                     , pac_tel_ramal = '$pac_tel_ramal' 
                     , pac_end_cep = '$pac_end_cep' 
                     , pac_end_endereco = '$pac_end_endereco' 
                     , pac_end_numero = '$pac_end_numero' 
                     , pac_end_complemento = '$pac_end_complemento' 
                     , pac_end_cidade = '$pac_end_cidade' 
                     , pac_end_bairo = '$pac_end_bairo' 
                     , pac_end_estado = '$pac_end_estado' 
                     , pac_end_pais = '$pac_end_pais'
                     , pac_id_plano = '$pac_id_plano'
                     , pac_id_convenio = '$pac_id_convenio' 
                     , pac_dt_alt = NOW() 
                     , pac_user_alt = '$pac_user_alt' 
                     WHERE pac_id = '$pac_id'";


if($tipoForm == 'I'){
    //novo cadastro de usuario
    if (mysqli_query($conn, $sqlInsert)) {
        echo ' cadastrado';
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
}

if($tipoForm == 'M'){
    //novo cadastro de usuario
    if (mysqli_query($conn, $sqlStatus)) {
        echo ' cadastrado';
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
}

if($tipoForm == 'MUSER'){
    //novo cadastro de usuario
    if (mysqli_query($conn, $sqlUpdate)) {
        echo ' Alterado';
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

}