<?php 
include '../verifica.php';
include '../conexao/config.php';


if (isset($_POST['agd_id'])) {
    $agd_id =  utf8_decode($_POST['agd_id']);
}else{
    $agd_id = '';
}

if (isset($_POST['agd_id_remarcada'])) {
    $agd_id_remarcada =  utf8_decode($_POST['agd_id_remarcada']);
}else{
    $agd_id_remarcada = '';
}

if (isset($_POST['agd_id_pac'])) {
    $agd_id_pac =  utf8_decode($_POST['agd_id_pac']);
}else{
    $agd_id_pac = '';
}

if (isset($_POST['agd_tel_celulcar'])) {
    $agd_tel_celulcar =  utf8_decode($_POST['agd_tel_celulcar']);
}else{
    $agd_tel_celulcar = '';
}

if (isset($_POST['agd_tel_residencia'])) {
    $agd_tel_residencia =  utf8_decode($_POST['agd_tel_residencia']);
}else{
    $agd_tel_residencia = '';
}

if (isset($_POST['agd_id_convenio'])) {
    $agd_id_convenio =  utf8_decode($_POST['agd_id_convenio']);
}else{
    $agd_id_convenio = '';
}

if (isset($_POST['agd_id_plano'])) {
    $agd_id_plano =  utf8_decode($_POST['agd_id_plano']);
}else{
    $agd_id_plano = '';
}

if (isset($_POST['agd_id_prof'])) {
     $agd_id_prof =  utf8_decode($_POST['agd_id_prof']);
}else{
     $agd_id_prof = '';
}

if (isset($_POST['agd_data'])) {
    $agd_data =  utf8_decode($_POST['agd_data']);
}else{
    $agd_data = '';
}

if (isset($_POST['agd_hora_inicial'])) {
    $agd_hora_inicial =  utf8_decode($_POST['agd_hora_inicial']);
}else{
    $agd_hora_inicial = '';
}

if (isset($_POST['agd_hora_final'])) {
    $agd_hora_final =  utf8_decode($_POST['agd_hora_final']);
}else{
    $agd_hora_final = '';
}

if (isset($_POST['agd_obs'])) {
    $agd_obs =  utf8_decode($_POST['agd_obs']);
}else{
    $agd_obs = '';
}


//  STATUS
// M  - MARCADO
// C  - CONFIRMADO
// AG - AGUARDANDO
// EM - EM ATENDIMENTO
// A  - ATENDIDO 
// R  - REMARCADO
// X  - EXCLUIDO

if (isset($_POST['agd_status'])) {
    echo $agd_status =  utf8_decode($_POST['agd_status']);
}else{
    $agd_status = '';
}



if (isset($_POST['tipoForm'])) {
    echo $tipoForm =  utf8_decode($_POST['tipoForm']);
}else{
    $tipoForm = '';
}


$agd_user_add = $_SESSION['prof_id'];
$agd_user_alt = $_SESSION['prof_id'];



 $sqlInsert = "INSERT INTO agendamento(    
										  agd_id_pac
										, agd_tel_celulcar
										, agd_tel_residencia
										, agd_id_convenio
										, agd_id_plano
										, agd_id_prof
										, agd_data
										, agd_hora_inicial
										, agd_hora_final
										, agd_obs
										, agd_status
										, agd_dt_add
										, agd_user_add
										) VALUES (
										  '$agd_id_pac'
										, '$agd_tel_celulcar'
										, '$agd_tel_residencia'
										, '$agd_id_convenio'
										, '$agd_id_plano'
										, '$agd_id_prof'
										,  STR_TO_DATE('$agd_data', '%d/%m/%Y')
										,  STR_TO_DATE('$agd_hora_inicial', '%H:%i:%s')
										,  STR_TO_DATE('$agd_hora_final', '%H:%i:%s')
										, '$agd_obs'
										, 'M'
										,  NOW()
                                        , '$agd_user_add')";

 
 $sqlRemarcada = "UPDATE  agendamento  SET  
                          agd_id_remarcada   =  LAST_INSERT_ID()
                        , agd_status         = '$agd_status'
                        , agd_dt_alt         =  NOW()
                        , agd_user_alt       =  '$agd_user_alt'
                         WHERE agd_id        = '$agd_id'";

$sqlExcluir = "UPDATE  agendamento  SET 
                          agd_status         = '$agd_status'
                        , agd_dt_alt         =  NOW()
                        , agd_user_alt       =  '$agd_user_alt'
                         WHERE agd_id        = '$agd_id'";


 if($tipoForm == 'I'){
//novo cadastro de usuario
    if (mysqli_query($conn, $sqlInsert)) {
        echo ' cadastrado';

        if ($agd_id_remarcada != null || $agd_id_remarcada != '') {
            // Caso for uma remarcação
            if (mysqli_query($conn, $sqlRemarcada)) {
                echo ' cadastrado';
            } else {
                echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            }   

        }

    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
}

if($tipoForm == 'M'){
    //novo cadastro de usuario
    if (mysqli_query($conn, $sqlExcluir)) {
        echo ' cadastrado';
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

}
