<?php 
include '../verifica.php';
include '../conexao/config.php';

if (isset($_POST['prof_id'])) {
    $prof_id =  utf8_decode($_POST['prof_id']);
}else{
    $prof_id = '';
}

if (isset($_POST['prof_nome'])) {
    $prof_nome =  utf8_decode($_POST['prof_nome']);
}else{
    $prof_nome = '';
}

if (isset($_POST['prof_email'])) {
    $prof_email =  utf8_decode($_POST['prof_email']);
}else{
    $prof_email = '';
}

if (isset($_POST['prof_sexo'])) {
    $prof_sexo =  utf8_decode($_POST['prof_sexo']);
}else{
    $prof_sexo = '';
}

if (isset($_POST['prof_senha'])) {
    $prof_senha =  utf8_decode($_POST['prof_senha']);
}else{
    $prof_senha = '';
}

if (isset($_POST['prof_obs'])) {
    $prof_obs =  utf8_decode($_POST['prof_obs']);
}else{
    $prof_obs = '';
}

if (isset($_POST['prof_tel_celular'])) {
    $prof_tel_celular =  utf8_decode($_POST['prof_tel_celular']);
}else{
    $prof_tel_celular = '';
}

if (isset($_POST['prof_tel_residencia'])) {
    $prof_tel_residencia =  utf8_decode($_POST['prof_tel_residencia']);
}else{
    $prof_tel_residencia = '';
}

if (isset($_POST['prof_tel_trabalho'])) {
    $prof_tel_trabalho =  utf8_decode($_POST['prof_tel_trabalho']);
}else{
    $prof_tel_trabalho = '';
}

if (isset($_POST['prof_tel_ramal'])) {
    $prof_tel_ramal =  utf8_decode($_POST['prof_tel_ramal']);
}else{
    $prof_tel_ramal = '';
}

if (isset($_POST['prof_saud_conselho'])) {
    $prof_saud_conselho =  utf8_decode($_POST['prof_saud_conselho']);
}else{
    $prof_saud_conselho = '';
}

if (isset($_POST['prof_saud_registro'])) {
    $prof_saud_registro =  utf8_decode($_POST['prof_saud_registro']);
}else{
    $prof_saud_registro = '';
}

if (isset($_POST['prof_saud_profissao'])) {
    $prof_saud_profissao =  utf8_decode($_POST['prof_saud_profissao']);
}else{
    $prof_saud_profissao = '';
}

if (isset($_POST['prof_saud_cbo'])) {
    $prof_saud_cbo =  utf8_decode($_POST['prof_saud_cbo']);
}else{
    $prof_saud_cbo = '';
}


if(isset($_FILES['file_imagem'])){
    if (!empty($_FILES['file_imagem']['name'])) {
        include 'upload_file.php';
    }else{ $img_perfil = ''; }
} else {
    $img_perfil = '';
}



if (isset($_POST['prof_tipo'])) {
    $prof_tipo =  utf8_decode($_POST['prof_tipo']);
}else{
    $prof_tipo = '';
}


if (isset($_POST['tipoForm'])) {
    $tipoForm =  utf8_decode($_POST['tipoForm']);
}else{
    $tipoForm = '';
}

if (isset($_POST['prof_status'])) {
    $prof_status =  utf8_decode($_POST['prof_status']);
}else{
    $prof_status = '';
}
 

$prof_dt_add = 'NOW()';
$prof_dt_alt = 'NOW()';

$prof_user_add = $_SESSION['prof_id'];
$prof_user_alt = $_SESSION['prof_id'];


$sqlInsert = "INSERT INTO profissional	(
                                prof_nome
                              , prof_email
                              , prof_sexo
                              , prof_img
                              , prof_senha
                              , prof_obs
                              , prof_tel_celular
                              , prof_tel_residencia
                              , prof_tel_trabalho
                              , prof_tel_ramal
                              , prof_saud_conselho
                              , prof_saud_registro
                              , prof_saud_profissao
                              , prof_saud_cbo
                              , prof_dt_add                              
                              , prof_user_add                              
                              , prof_status
                              , prof_tipo
                              ) VALUES (
                               '$prof_nome'
                              ,'$prof_email'
                              ,'$prof_sexo'
                              ,'$img_perfil'
                              ,'$prof_senha'
                              ,'$prof_obs'
                              ,'$prof_tel_celular'
                              ,'$prof_tel_residencia'
                              ,'$prof_tel_trabalho'
                              ,'$prof_tel_ramal'
                              ,'$prof_saud_conselho'
                              ,'$prof_saud_registro'
                              ,'$prof_saud_profissao'
                              ,'$prof_saud_cbo'
                              , NOW()                              
                              ,'$prof_user_add'                              
                              ,'A'
                              ,'$prof_tipo')";


$sqlStatus = "UPDATE profissional SET prof_status = '$prof_status' 
                                     ,prof_user_alt ='$prof_user_alt'
                                     ,prof_dt_alt = NOW()
                                      WHERE prof_id = '$prof_id'";


$sqlModifica = "UPDATE profissional SET prof_nome= '$prof_nome'
                                       ,prof_email='$prof_email'
                                       ,prof_sexo='$prof_sexo'
                                       ,prof_img='$img_perfil'
                                       ,prof_senha='$prof_senha'
                                       ,prof_obs='$prof_obs'
                                       ,prof_tel_celular='$prof_tel_celular'
                                       ,prof_tel_residencia='$prof_tel_residencia'
                                       ,prof_tel_trabalho='$prof_tel_trabalho'
                                       ,prof_tel_ramal='$prof_tel_ramal'
                                       ,prof_saud_conselho='$prof_saud_conselho'
                                       ,prof_saud_registro='$prof_saud_registro'
                                       ,prof_saud_profissao='$prof_saud_profissao'
                                       ,prof_saud_cbo='$prof_saud_cbo'
                                       ,prof_dt_alt=NOW()
                                       ,prof_user_alt='$prof_user_alt'
                                       WHERE prof_id = '$prof_id'";

if($tipoForm == 'I'){
    //novo cadastro de usuario
    if (mysqli_query($conn, $sqlInsert)) {
        echo ' cadastrado';
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

}

if($tipoForm == 'M'){
    //novo cadastro de usuario
    if (mysqli_query($conn, $sqlStatus)) {
        echo ' cadastrado';
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

}

if($tipoForm == 'MUSER'){
    //novo cadastro de usuario
    if (mysqli_query($conn, $sqlModifica)) {
        echo ' Alterado';
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

}