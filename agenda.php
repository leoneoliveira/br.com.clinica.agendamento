<?php include 'head.php'; ?>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <!-- BEGIN HEADER -->
    <?php include 'nav-top.php'; ?>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php include 'nav-left.php'; ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE BAR -->
                <?php include 'breadcrumb.php'; ?>
                <!-- END PAGE BAR -->
                <!-- BEGIN PAGE TITLE-->
                <h3 class="page-title"> AGENDA                
                </h3>
                <!-- END PAGE TITLE-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light portlet-fit bordered calendar">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                            <a href="c_agenda.php" class="btn btn-success"><i class="fa fa-plus-circle"></i>
                              &nbsp;ADICIONAR</a>
                        </div>
                            </div>
                            <div class="portlet-body">
                                                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light portlet-fit bordered calendar">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class=" icon-layers font-green"></i>
                                            <span class="caption-subject font-green sbold uppercase">Calendar</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-12">
                                                <!-- BEGIN DRAGGABLE EVENTS PORTLET-->
                                                <h3 class="event-form-title margin-bottom-20">Adicionar Paciente</h3>
                                                <div id="external-events">
                                                    <form class="inline-form">
                                                        <input type="text" value="" class="form-control" placeholder="Nome complento" id="event_title" />
                                                        <br/>
														<input type="text" value="" class="form-control" placeholder="Convenio" id="event_title" />
														<br/>
                                                        <a href="javascript:;" id="event_add" class="btn green"> Add Event </a>
                                                    </form>
                                                    <hr/>
                                                    <div id="event_box" class="margin-bottom-10"></div>                                                    
                                                    <hr class="visible-xs" /> </div>
                                                <!-- END DRAGGABLE EVENTS PORTLET-->
                                            </div>
                                            <div class="col-md-9 col-sm-12">
                                                <div id="calendar" class="has-toolbar"> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE HEADER-->
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->
    <?php include 'footer.php'; ?>
    <script>
    $(document).ready(function() {
        $(".nav-item").removeClass('start active open');
        $("#m_agenda").addClass('start active open');
        $("#m_agenda_l").addClass('start active open');
    });
    </script>
</body>
</html>