<?php include 'head.php'; ?>
<link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

    <?php 

    if (isset($_GET['tp'])) {
        $tipo_profi =  $_GET['tp'];
    }else{
        $tipo_profi = '0';
    }

    ?>


    <!-- BEGIN HEADER -->
    <?php include 'nav-top.php'; ?>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php include 'nav-left.php'; ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE BAR -->
                <?php include 'breadcrumb.php'; ?>
                <!-- END PAGE BAR -->
                <!-- BEGIN PAGE TITLE-->
                <h3 class="page-title"> 
                    <?php if ($tipo_profi == 1) {
                        echo "RECEPCIONISTA";
                        $cancelar = "usuarios.php";
                    } else {
                        echo "PROFISSIONAL DE SAÚDE";
                        $cancelar = "profissional_saude.php";
                    }
                    ?>                
                </h3>
                <!-- END PAGE TITLE-->
                <div class="portlet light bordered">

                    <div class="portlet-body">
                        <form class="horizontal-form" id="form_sample_1" method="POST" enctype="multipart/form-data">
                            <div class="form-body">
                                <h3 class="form-section">Dados do usuário</h3> <br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" ><b>Nome</b></label>
                                            <input type="text" id="prof_nome" name="prof_nome" class="form-control" placeholder="Infome o nome do Pacinete">
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label"><b>Email</b></label>
                                            <input type="text" id="prof_email" name="prof_email" class="form-control" placeholder="Infome o email do paciente">
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label"><b>Sexo</b></label>
                                            <div class="radio-list">
                                                <label class="radio-inline">
                                                    <input type="radio" name="prof_sexo" id="prof_sexo1" value="M" checked> Masculino </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="prof_sexo" id="prof_sexo2" value="F"> Feminino </label>
                                                    </div>
                                                </div>

                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group ">

                                                    <div class="col-md-9">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 168px;">
                                                                <img src="assets/pages/img/avatars/male.png" height="600" width="600" alt=""> </div>
                                                                <div>
                                                                    <span class="btn red btn-outline btn-file">
                                                                        <span class="fileinput-new"> Imagem de perfil </span>
                                                                        <span class="fileinput-exists"> Alterar </span>
                                                                        <input type="file" name="file_imagem" id="file_imagem"> </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remover </a>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"><b>Senha</b></label>
                                                            <input type="password" id="prof_senha" name="prof_senha" class="form-control" placeholder="Infome o nome do Pacinete">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"><b>Repetir Senha</b></label>
                                                            <input type="password" id="prof_senha_repet" name="prof_senha_repet" class="form-control" placeholder="Infome o nome do Pacinete">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label"><b>Observações</b></label>
                                                            <textarea class="form-control" name="prof_obs" rows="4"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h3>Telefones</h3><br>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"><b>Celular</b></label>
                                                            <input type="text" name="prof_tel_celular" class="form-control mask_phone" placeholder="Infome o celular do paciente">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"><b>Residencial</b></label>
                                                            <input type="text" id="firstName" name="prof_tel_residencia" class="form-control mask_tel" placeholder="Infome o telefone do paciente">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"><b>Trabalho</b></label>
                                                            <input type="text" id="firstName" name="prof_tel_trabalho" class="form-control mask_tel" placeholder="Infome o telefone empresarial do paciente">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"><b>Ramal</b></label>
                                                            <input type="text" id="firstName" name="prof_tel_ramal" class="form-control" placeholder="Infome o ramal do paciente">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                                <div id="p_Profissional">
                                                    <h3 class="form-section">Profissional de saúde</h3> <br>
                                                    <div class="row">
                                                        <div class="col-md-6 ">
                                                            <div class="form-group">
                                                                <label><b>Conselho</b></label>
                                                                <select class="form-control" name="prof_saud_conselho">
                                                                    <option value=""> -- Selecione o conselho -- </option>
                                                                    <?php include 'includes/conselho.php'; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <div class="form-group">
                                                                <label><b>Registro</b></label>
                                                                <input type="text" class="form-control" name="prof_saud_registro">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6 ">
                                                            <div class="form-group">
                                                                <label><b>Profissão</b></label>
                                                                <select class="form-control" name="prof_saud_profissao">
                                                                    <option value=""> -- Selecione a profissão -- </option>
                                                                    <?php include 'includes/profissao.php'; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <div class="form-group">
                                                                <label><b>Cód. brasileiro de ocupação</b></label>
                                                                <select class="form-control" name="prof_saud_cbo">
                                                                    <option value=""> -- Selecione o CBO -- </option>
                                                                    <?php include 'includes/cbo.php'; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <br>

                                                <input type="hidden" name="tipoForm" value="I"> 
                                                <input type="hidden" name="prof_tipo" value="<?php echo $tipo_profi; ?>">
                                            </form>
                                            <div class="form-actions right">
                                                <a href="<?php echo $cancelar; ?>" class="btn default">Cancelar</a>
                                                <button type="button" class="btn blue" onclick="enviarForm();">
                                                    <i class="fa fa-check"></i> Salvar</button>
                                                </div>
                                                
                                            </div>



                                            <!-- Modal -->
                                            <div class="modal fade" id="Mcadastrado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">
                                                        <?php if ($tipo_profi == 1) {
                                                            echo "USUÁRIO";
                                                        } else {
                                                            echo "PROFISSIONAL DE SAÚDE";
                                                        }
                                                        ?>  
                                                    </h4>
                                                </div>
                                                <div class="modal-body" align="center">
                                                    <h2>Cadastrado com sucesso !</h2>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="concluido()">Fechar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <!-- END PAGE HEADER-->
                            </div>
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                    </div>
                    <!-- END CONTAINER -->
                    <?php include 'footer.php'; ?>
                    <script src="assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
                    <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
                    <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
                    <script src="js/table-datatables.js" type="text/javascript"></script>
                    <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>

                    <script src="js/model/profissinal_saude.js" type="text/javascript"></script>
                    <script src="assets/apps/scripts/form-input-mask.js" type="text/javascript"></script>
                    <script src="assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
                    <script>

                    $(document).ready(function() {
                       $(".nav-item").removeClass('start active open');
                       $("#m_usuario").addClass('start active open');                                

                       if ("<?php echo $tipo_profi; ?>" == 1) {
                         $('#p_Profissional').hide();
                         $("#m_usuarios").addClass('start active open');
                     }else{
                        $("#m_profissional").addClass('start active open');

                    }

                });

                    function enviarForm(){
                        $('#form_sample_1').submit();
                    }
                    function concluido(){
                         window.location.assign("<?php echo $cancelar; ?>")
                    }
                    </script>
                </body>
                </html>