<?php include 'head.php';?>
<link href="assets/pages/css/profile-2.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <!-- BEGIN HEADER -->
    <?php include 'nav-top.php'; ?>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php include 'nav-left.php'; ?>
        <!-- END SIDEBAR -->
        <?php include 'conexao/config.php';
        if (isset($_GET['paciente'])) {
            $pac_id =  utf8_decode($_GET['paciente']);
        }else{
            $pac_id = '';
        }

        $query = " SELECT pac_id, 
        pac_nome, 
        pac_nasc, 
        pac_cpf, 
        pac_email, 
        pac_sexo, 
        pac_img, 
        pac_obs, 
        pac_tel_celular, 
        pac_tel_residencia, 
        pac_tel_trabalho, 
        pac_tel_ramal, 
        pac_end_cep, 
        pac_end_endereco, 
        pac_end_numero, 
        pac_end_complemento, 
        pac_end_cidade, 
        pac_end_bairo, 
        pac_end_estado, 
        pac_end_pais, 
        (SELECT conv_nome FROM convenio WHERE pac_id_convenio = conv_id ) pac_convenio, 
        (SELECT plan_nome FROM plano    WHERE pac_id_plano    = plan_id ) pac_plano, 
        pac_id_convenio, 
        pac_id_plano,
        pac_status 
        FROM   paciente 
        WHERE pac_id = '$pac_id'";
        $result = mysqli_query($conn, $query);
        $row = mysqli_fetch_array($result);

        if (isset($_GET['control'])) {
            $control =  utf8_decode($_GET['control']);
        }else{
            $control = '1';
        }
        $queryAgenda = "SELECT   agd.agd_id
        , prof.prof_nome
        , pac.pac_nome
        , pac.pac_id
        , agd.agd_tel_celulcar
        , agd.agd_tel_residencia
        , agd.agd_data
        , agd.agd_hora_inicial
        , agd.agd_hora_final
        , agd.agd_status
        FROM agendamento agd
        , paciente pac
        , profissional prof
        WHERE agd.agd_id_pac = pac.pac_id
        and   agd.agd_id_prof= prof.prof_id
        and   agd.agd_status  not in ('A','R')
        and   pac.pac_id = '$pac_id'
        order by agd.agd_data , agd.agd_hora_inicial
        asc";
        $resultAgenda = mysqli_query($conn, $queryAgenda);

        $sqlLaudo = "SELECT
        agd.agd_id
        , pront.pront_id
        , prof.prof_nome
        , pac.pac_nome
        , pac.pac_id
        , agd.agd_tel_celulcar
        , agd.agd_tel_residencia
        , agd.agd_data
        , agd.agd_hora_inicial
        , agd.agd_hora_final
        , agd.agd_status
        , pront.pront_laudo
        , pront.pront_status
        from
        prontuario pront
        ,paciente pac
        ,profissional prof
        ,agendamento agd
        where
        pront.pront_agd_id  = agd.agd_id
        and pront.pront_prof_id = prof.prof_id
        and agd.agd_id_pac = pac.pac_id
        and agd.agd_id_pac = '$pac_id'
        and pront.pront_status <> 'X'
        order by pront.pront_id DESC";
        $resultLaudo = mysqli_query($conn, $sqlLaudo);

        $queryConv = "SELECT * FROM convenio where conv_status = 'A' order by 1 asc";
        $resultConv = mysqli_query($conn, $queryConv);


        $queryPlano = "SELECT * FROM plano where plan_status = 'A' order by 1 asc";
        $resultPlan = mysqli_query($conn, $queryPlano);

        function situacao($args){
            switch ($args) {
                case 'M':
                $situacao =  '<span class="label label-success label-sm"><b>MARCADO</b></span>';
                break;
                case 'C':
                $situacao = '<span class="label label-info label-sm"><b>CONFIRMADO</b></span>';
                break;
                case 'AG':
                $situacao = '<span class="label label-warning label-sm"><b>AGUARDANDO</b></span>';
                break;
                case 'EM':
                $situacao = '<span class="label label-info label-sm"><b>EM ATENDIMENTO</b></span>';
                break;
                case 'A':
                $situacao = '<span class="label label-danger label-sm"><b>ATENDIDO</b></span>';
                break;
            }
            echo $situacao;
        }
        function calcula_idade($data_nascimento){
            $mes = date("m");
            $ano = date("Y");
            $dia = date("d");
            $data  = $data_nascimento;
            $teste = explode("/", $data);
            $dia1  = $teste[0];
            $mes1  = $teste[1];
            $ano1  = $teste[2];
            $anob = $ano - $ano1 ;
            $mesb = $mes - $mes1 ;
            $diab = $dia - $dia1 ;
            echo $anob. " ANOS, ".$mesb." MESES E ".abs($diab)." DIAS" ;
        }
        ?>
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE BAR -->
                <?php include 'breadcrumb.php'; ?>
                <!-- END PAGE BAR -->
                <!-- BEGIN PAGE TITLE-->
                <h3 class="page-title"> DADOS DO PACIENTE
                </h3>
                <!-- END PAGE TITLE-->
                <div class="profile">
                    <div class="tabbable-line tabbable-full-width">
                        <ul class="nav nav-tabs">
                            <li class="tab_1_1">
                                <a href="#tab_1_1" data-toggle="tab"> Dados </a>
                            </li>
                            <li class="tab_1_2">
                                <a href="#tab_1_2" data-toggle="tab"> Ajustes </a>
                            </li>
                            <li class="tab_1_3">
                                <a href="#tab_1_3" data-toggle="tab"> Prontuário </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <?php include 'perfil_paciente_tb1.php' ?>
                            <?php include 'perfil_paciente_tb2.php' ?>
                            <?php include 'perfil_paciente_tb3.php' ?>
                        </div>
                    </div>
                </div>
                <!-- END PAGE HEADER-->
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->
    <?php include 'footer.php'; ?>
    <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="includes/busca_cep.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
    <script src="assets/apps/scripts/form-input-mask.js" type="text/javascript"></script>
    <script src="js/tinymce/tinymce.min.js"></script>
    <script src="js/model/paciente.js" type="text/javascript"></script>
    <script src="js/model/prontuario.js" type="text/javascript"></script>
    <script src="js/model/prontuario_edt.js" type="text/javascript"></script>
    <script src="js/model/prontuario_agd.js" type="text/javascript"></script>
    <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            $("#carregandoCep").hide();
            $("#carregandoLaudo").hide();
            $("#idPais option[value='<?php echo $row['pac_end_pais']; ?>']").attr('selected','selected');
            $("#pac_id_plano").prop("disabled", true);
            switch ('<?php echo $control; ?>') {
                case '1':
                $('.tab_1_1').addClass("active");
                $('#tab_1_1').addClass("active");
                break;
                case '2':
                $('.tab_1_2').addClass("active");
                $('#tab_1_2').addClass("active");
                break;
                case '3':
                $('.tab_1_3').addClass("active");
                $('#tab_1_3').addClass("active");
                break;
            }
            $('.cAtender').click(function(){
                var cod = $(this).attr('data-cod');
                var mConsulta = $(this).attr('data-mConsulta');
                if (cod!='' && cod!=null && cod!=undefined ) {
                    $('#agd_id').val(cod);
                    $('#mConsulta').html(mConsulta)
                }
            });
            $('.cEditLaudo').click(function(){
                $('#mostraLaudo').html('');
                var cod = $(this).attr('data-cod');
                var mConsulta = $(this).attr('data-mConsulta');
                if (cod!='' && cod!=null && cod!=undefined ) {
                    $('#agd_id_laudo').val(cod);
                    $('#mConsultaLaudo').html(mConsulta)
                    $.ajax({
                        url: "includes/carregar_campos_edt_laudo.php",
                        type: "POST",
                        data: {pront_id:cod},
                        success: function(data) {
                            $("#carregandoLaudo").hide();
                            $('#mostraLaudo').html(data);
                            tinymce.init({
                                selector:'textarea#pront_laudoEdit',
                                language: 'pt_BR'
                            });
                        },
                        error: function(data) {
                            console.log(data + 'Erro')
                        },
                        beforeSend: function () {
                            $("#carregandoLaudo").show();
                        }
                    });
                }
            });
            $('.excluir_laudo').click(function(){
                $('#Mpergunta').modal('show');
                var cod = $(this).attr('data-cod');
                $("#enviaForm").attr('data-codAgd', cod);
            });
            $('#enviaForm').click(function(){
                var cod = $(this).attr('data-codAgd');
                var formAction = 'M';
                var status = 'X';
                if (cod!='' && cod!=null && cod!=undefined ) {
                    console.log(cod);
                    $.ajax({
                        method: "POST",
                        url: "model/prontuario.php",
                        data: { pront_id: cod , tipoForm: formAction, pront_status:status},
                        success: function( data ) {
                            $('#Malterado').modal('show');
                        },
                        error: function (){
                        }
                    });
                }
            });

            $( "#pac_id_convenio" ).change(function() {
              var pac_id_conv = $(this).val();
              $('#pac_id_plano').html(' ');
              $.ajax({
                  method: "POST",
                  url: "includes/carregar_campos_plano.php",
                  data: { pac_id_conv: pac_id_conv},
                  success: function( data ) {
                    $('#pac_id_plano').html('<option disabled selected>Plano do paciente</option>');
                    $("#pac_id_plano").prop("disabled", false); 
                    var data = $.parseJSON(data);
                    $.each(data, function(index) {
                        $('#pac_id_plano').append("<option value="+data[index].plan_id+">"+data[index].plan_nome+"</option>")
                    });
                },
                error: function (){
                }
            }); 
          });
        });
function enviarForm(){
    $('#form_sample_1').submit();
}
function enviarFormLaudo(){
    tinyMCE.triggerSave();
    $('#form_laudo').submit();
}
function enviarFormLaudoEdt(){
    tinyMCE.triggerSave();
    $('#form_laudoEdt').submit();
}
function enviarFormLaudoAgd(){
    tinyMCE.triggerSave();
    $('#form_laudoAgd').submit();
}
function concluidoAlterar(){
    window.location.href = "perfil_paciente.php?paciente=<?php echo $pac_id; ?>";
}
function PrintElem(elem)
{
    Popup($(elem).html());
}
function Popup(data)
{
    var mywindow = window.open('', 'Prontuário Médico', 'height=600,width=800');
    mywindow.document.write('<html><head><title> INSTITUTO DE GENÉTICA E HEMATOLOGIA </title>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body><br><br>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');
        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
        mywindow.close();
        return true;
    }
</script>
<script>
    tinymce.init({
        selector:'textarea#pront_laudo',
        language: 'pt_BR'
    });
    tinymce.init({
        selector:'textarea#pront_laudoAgd',
        language: 'pt_BR'
    });
</script>
</body>
</html>