 $(document).ready(function() {
 			$(".cep").blur(function() {
		    if ($(this).val() != "") {
		        $.ajax({
		            url: "https://viacep.com.br/ws/"+ $(this).val() +"/json/?callback=?",
		            type: "POST",
		            dataType: "JSON",
		            success: function(data) {
		                $("#cidade").val(data.localidade);
		                $("#uf").val(data.uf);
		                $("#endereco").val(data.logradouro);
		                $("#bairro").val(data.bairro);
		                 $("#carregandoCep").hide();
		            },

		            error: function(data) {
		            }, 
		            beforeSend: function () {
				       $("#carregandoCep").show();
				    }

		        });

		    }

		});


 });