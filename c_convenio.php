<?php include 'head.php'; ?>
<link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <!-- BEGIN HEADER -->
    <?php include 'nav-top.php'; ?>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php 

        include 'nav-left.php';
        include 'conexao/config.php';

        if (isset($_GET['conv_id'])) {
            $conv_id =  $_GET['conv_id'];
        }else{
            $conv_id = '';
        }

        $query = "SELECT * FROM convenio where conv_id = '$conv_id'";
        $result = mysqli_query($conn, $query);
        $rowConv = mysqli_fetch_array($result);

        $queryPlano = "SELECT * FROM plano where plan_status = 'A' and plan_conv_id = '$conv_id' order by 1 desc";
        $resultPlano = mysqli_query($conn, $queryPlano);

        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE BAR -->
                <?php include 'breadcrumb.php'; ?>
                <!-- END PAGE BAR -->
                <!-- BEGIN PAGE TITLE-->
                <h3 class="page-title"> ADICIONAR PLANO
                </h3>
                <!-- END PAGE TITLE-->
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5><b>Nome:</b> <?php echo utf8_encode($rowConv['conv_nome']); ?></h5>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <h5><b>REGISTRO:</b> <?php echo utf8_encode($rowConv['conv_registro']); ?></h5>
                                </div>
                                <div class="col-md-3">
                                    <h5><b>CARÊNCIA:</b> <?php echo utf8_encode($rowConv['conv_carencia']); ?></h5>
                                </div>
                            </div>
                            <h3>
                                <hr>
                                <h3 class="form-section">Planos</h3>  <br>
                                <div class="form-group">
                                    <label><b>Plano</b></label>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <form action="#" class="horizontal-form" id="form_sample_1">
                                                <div class="form-group">
                                                    <input type="text" name="plan_nome" id="plan_nome" class="form-control" placeholder="Informe o nome do Plano">
                                                </div>
                                                <input type="hidden" name="plan_conv_id" value="<?php echo $conv_id; ?>">
                                                <input type="hidden" name="tipoForm" value="I">
                                            </form>
                                        </div>
                                        <div class="col-md-2">
                                            <button class="btn green" type="button" onclick="enviarForm()">
                                                <i class="fa fa-plus-circle"></i>
                                                &nbsp;ADICIONAR
                                            </button>
                                        </div>
                                    </div>

                                    <!-- /input-group -->
                                </div>
                                <ul class="list-group">
                                    <?php while($rowPlano = mysqli_fetch_array($resultPlano)){ ?>
                                    <li class="list-group-item"> <?php echo ucfirst(utf8_encode($rowPlano['plan_nome'])); ?>

                                       <!--  <span class="badge badge-danger label label inativar_conv" data-cod="<?php echo $rowPlano['plan_id']; ?>">
                                            <i class="icon-trash"></i>
                                            Excluir 
                                        </span> -->

                                        <button data-cod="<?php echo $rowPlano['plan_id']; ?>" class="btn btn-sm red badge badge-danger inativar_plan" style="padding-bottom: 18px;"> 
                                            Excluir
                                            <i class="icon-trash"></i>
                                        </button>

                                    </li> 
                                    <?php } ?>                                 
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- Modal -->
                    <div class="modal fade" id="mCadastrado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">
                                        ADICIONAR PLANO
                                    </h4>
                                </div>
                                <div class="modal-body" align="center">
                                    <h2>Cadastrado com sucesso !</h2>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="atualizar()">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="Malterado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">
                                        PLANO
                                    </h4>
                                </div>
                                <div class="modal-body" align="center">
                                    <h2>Situação alterada com sucesso !</h2>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="atualizar()">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include 'footer.php'; ?>
        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="js/model/plano.js" type="text/javascript"></script>
        <script>
        $(document).ready(function() {
            $(".nav-item").removeClass('start active open');
            $("#m_configuracao").addClass('start active open');
            $("#m_convenio").addClass('start active open');

                    //inativar Prestador
                    $('.inativar_plan').click(function(){
                        var cod = $(this).attr('data-cod');
                        var formAction = 'M';
                        var status = 'I';
                        console.log('inativar_plan');
                        if (cod!='' && cod!=null && cod!=undefined ) {
                            $.ajax({
                                method: "POST",
                                url: "model/plano.php",
                                data: { plan_id: cod , tipoForm: formAction, plan_status:status},
                                success: function( data ) {
                                    $('#Malterado').modal('show');
                                },
                                error: function (){
                                }
                            });
                        }
                    });

                });
function enviarForm(){
    $('#form_sample_1').submit();
}
function atualizar(){
    location.reload();
}
</script>
</body>
</html>