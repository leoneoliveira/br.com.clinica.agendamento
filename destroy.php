<?php ob_start();
session_start();
session_unset($_SESSION['prof_id']);
session_unset($_SESSION['prof_nome']);
session_destroy();
header('Location: index.php');
?>
<?php	
ob_end_flush();
?>