<!--tab_1_2-->
<div class="tab-pane" id="tab_1_2">
    <div class="row profile-account">
        <div class="col-md-3">
            <ul class="ver-inline-menu tabbable margin-bottom-10">
                <li class="active">
                    <a data-toggle="tab" href="#tab_1-1">
                        <i class="fa fa-cog"></i> Geral </a>
                        <span class="after"> </span>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#tab_2-2">
                            <i class="fa fa-picture-o"></i> Imagem de perfil </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab_3-3">
                                <i class="fa fa-medkit"></i> Convênio </a>
                            </li>
                            <li><br>
                                <li>
                                    <a href="javascript:;" class="btn green" onclick="enviarForm()"> SALVAR AJUSTES </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <div class="tab-content">
                                <div id="tab_1-1" class="tab-pane active">
                                    <form role="form" id="form_sample_1" method="POST" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label class="control-label">Nome</label>
                                            <input type="text" id="firstName" name="pac_nome" value="<?php echo utf8_encode($row['pac_nome']); ?>" class="form-control" placeholder="Infome o nome do Pacinete">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Data de Nascimento</label>
                                                    <input class="form-control" id="mask_date" type="text" placeholder="__/__/____"  name="pac_nasc" value="<?php echo $row['pac_nasc']; ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Email</label>
                                                    <input type="text" id="firstName" name="pac_email" value="<?php echo $row['pac_email']; ?>" class="form-control" placeholder="Infome o email do paciente">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Sexo</label>
                                                    <div class="radio-list">
                                                        <label class="radio-inline">
                                                            <input type="radio" name="pac_sexo" id="pac_sexo1" value="M"    <?php echo ($row['pac_sexo'] == 'M' ) ? ' checked ' : ' '; ?>> Masculino </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="pac_sexo" id="pac_sexo2" value="F" <?php echo ($row['pac_sexo'] == 'F' ) ? ' checked ' : ' '; ?>> Feminino </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">CPF</label>
                                                            <input type="text" id="mask_cpf" name="pac_cpf" value="<?php echo $row['pac_cpf']; ?>" class="form-control" placeholder="Infome o CPF do paciente">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label">Observações</label>
                                                            <textarea class="form-control" rows="4" name="pac_obs"><?php echo utf8_encode($row['pac_obs']); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h3>Telefones</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Celular</label>
                                                            <input type="text" name="pac_tel_celular" value="<?php echo $row['pac_tel_celular']; ?>" class="form-control mask_phone" placeholder="Infome o celular do paciente">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Casa</label>
                                                            <input type="text" id="firstName" name="pac_tel_residencia" value="<?php echo $row['pac_tel_residencia']; ?>" class="form-control mask_tel" placeholder="Infome o telefone do paciente">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Trabalho</label>
                                                            <input type="text" id="firstName" name="pac_tel_trabalho" value="<?php echo $row['pac_tel_trabalho']; ?>" class="form-control mask_tel" placeholder="Infome o telefone empresarial do paciente">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Ramal</label>
                                                            <input type="text" id="firstName" name="pac_tel_ramal" value="<?php echo $row['pac_tel_ramal']; ?>" class="form-control" placeholder="Infome o ramal do paciente">
                                                        </div>
                                                    </div>
                                                </div>
                                                <h3 class="form-section">Endereço</h3>
                                                <div class="row">
                                                    <div class="col-md-6 ">
                                                        <div class="form-group">
                                                            <label>CEP</label>
                                                            <input type="text" class="form-control cep" name="pac_end_cep" value="<?php echo $row['pac_end_cep']; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <div id="carregandoCep">
                                                            <br>
                                                            <img src="img/reload.gif" height="15" width="15" alt="">
                                                            Carregando informações do CEP ...
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 ">
                                                        <div class="form-group">
                                                            <label>Endereço</label>
                                                            <input type="text" class="form-control" id="endereco" name="pac_end_endereco" value="<?php echo $row['pac_end_endereco']; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Numero</label>
                                                            <input type="text" class="form-control" name="pac_end_numero" value="<?php echo $row['pac_end_numero']; ?>">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Complemento</label>
                                                            <input type="text" class="form-control" name="pac_end_complemento" value="<?php echo $row['pac_end_complemento']; ?>">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Cidade</label>
                                                            <input type="text" class="form-control" name="pac_end_cidade" id="cidade" value="<?php echo $row['pac_end_cidade']; ?>">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Bairro</label>
                                                            <input type="text" class="form-control" name="pac_end_bairo"  id="bairro" value="<?php echo $row['pac_end_bairo']; ?>">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Estado</label>
                                                            <input type="text" class="form-control" name="pac_end_estado" id="uf" value="<?php echo $row['pac_end_estado']; ?>">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Pais</label>
                                                            <select class="form-control" id="idPais">
                                                                <?php include 'includes/paises.php'; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <br><br>
                                            </div>
                                            <div id="tab_2-2" class="tab-pane">
                                                <?php if ($row['pac_img'] == null OR $row['pac_img'] == "") {
                                                    $img = 'assets/pages/img/avatars/male.png';
                                                } else {
                                                    $img = 'uploads_foto_perfil/' . $row['pac_img'];
                                                }
                                                ?>
                                                <div class="form-group">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="<?php echo $img; ?>" height="600" width="600" alt=""> </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                            <div>
                                                                <span class="btn red btn-outline btn-file">
                                                                    <span class="fileinput-new"> Imagem de perfil </span>
                                                                    <span class="fileinput-exists"> Alterar </span>
                                                                    <input type="file"  name="file_imagem" id="file_imagem"> </span>
                                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remover </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="tab_3-3" class="tab-pane">
                                                        <h3>Convênio</h3>
                                                        <br>

                                                        <div class="row">
                                                            <div class="col-md-6">

                                                                <div class="form-group">
                                                                    <label for="nome" class="control-label"><b>Convênio</b></label>
                                                                    <div class="input-group select2-bootstrap-prepend">
                                                                        <select id="pac_id_convenio" class="form-control select2" name="pac_id_convenio" >
                                                                            <option disabled selected>Convênio do paciente</option>
                                                                            <?php while($rowConv = mysqli_fetch_array($resultConv)){ ?>
                                                                            <option value="<?php echo $rowConv['conv_id']; ?>"
                                                                                    <?php if($row['pac_id_convenio'] == $rowConv['conv_id']){echo 'selected="selected"';} ?>                                                                       >
                                                                                <?php echo utf8_encode($rowConv['conv_nome']); ?>
                                                                            </option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="nome" class="control-label"><b>Plano</b></label>
                                                                    <div class="input-group select2-bootstrap-prepend">
                                                                        <select id="pac_id_plano" class="form-control select2" name="pac_id_plano" >
                                                                            <option disabled selected>Plano do paciente</option>
                                                                            <?php while($rowPlan = mysqli_fetch_array($resultPlan)){ ?>
                                                                                <option value="<?php echo $rowPlan['plan_id']; ?>"
                                                                                        <?php if($row['pac_id_plano'] == $rowPlan['plan_id']){echo 'selected="selected"';} ?>                                                                       >
                                                                                    <?php echo utf8_encode($rowPlan['plan_nome']); ?>
                                                                                </option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden"  id="tipoForm" name="tipoForm"  value="MUSER">
                                                        <input type="hidden"  id="pac_id" name="pac_id"  value="<?php echo $row['pac_id']; ?>">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end col-md-9-->
                                    </div>
                                </div>
                                <!-- Modal -->
                                <div class="modal fade" id="Mcadastrado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">
                                                    PACIENTE
                                                </h4>
                                            </div>
                                            <div class="modal-body" align="center">
                                                <h2>Alterado com sucesso !</h2>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="concluidoAlterar()">Fechar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>