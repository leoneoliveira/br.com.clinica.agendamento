<?php include 'head.php'; ?>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <!-- BEGIN HEADER -->
    <?php include 'nav-top.php'; ?>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php include 'nav-left.php'; ?>

         <?php include 'conexao/config.php'; 


         $queryPainel = "SELECT 
                      (select count(*) from agendamento where agd_status = 'M') AS MARCADO 
                    , (select count(*) from agendamento where agd_status = 'C') AS CONFIRMADO
                    , (select count(*) from agendamento where agd_status = 'C') AS ATENDIDOS
                    , (select count(*) from agendamento where agd_status = 'M' AND agd_data <  STR_TO_DATE(now(), '%Y-%m-%d')) AS FALTARAM
                    FROM 
                    agendamento
                    group by MARCADO, CONFIRMADO, ATENDIDOS, FALTARAM";
        $resultPainel = mysqli_query($conn, $queryPainel);
        $rowPainel = mysqli_fetch_array($resultPainel);

       $queryNiver = "SELECT pac_nome, pac_nasc  FROM paciente where pac_nasc = STR_TO_DATE(now(), '%Y-%m-%d')";
       $resultNiver = mysqli_query($conn, $queryNiver);

       $queryPacDia = "SELECT    pac_nome
                               , DATE_FORMAT(agd_data, '%d/%m/%Y') AS dataAgd
                               , agd_hora_inicial
                               FROM agendamento
                               , paciente
                               where agendamento.agd_id_pac = paciente.pac_id
                               and agd_data = NOW()";
       $resultPacDia = mysqli_query($conn, $queryPacDia);

        function calcula_idade($data_nascimento){
            $mes = date("m");
            $ano = date("Y");
            $dia = date("d");

            $data  = $data_nascimento;
            $teste = explode("/", $data); 
            $dia1  = $teste[0];
            $mes1  = $teste[1]; 
            $ano1  = $teste[2];

            $anob = $ano - $ano1 ;
            $mesb = $mes - $mes1 ;
            $diab = $dia - $dia1 ;
            echo $anob. " ANOS, ".$mesb." MESES E ".abs($diab)." DIAS" ;
        }

?>


        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE BAR -->
                <?php include 'breadcrumb.php'; ?>
                <!-- END PAGE BAR -->
                <!-- BEGIN PAGE TITLE-->
                <h3 class="page-title"> <b>INSTITUTO DE GENÉTICA E HEMATOLOGIA</b>
                </h3>
                <!-- END PAGE TITLE-->
                <br>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 yellow" href="#">
                            <div class="visual">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup" data-value="<?php echo $rowPainel['MARCADO']; ?>"><?php echo $rowPainel['MARCADO']; ?></span>
                                </div>
                                <div class="desc"> Agendados </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                            <div class="visual">
                                <i class="fa fa-check-circle-o"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup" data-value="<?php echo $rowPainel['CONFIRMADO']; ?>"><?php echo $rowPainel['CONFIRMADO']; ?></span>
                                </div>
                                <div class="desc"> Confirmados </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                            <div class="visual">
                                <i class="fa fa-check-circle"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup" data-value="<?php echo $rowPainel['ATENDIDOS']; ?>"><?php echo $rowPainel['ATENDIDOS']; ?></span>
                                </div>
                                <div class="desc"> Atendidos </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                            <div class="visual">
                                <i class="fa fa-ban"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup" data-value="<?php echo $rowPainel['FALTARAM']; ?>"><?php echo $rowPainel['FALTARAM']; ?></span>
                                </div>
                                <div class="desc"> Faltaram </div>
                            </div>
                        </a>
                    </div>
                </div>                
                <br>
                <!-- BEGIN PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-share font-red-sunglo hide"></i>
                            <span class="caption-subject font-dark bold uppercase">Consultas nos últimos 12 meses</span>
                            <span class="caption-helper">2016</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="site_activities_loading">
                            <img src="assets/global/img/loading.gif" alt="loading" /> </div>
                            <div id="site_activities_content" class="display-none">
                                <div id="site_activities" style="height: 228px;"> </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET-->
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                         <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-share font-red-sunglo hide"></i>
                                    <span class="caption-subject font-dark bold uppercase">Pacientes do dia </span>
                                    <span class="caption-helper"> <?php echo date("d/m/Y") ?> </span>
                                </div>
                            </div>
                            <ul class="list-group">
                            <?php while($rowPacDia = mysqli_fetch_array($resultPacDia)){?>
                                <li class="list-group-item"> <?php echo utf8_encode(ucfirst(strtolower($rowPacDia['pac_nome']))) ?>
                                    <span class="badge badge-success"><b><?php echo $rowPacDia['dataAgd'] . ' - ' .  $rowPacDia['agd_hora_inicial'] ?></b>  </span>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-share font-red-sunglo hide"></i>
                                    <span class="caption-subject font-dark bold uppercase">Aniversariantes do dia </span>
                                    <span class="caption-helper"> <?php echo date("d/m/Y") ?> </span>
                                </div>
                            </div>
                            <ul class="list-group">
                            <?php while($rowNiver = mysqli_fetch_array($resultNiver)){?>
                                <li class="list-group-item"> <?php echo utf8_encode(ucfirst(strtolower($rowNiver['pac_nome']))) ?>
                                    <span class="badge badge-success"><b>
                                    <?php  calcula_idade($rowNiver['pac_nasc']); ?>
                                    </b></span> 
                                </li>
                            <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- END PAGE HEADER-->
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include 'footer.php'; ?>
<script src="assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<?php $leone = 18787; ?>

<script>
$(document).ready(function() {
    $(".nav-item").removeClass('start active open');
    $("#m_home").addClass('start active open');            
    var leone = 123;
});
</script>

<script src="js/grafico-consulta-ano.js" type="text/javascript" charset="utf-8" ></script>
</body>
</html>