<?php include 'head.php';?>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <!-- BEGIN HEADER -->
    <?php include 'nav-top.php'; ?>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php include 'nav-left.php'; ?>
        <!-- END SIDEBAR -->
        <?php 
        include 'conexao/config.php';

        $query = "SELECT * FROM profissional  where prof_tipo = 0 order by 1 desc";
        $result = mysqli_query($conn, $query);
        $total_num_rows = mysqli_num_rows($result);

        function situacao($args){
            switch ($args) {
                case 'A':
                $situacao =  '<span class="label label-success">Ativo';
                break;
                case 'I':
                $situacao = '<span class="label label-info">Inativo</span>';
                break;
                case 'E':
                $situacao = '<span class="label label-danger">Excluído</span>';
                break;
            }

            echo $situacao;
        }
        ?>

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE BAR -->
                <?php include 'breadcrumb.php'; ?>
                <!-- END PAGE BAR -->
                <!-- BEGIN PAGE TITLE-->
                <h3 class="page-title"> PROFISSIONAIS DE SAÚDE                
                </h3>
                <!-- END PAGE TITLE-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <a href="c_profissional_saude.php" class="btn btn-success"><i class="fa fa-plus-circle"></i>
                              &nbsp;ADICIONAR</a>
                          </div>
                          <div class="tools"> </div>
                      </div>
                      <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="tb_lists">
                            <thead>
                                <tr>
                                    <th width="30%"> NOME </th>
                                    <th width="10%"> CONSELHO </th>
                                    <th width="5%"> REGISTRO </th>
                                    <th width="10%"> EMAIL </th>
                                    <th width="10%"> SITUAÇÃO </th>
                                    <th width="2%"> CONFIGURAÇÕES</th>
                                </tr>
                            </thead>
                            <tbody>
                             <?php $cont = 1;
                             while($row = mysqli_fetch_array($result)){ ?>
                             <tr>
                                <td> <?php echo $row['prof_nome']; ?> </td>
                                <td> <?php echo $row['prof_saud_conselho']; ?></td>
                                <td> <?php echo $row['prof_saud_registro']; ?> </td>
                                <td> <?php echo $row['prof_email']; ?> </td>
                                <td align="center"> <?php situacao($row['prof_status']); ?></td>
                                <td align="center">

                                    <!-- opções button -->
                                    <div class="btn-group">
                                      <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                        Opções
                                        <i class="fa fa-cog"></i> 
                                        <span class="caret"></span>
                                    </button>   
                                    <ul class="dropdown-menu" role="menu" style="text-align: left;">
                                        <li data-cod="<?php echo $row['prof_id']; ?>" class="aletar_prof"><a href="#"> <i class="fa fa-edit icon-circle icon-info"></i> Alterar </a></li>
                                        <li data-cod="<?php echo $row['prof_id']; ?>" class="inativar_prof"><a href="#"> <i class="fa fa-ban icon-circle icon-warning"></i> Inativar </a></li>
                                        <li data-cod="<?php echo $row['prof_id']; ?>" class="ativar_prof"><a href="#"> <i class="glyphicon glyphicon-ok icon-circle icon-success"></i> Ativar </a></li>                                                        
                                        <li class="divider"></li>
                                        <li data-cod="<?php echo $row['prof_id']; ?>" class="excluir_prof"><a href="#"> <i class="fa  fa-trash-o icon-circle icon-danger"></i> Excluir </a></li>
                                    </ul>
                                </div>
                                <!-- end opções button -->

                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>

                <!-- Modal -->
                <div class="modal fade" id="Malterado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">
                            <?php 
                            echo "PROFISSIONAL DE SAÚDE";
                            ?>  
                        </h4>
                    </div>
                    <div class="modal-body" align="center">
                        <h2>Situação alterada com sucesso !</h2>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="atualizar()">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Form Para editar -->
        <form id="form_enviar" method="post" accept-charset="UTF-8"  >
         <input type="hidden"  id="controle" name="controle"  value="">               
         <input type="hidden"  id="prof_id" name="prof_id"  value="">
     </form>

 </div>
</div>
<!-- END PAGE HEADER-->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include 'footer.php'; ?>
<script>
$(document).ready(function() {
    $(".nav-item").removeClass('start active open');
    $("#m_usuario").addClass('start active open');
    $("#m_profissional").addClass('start active open');

    $('#tb_lists').dataTable( {
        "pageLength": 10,
        "lengthChange": false,  
                        "order": [[ 3, "desc" ]], //ordena por coluna 
                        "language": {
                            "url": "js/data-tables/dataModificado/Portuguese-Brasil.json" //tradução para português
                        },
                        stateSave: true, //salvar pesquisa em tempo 
                        "searching": true //oculta ou mostra
                    }); 


        //inativar Prestador
        $('.inativar_prof').click(function(){
            var cod = $(this).attr('data-cod');
            var formAction = 'M';
            var status = 'I';
            console.log('inativar_prof');

            if (cod!='' && cod!=null && cod!=undefined ) {

                $.ajax({
                  method: "POST",
                  url: "model/profissional_saude.php",
                  data: { prof_id: cod , tipoForm: formAction, prof_status:status},
                  success: function( data ) {
                    $('#Malterado').modal('show');                    
                },
                error: function (){

                }
            });    
            }
            
        });

        //Ativar Prestador
        $('.ativar_prof').click(function(){
            var cod = $(this).attr('data-cod');
            var formAction = 'M';
            var status = 'A';
            console.log('inativar_prof');

            if (cod!='' && cod!=null && cod!=undefined ) {

                $.ajax({
                  method: "POST",
                  url: "model/profissional_saude.php",
                  data: { prof_id: cod , tipoForm: formAction, prof_status:status},
                  success: function( data ) {
                    $('#Malterado').modal('show');  
                },
                error: function (){

                }
            });    
            }
            
        });

                //Excluir Prestador
                $('.excluir_prof').click(function(){
                    var cod = $(this).attr('data-cod');
                    var formAction = 'M';
                    var status = 'E';
                    console.log('inativar_prof');

                    if (cod!='' && cod!=null && cod!=undefined ) {

                        $.ajax({
                          method: "POST",
                          url: "model/profissional_saude.php",
                          data: { prof_id: cod , tipoForm: formAction, prof_status:status},
                          success: function( data ) {
                            $('#Malterado').modal('show');  
                        },
                        error: function (){

                        }
                    });    
                    }

                });

                        //alterar medico
                        $('.aletar_prof').click(function(){
                            var cod = $(this).attr('data-cod');
                            var form = $('#form_enviar'); 

                            if (cod!='' && cod!=null && cod!=undefined ) {

                                $('#controle').val('M');
                                $('#prof_id').val(cod); 
                                form.attr('action', 'a_profissional_saude.php');                
                                form.submit();
                            }

                        });

                    });

function atualizar(){
    location.reload();
}

</script>
</body>
</html>