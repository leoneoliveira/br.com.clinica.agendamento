<?php include 'head.php'; ?>
<link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <!-- BEGIN HEADER -->
    <?php include 'nav-top.php'; ?>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php include 'nav-left.php'; ?>

        <?php include 'conexao/config.php';


        $query = "SELECT   agd.agd_id
        , prof.prof_nome
        , pac.pac_nome
        , pac.pac_id
        , agd.agd_tel_celulcar
        , agd.agd_tel_residencia
        , agd.agd_data
        , agd.agd_hora_inicial
        , agd.agd_hora_final
        , agd.agd_status
        FROM agendamento agd
        , paciente pac
        , profissional prof 
        WHERE agd.agd_id_pac = pac.pac_id
        and   agd.agd_id_prof= prof.prof_id
        and   agd.agd_status  not in ('A','R','X') 
        order by agd.agd_data
        , agd.agd_hora_inicial 
        asc";

        $result = mysqli_query($conn, $query);

        function situacao($args){
            switch ($args) {
                case 'M':
                $situacao =  '<span class="label label-success"><b>MARCADO</b></span>';
                break;
                case 'C':
                $situacao = '<span class="label label-info"><b>CONFIRMADO</b></span>';
                break;
                case 'AG':
                $situacao = '<span class="label label-warning"><b>AGUARDANDO</b></span>';
                break;
                case 'EM':
                $situacao = '<span class="label label-info"><b>EM ATENDIMENTO</b></span>';
                break;
                case 'A':
                $situacao = '<span class="label label-danger"><b>ATENDIDO</b></span>';
                break;
            }

            echo $situacao;
        }

        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE BAR -->
        <?php include 'breadcrumb.php'; ?>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> PACIENTES AGENDADOS
        </h3>
        <!-- END PAGE TITLE-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <a href="c_agenda.php" class="btn btn-success"><i class="fa fa-plus-circle"></i>
                    &nbsp;ADICIONAR</a>
                </div>
                <div style="float: right;">
                    <form action="home.php" class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-2"><b>Período</b> </label>
                            <div class="col-md-6">
                                <div class="input-group input-large" id="defaultrange_modal">
                                    <input type="text" class="form-control" name="leone">
                                    <span class="input-group-btn">
                                        <button class="btn default date-range-toggle" type="button">
                                        <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="tb_lists">
                    <thead>
                        <tr>
                            <th>ATENDER</th>
                            <th width=""> NOME </th>
                            <th width=""> CONTATO </th>
                            <th width=""> CONSULTA </th>
                            <th width="10%"> SITUAÇÃO </th>
                            <th width="" align="center"> CONFIGURAÇÕES </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php while($row = mysqli_fetch_array($result)){
                        $data = implode("/",array_reverse(explode("-",$row['agd_data']))) .' '.
                        substr($row['agd_hora_inicial'], 0, 5) .' às '. substr($row['agd_hora_final'], 0, 5);
                        ?>
                        <tr>
                            <td>
                                <button type="button" class="btn red btn-outline cAtender" data-toggle="modal" data-target="#atenderM"
                                data-cod="<?php echo $row['agd_id']; ?>"
                                data-mNome="<?php echo utf8_encode($row['pac_nome']); ?>"
                                data-mpacId="<?php echo $row['pac_id']; ?>"
                                data-mConsulta="<?php echo $data; ?>">
                                <i class="fa fa-calendar"></i>
                                </button>
                            </td>
                            <td> <a href="perfil_paciente.php?paciente=<?php echo $row['pac_id']; ?>">
                                <?php echo utf8_encode($row['pac_nome']); ?>
                            </a>
                        </td>
                        <td> <?php echo $row['agd_tel_celulcar']; ?> - <?php echo $row['agd_tel_residencia']; ?> </td>
                        <td> <?php
                            echo $data;
                        ?></td>
                        <td><?php situacao($row['agd_status']); ?></td>
                        <td align="center">
                            <!-- opções button -->
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                Opções
                                <i class="fa fa-cog"></i>
                                <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu" style="text-align: left;">
                                    <li data-cod="<?php echo $row['agd_id']; ?>" class="aletar_agd"><a href="#"> <i class="fa fa-edit icon-circle icon-info"></i> Remarcar </a></li>
                                    <li class="divider"></li>
                                    <li data-cod="<?php echo $row['agd_id']; ?>" class="excluir_agd"><a href="#"> <i class="fa  fa-trash-o icon-circle icon-danger"></i> Cancelar </a></li>
                                </ul>
                            </div>
                            <!-- end opções button -->
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="atenderM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog" role="document" style="width: 800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">
                    ATENDIMENTO DE PACIENTE
                    </h4>
                </div>
                <div class="modal-body">
                    <b>PACIENTE:</b> <span id="mNome"></span>
                    <br>
                    <b>CONSULTA:</b> <span id="mConsulta"></span>
                    <br><br>
                    <form class="horizontal-form" id="form_laudo" method="POST" enctype="multipart/form-data">
                        <input type="hidden"  id="controle" name="controle"  value="">
                        <input type="hidden"  id="agd_id" name="pront_agd_id"  value="">
                        <input type="hidden" name="tipoForm" value="I">
                        <textarea id="pront_laudo" name="pront_laudo"></textarea>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" >Fechar</button>
                    <button type="button" class="btn blue" onclick="enviarForm();">Salvar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Form Para editar -->
    <form id="form_enviar" method="post" accept-charset="UTF-8"  >
        <input type="hidden"  id="agd_idEdit" name="agd_id"  value="">
    </form>
    <!-- Modal -->
    <div class="modal fade" id="McadastradoLaudo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">
                    ATENDIMENTO DE PACIENTE
                    </h4>
                </div>
                <div class="modal-body" align="center">
                    <h2>Prontuário médico cadastrado com sucesso.</h2>
                </div>
                <div class="modal-footer">
                    <a type="button" class="btn green" id="perfPac">Verificar histórico do paciente</a>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="atualizar()">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="Malterado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">
                    PACIENTES AGENDADOS
                    </h4>
                </div>
                <div class="modal-body" align="center">
                    <h2>Agendamento cancelado com sucesso !</h2>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="atualizar()">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="Mpergunta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">
                    PACIENTES AGENDADOS
                    </h4>
                </div>
                <div class="modal-body" align="center">
                    <h2>Deseja excluir esse agendamento ?</h2>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green" data-dismiss="modal" id="enviaForm">Sim</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Não</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE HEADER-->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include 'footer.php'; ?>
<script src="assets/global/plugins/moment.min.js" type="text/javascript"></script> 
<script src="assets/apps/scripts/traducao.js" type="text/javascript"></script>    
<script src="assets/apps/scripts/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="js/tinymce/tinymce.min.js"></script>
<script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/model/prontuario.js" type="text/javascript"></script>
<script>
   $(document).ready(function() {
    $(".nav-item").removeClass('start active open');
    $("#m_agenda").addClass('start active open');
    $("#p_agenda").addClass('start active open');


    $('#defaultrange_modal').daterangepicker({
        opens: (App.isRTL() ? 'left' : 'right'),
        locale: {
           format: 'DD/MM/YYYY',  
           applyLabel: 'Aplicar',
           cancelLabel: 'Cancelar',
           fromLabel: 'From',
       },
       separator: ' to ',
       startDate: moment().subtract('days', 5),
       endDate: moment(),
       minDate: '01/01/2012',
       maxDate: '12/31/2018',
   },
   function (start, end) {
    $('#defaultrange_modal input').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
}
);  

    $('#tb_lists').dataTable( {
        "pageLength": 10,
        "lengthChange": false,  
                        "order": [[ 3, "desc" ]], //ordena por coluna 
                        "language": {
                            "url": "js/data-tables/dataModificado/Portuguese-Brasil.json" //tradução para português
                        },
                        stateSave: true, //salvar pesquisa em tempo 
                        "searching": true //oculta ou mostra
                    }); 

                                //Remarcar Agendamento
                                $('.aletar_agd').click(function(){
                                    var cod = $(this).attr('data-cod');
                                    var form = $('#form_enviar'); 

                                    if (cod!='' && cod!=null && cod!=undefined ) {                                        
                                        $('#agd_idEdit').val(cod); 
                                        form.attr('action', 'a_agenda.php');                
                                        form.submit();
                                    }

                                });


                                $('.cAtender').click(function(){

                                    var cod = $(this).attr('data-cod');   
                                    var mNome = $(this).attr('data-mNome');  
                                    var mConsulta = $(this).attr('data-mConsulta');
                                    var mpacId = $(this).attr('data-mpacId');  

                                    if (cod!='' && cod!=null && cod!=undefined ) {
                                        $('#agd_id').val(cod);
                                        $('#mNome').html(mNome)
                                        $('#mConsulta').html(mConsulta) 
                                        url_perfil_pac(mpacId)
                                    }
                                });

                                // Regra para fazer pegunta se pode excluir
                                $('.excluir_agd').click(function(){
                                    $('#Mpergunta').modal('show'); 
                                    var cod = $(this).attr('data-cod');
                                    $("#enviaForm").attr('data-codAgd', cod);
                                });
                                $('#enviaForm').click(function(){
                                    var cod = $(this).attr('data-codAgd');
                                    var formAction = 'M';
                                    var status = 'X';

                                    if (cod!='' && cod!=null && cod!=undefined ) {

                                        $.ajax({
                                          method: "POST",
                                          url: "model/agendamento.php",
                                          data: { agd_id: cod , tipoForm: formAction, agd_status:status},
                                          success: function( data ) {
                                            $('#Malterado').modal('show');  
                                        },
                                        error: function (){
                                        }
                                    });    

                                    }
                                });



     });// Fim document

$('#defaultrange_modal').on('apply.daterangepicker', function(ev, picker) {
  alert(picker.startDate.format('YYYY-MM-DD'));
  alert(picker.endDate.format('YYYY-MM-DD'));
});

function enviarForm(){
    tinyMCE.triggerSave();
    $('#form_laudo').submit();
}
function url_perfil_pac(idPac){
 $("#perfPac").attr("href", "perfil_paciente.php?control=3&paciente="+idPac);
}
function atualizar(){
    location.reload();
}
</script>
<script>tinymce.init({ 
    selector:'textarea',
    language: 'pt_BR'
});</script>
</body>
</html>