-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 02/02/2017 às 09:57
-- Versão do servidor: 5.5.51-38.2
-- Versão do PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `leone237_dbacli_clinica`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `agendamento`
--

CREATE TABLE IF NOT EXISTS `agendamento` (
  `agd_id` int(10) NOT NULL,
  `agd_id_pac` int(10) NOT NULL,
  `agd_tel_celulcar` varchar(15) NOT NULL,
  `agd_tel_residencia` varchar(15) NOT NULL,
  `agd_id_convenio` int(10) NOT NULL,
  `agd_id_plano` int(10) NOT NULL,
  `agd_id_prof` int(10) NOT NULL,
  `agd_data` date NOT NULL,
  `agd_hora_inicial` time NOT NULL,
  `agd_hora_final` time NOT NULL,
  `agd_obs` varchar(500) NOT NULL,
  `agd_status` varchar(10) NOT NULL COMMENT '//  STATUS // M  - MARCADO // C  - CONFIRMADO // AG - AGUARDANDO // EM - EM ATENDIMENTO // A  - ATENDIDO ',
  `agd_dt_add` datetime NOT NULL,
  `agd_dt_alt` datetime NOT NULL,
  `agd_user_add` varchar(10) NOT NULL,
  `agd_user_alt` varchar(10) NOT NULL,
  `agd_id_remarcada` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 COMMENT='Agendamento de Consultas';

--
-- Fazendo dump de dados para tabela `agendamento`
--

INSERT INTO `agendamento` (`agd_id`, `agd_id_pac`, `agd_tel_celulcar`, `agd_tel_residencia`, `agd_id_convenio`, `agd_id_plano`, `agd_id_prof`, `agd_data`, `agd_hora_inicial`, `agd_hora_final`, `agd_obs`, `agd_status`, `agd_dt_add`, `agd_dt_alt`, `agd_user_add`, `agd_user_alt`, `agd_id_remarcada`) VALUES
(1, 48, '', '', 0, 0, 3, '2016-11-07', '16:40:00', '17:00:00', '', 'M', '2016-09-21 16:39:53', '0000-00-00 00:00:00', '3', '', 0),
(2, 1, '', '', 0, 0, 3, '2016-09-22', '07:55:00', '08:00:00', '', 'A', '2016-09-22 07:52:05', '2016-09-22 07:52:36', '3', '3', 0),
(3, 1, '', '', 0, 0, 3, '2016-10-22', '07:55:00', '08:00:00', '', 'A', '2016-09-22 07:54:19', '2016-09-22 08:27:58', '3', '3', 0),
(4, 49, '', '', 0, 0, 3, '2016-09-22', '08:05:00', '08:30:00', '', 'M', '2016-09-22 08:01:44', '0000-00-00 00:00:00', '3', '', 0),
(5, 49, '', '', 0, 0, 3, '2016-09-30', '08:15:00', '08:30:00', '', 'M', '2016-09-22 08:12:02', '0000-00-00 00:00:00', '3', '', 0),
(6, 69, '', '', 0, 0, 3, '2016-12-12', '07:00:00', '10:30:00', 'EXAMES ', 'M', '2016-09-28 10:07:28', '0000-00-00 00:00:00', '3', '', 0),
(7, 70, '', '', 0, 0, 3, '2016-10-28', '10:35:00', '11:00:00', '', 'M', '2016-09-28 10:36:00', '0000-00-00 00:00:00', '3', '', 0),
(8, 127, '', '', 0, 0, 3, '2016-10-17', '13:30:00', '17:00:00', '', 'M', '2016-10-10 16:35:38', '0000-00-00 00:00:00', '3', '', 0),
(9, 49, '', '', 0, 0, 3, '2016-11-21', '14:05:00', '14:30:00', '', 'A', '2016-11-21 14:03:09', '2016-11-21 14:04:13', '3', '3', 0),
(10, 200, '', '', 0, 0, 3, '2016-12-06', '06:28:42', '06:28:42', '', 'A', '2016-12-06 10:28:42', '0000-00-00 00:00:00', '3', '', 0),
(11, 205, '', '', 0, 0, 3, '2016-12-21', '05:49:40', '05:49:40', '', 'A', '2016-12-21 09:49:40', '0000-00-00 00:00:00', '3', '', 0),
(12, 250, '', '', 0, 0, 3, '2017-02-01', '11:25:00', '11:30:00', '', 'M', '2017-02-01 11:22:08', '0000-00-00 00:00:00', '3', '', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `convenio`
--

CREATE TABLE IF NOT EXISTS `convenio` (
  `conv_id` int(10) NOT NULL,
  `conv_nome` varchar(50) DEFAULT NULL,
  `conv_registro` varchar(50) DEFAULT NULL,
  `conv_carencia` varchar(50) DEFAULT NULL,
  `conv_status` varchar(2) DEFAULT NULL,
  `conv_dt_add` datetime DEFAULT NULL,
  `conv_dt_alt` datetime DEFAULT NULL,
  `conv_user_add` varchar(50) DEFAULT NULL,
  `conv_user_alt` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `paciente`
--

CREATE TABLE IF NOT EXISTS `paciente` (
  `pac_id` int(10) NOT NULL,
  `pac_nome` varchar(100) NOT NULL,
  `pac_nasc` varchar(10) NOT NULL,
  `pac_cpf` varchar(20) NOT NULL,
  `pac_email` varchar(50) NOT NULL,
  `pac_sexo` varchar(1) NOT NULL,
  `pac_img` varchar(100) NOT NULL,
  `pac_obs` varchar(500) NOT NULL,
  `pac_tel_celular` varchar(20) NOT NULL,
  `pac_tel_residencia` varchar(20) NOT NULL,
  `pac_tel_trabalho` varchar(20) NOT NULL,
  `pac_tel_ramal` varchar(20) NOT NULL,
  `pac_end_cep` varchar(10) NOT NULL,
  `pac_end_endereco` varchar(50) NOT NULL,
  `pac_end_numero` varchar(10) NOT NULL,
  `pac_end_complemento` varchar(50) NOT NULL,
  `pac_end_cidade` varchar(50) NOT NULL,
  `pac_end_bairo` varchar(20) NOT NULL,
  `pac_end_estado` varchar(20) NOT NULL,
  `pac_end_pais` varchar(20) NOT NULL,
  `pac_id_convenio` varchar(10) DEFAULT NULL,
  `pac_id_plano` varchar(10) DEFAULT NULL,
  `pac_dt_add` datetime NOT NULL,
  `pac_dt_alt` datetime NOT NULL,
  `pac_user_add` varchar(10) NOT NULL,
  `pac_user_alt` varchar(10) NOT NULL,
  `pac_status` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=254 DEFAULT CHARSET=latin1 COMMENT='Tabela de Cadastro de Pacientes';

--
-- Fazendo dump de dados para tabela `paciente`
--

INSERT INTO `paciente` (`pac_id`, `pac_nome`, `pac_nasc`, `pac_cpf`, `pac_email`, `pac_sexo`, `pac_img`, `pac_obs`, `pac_tel_celular`, `pac_tel_residencia`, `pac_tel_trabalho`, `pac_tel_ramal`, `pac_end_cep`, `pac_end_endereco`, `pac_end_numero`, `pac_end_complemento`, `pac_end_cidade`, `pac_end_bairo`, `pac_end_estado`, `pac_end_pais`, `pac_id_convenio`, `pac_id_plano`, `pac_dt_add`, `pac_dt_alt`, `pac_user_add`, `pac_user_alt`, `pac_status`) VALUES
(1, 'leone pinheiro de oliveira', '21/03/1992', '088.301.844-62', '', 'M', '', '', '', '', '', '', '50610-400', 'Rua Campos Sales', '', '', 'Recife', 'Madalena', 'PE', 'Brasil', NULL, NULL, '2016-09-11 22:48:12', '0000-00-00 00:00:00', 'tss', '', 'A'),
(2, 'MARCOS GOMES DE ARAUJO ', '16/05/2012', '000.000.000-00', '', 'M', '', 'PACIENTE HIPERATIVO E COM ADNPM.\r\nEM USO DE NEULEPTIL E RESPERIDON ', '(87) 9.8839-0352', '', '', '', '', '', '', '', 'PETROLINA ', '', '', 'Brasil', NULL, NULL, '2016-09-13 10:12:27', '0000-00-00 00:00:00', 'tss', '', 'A'),
(3, 'TATIANE SILVA MONTEIRO', '04/07/1983', '046.716.534-35', '', 'F', '', 'PERDA GESTACIONAL \r\n02 X \r\n1 - 2014 COM 36 SEMANAS / 10 . 11 . 2014 / COM PRÉ NATAL COMPLETO - SEXO FEMININO / ECLAMPSIA? / HELP?\r\n2 - 2015 OUTRA PERDA  - proteína s baixa  42 / 37 SEMANAS E 3 DIAS - 2016 / BPM BAIXO - NATIMORTO ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-13 10:53:46', '0000-00-00 00:00:00', 'tss', '', 'A'),
(4, 'ROBERTA GONÇALVES DOS SANTOS ', '17/06/1945', '275.971.054-87', '', 'M', '', 'MANCHAS PELO CORPO\r\nHAS +\r\nDM + \r\nPEÇO COAGULOGRAMA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-13 11:24:26', '0000-00-00 00:00:00', 'tss', '', 'A'),
(5, 'ANTONIO MARTINS BATISTA NETO ', '08/08/2015', '000.000.000-00', '', 'M', '', 'ADNPM + COLOBOMA + PERDA AUDITIVA + CRIPTORQUIDIA + LIMITAÇÃO ARTICULAR\r\nCGH ARRAY - 22 X 2 / XY  ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-13 12:11:03', '0000-00-00 00:00:00', 'tss', '', 'A'),
(6, 'JOÃO GABRIEL ', '14/06/2009', '000.000.000-00', '', 'M', '', 'Sequenciamento Completo dos Genes TSC1 e TSC2: NÃO ALTERADOS\r\nPELO COMPORTAMENTO DISPERSO FAREI CARIÓTIPO \r\nDEPOIS DE FMR 1 X FRA E CGH ARRAY', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-13 12:23:43', '0000-00-00 00:00:00', 'tss', '', 'A'),
(7, 'CLAUDIA CANDIDA C SANTOS ', '10/07/1977', '000.000.000-00', '', 'F', '', 'MANCHA NA PELE OCASIONAL \r\nFAZ USO DE ESCITALOPRAN / RIVOTRIL / LABIRIN\r\nHAS - / DM - \r\nTROMBOFILIA FAMILIAR / ABORTOS DE REPETIÇÃO ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-13 12:45:24', '0000-00-00 00:00:00', 'tss', '', 'A'),
(8, 'LUCIA MARA CALADO BEZERRA DE MELO', '20/11/1938', '000.000.000-00', '', 'F', '', 'RETORNA PARA AVALIAR MIELOGRAMA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-13 12:49:25', '0000-00-00 00:00:00', 'tss', '', 'A'),
(9, 'PIRRE MARCEL ', '12/08/2011', '000.000.000-00', 'limeirap@hotmail.com ', 'M', '', 'AUTISMO E SINDROME DE DOWN\r\nCARIÓTIPO 47,XY, +21\r\nPEÇO CGH ARRAY', '(81) 9.8820-2116', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-13 15:15:15', '0000-00-00 00:00:00', 'tss', '', 'A'),
(10, 'PIRRE MARCEL ', '12/08/2011', '000.000.000-00', 'limeirap@hotmail.com ', 'M', '', 'AUTISMO E SINDROME DE DOWN\r\nCARIÓTIPO 47,XY, +21\r\nPEÇO CGH ARRAY\r\nPABLO - 14 ANOS NORMAL ', '(81) 9.8820-2116', '', '', 'CONVENIO AMIL ', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-13 15:34:07', '0000-00-00 00:00:00', 'tss', '', 'A'),
(11, 'ANDRÉ FELIPE OLIVEIRA GONDIM', '18/09/1977', '022.936.854-97', '', 'M', '', 'SUSPEITA DE Síndrome de Ehlers-Danlos\r\nAVALIAÇÃO DA ORTOPEDIA\r\nNEFROLITIASE\r\nPEÇO NUTRICIONISTA', '(81) 9.9975-2240', '', '', '', '', '', '', '', 'OLINDA ', '', '', 'Brasil', NULL, NULL, '2016-09-13 16:07:09', '0000-00-00 00:00:00', 'tss', '', 'A'),
(12, 'REINALDO CHAVES MAIA', '17/07/1977', '759.701.473-20', 'reinaldocmaia@hotmail.com', 'M', '', 'ISQUEMIA DIA 09.03.2016\r\nEM BARBALHA / CE\r\nTRABALHA COM PLATAFORMA DE PETROLEO ', '', '', '', 'SAUDE BRADESCO ', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-13 16:45:13', '0000-00-00 00:00:00', 'tss', '', 'A'),
(13, 'LUCIENE FERRAZ ', '01/01/1962', '000.000.000-00', '', 'F', '', 'MIELOGRAMA - LEUCOPENIA / TIREOIDITE DE HASHIMOTO ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 09:56:18', '0000-00-00 00:00:00', '3', '', 'A'),
(14, 'COSMA TENORIO ', '01/01/1950', '000.000.000-00', '', 'F', '', 'MIELOMA + ARBOVIROSE / CHIKUNGUNYA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 09:57:01', '0000-00-00 00:00:00', '3', '', 'A'),
(15, 'EIDE COSTA ', '01/01/1980', '000.000.000-00', '', 'F', '', 'PTI CRONICA / RESOLVIDA OU ESTÁVEL ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 09:57:41', '0000-00-00 00:00:00', '3', '', 'A'),
(16, 'SEVERINO FREITAS DA ROCHA ', '19/10/1974', '920.673.874-72', '', 'M', '', 'BIÓPSIA ÓSSEA + IMUNOHISTOQUÍMICA\r\nLINFOMA?', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 10:17:19', '0000-00-00 00:00:00', '3', '', 'A'),
(17, 'MARINA ARAUJO ROSAS', '27/03/1983', '041.798.864-80', 'marinaarosas@gmail.com', 'F', '', 'PERDA GESTACIONAL  - ERA GEMELAR - PERDEU COM 8 SEMANAS UM DOS G?MEOS\r\nMORFOLOGIA COM HIDROPSIA FETAL / ICC / ATRESIA ARTERIA PULMOMAR (5 MESES DE GESTA??O)\r\nNATIMORTO EM AGOSTO AOS 7 MESES DA GESTA??O\r\nFEZ PR? NATAL COMPLETO  + ACIDO F?LICO VO \r\n- PROX CONSULTA SOLICITAR EXAMES TROMBOFILIA \r\nCARIOTIPO DO NATIMORTO N?O FEITO\r\nCARIOTIPO PEDIDO P PAULA ARRUDA PARA A PP PACIENTE - PEDIR FOTOS \r\n', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-09-14 10:34:48', '2016-09-14 10:36:29', '3', '3', 'A'),
(18, 'MARIA APARECIDA', '05/12/1960', '245.051.374-49', '', 'F', '', 'BIOPSIA ÓSSEA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 10:45:06', '0000-00-00 00:00:00', '3', '', 'A'),
(19, 'MARLETTE DA SILVA BATISTA', '14/03/1948', '000.000.000-00', '', 'F', '', 'AVALIAÇÃO CLINICA / ANEMIA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 11:12:25', '0000-00-00 00:00:00', '3', '', 'A'),
(20, 'HELCIAS JUDITE DA SILVA LIMA', '01/05/1970', '000.000.000-00', '', 'F', '', 'ANEMIA E PLAQUETOPENIA \r\nTRABALHA COMO AGENTE DE SAUDE E APLICA VENENOS DE RATO.\r\nPEÇO MIELOGRAMA', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 11:18:31', '0000-00-00 00:00:00', '3', '', 'A'),
(21, 'ELIS REGINA ARAUJO DE CASTRO', '16/11/1992', '000.000.000-00', 'simaraujo13@hotmail.com', 'F', '', 'SINDROME DE DOWN \r\nCARIÓTIPO SOLICITO NOVO MIELOGRAMA PARA CONFIRMAÇÃO À CASSI \r\nTEM DOIS FILHOS\r\nIDADE DOS PAIS:\r\n43 ANOS', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 11:31:55', '0000-00-00 00:00:00', '3', '', 'A'),
(22, 'ELIS REGINA ARAUJO DE CASTRO', '16/11/1992', '000.000.000-00', 'simaraujo13@hotmail.com', 'F', '', 'SINDROME DE DOWN \r\nCARIÓTIPO SOLICITO NOVO MIELOGRAMA PARA CONFIRMAÇÃO À CASSI \r\nTEM DOIS FILHOS\r\nIDADE DOS PAIS:\r\n43 ANOS', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 11:36:26', '0000-00-00 00:00:00', '3', '', 'A'),
(23, 'ISABELLY AMAZONAS', '06/07/1988', '000.000.000-00', '', 'F', '', 'REAVALIAÇÃO CLINICA', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 11:40:04', '0000-00-00 00:00:00', '3', '', 'A'),
(24, 'ANA IZABEL FRANÇA', '06/06/1960', '000.000.000-00', 'anaizabelfranca', 'F', '', 'Histórico de câncer na família.', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 11:55:06', '0000-00-00 00:00:00', '3', '', 'A'),
(25, 'WESLEY CLAUDEMIR JOSÉ DOS SANTOS', '02/04/1995', '113.621.474-78', 'wesleybtv@hotmail.com', 'M', '', 'BIOPSIA ÓSSEA\r\nLEUCOPENIA', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 12:09:02', '0000-00-00 00:00:00', '3', '', 'A'),
(26, 'DAVI PACHECO ', '07/01/2014', '000.000.000-00', '', 'M', '', 'ADNPM \r\nPESO: 10 800\r\nALTURA:  \r\nAVALIA??O CL?NICA E GENETICA\r\nRGE \r\n\r\nDADOS +\r\nBAIXA ESTATURA\r\nSINOFRE\r\nORELHAS ANTERIORIZADAS\r\nDEDOS DE MÃOS E PÉS COM IMPLANTAÇÃO ALTERADA\r\nN?O FALA\r\nTESTÍCULO RETRATIL \r\nFEZ PR? NATAL COMPLETO / SEM INTERCORRENCIAS / 38 SEMANAS - MECONIO E HIP?XIA\r\nPAIS N?O CONSANGUINEOS - PAI: 39 ANOS / MAE: 39 ANOS (COMERCIANTES)\r\nFILHOS:\r\nLUANA 10A\r\nLARA 7A\r\n- SEM CRISES CONVULSIVAS\r\n- SEM MEDICA??ES\r\n- BOA NOITE DE SONO\r\n\r\n- RAIO X DE PUNHO E PROXIMA CONSULTA', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-09-14 12:14:39', '2016-09-14 12:26:53', '3', '3', 'A'),
(27, 'MARLUCE OLIVEIRA', '02/07/1945', '000.000.000-00', '', 'M', '', 'EXAMES GERAIS COM ENFOQUE NA FERRITINA\r\nHEMOCROMATOSE\r\nCHECAR H63 D / C282 Y', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 15:17:17', '0000-00-00 00:00:00', '3', '', 'A'),
(28, 'ALINE MENDES ', '01/01/2000', '000.000.000-00', '', 'M', '', 'PTI CRONICA', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 15:36:37', '0000-00-00 00:00:00', '3', '', 'A'),
(29, 'AMARA FRANCISCA DE SENA ', '30/12/1933', '000.000.000-00', '', 'F', '', 'DOR EM MIE HÁ CERCA DE 15 DIAS.\r\nFOI AO SPA ONDE REALIZOU USG DOPPLER DE MIE - SEM TVP \r\nFEZ USO DE PACO QUE FOI SUFIENTE', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 15:56:55', '0000-00-00 00:00:00', '3', '', 'A'),
(30, 'RAFAEL ABREU NUNES DA SILVA', '18/07/2016', '000.000.000-00', '', 'M', '', 'DIMORFISMOS COMPATÍVEIS COM INDROME DE DOWN\r\nFILHO DE CASAL NÃO CONSANGUINEO - SEGUNDO\r\nO PRIMEIRO FILHO TEM 03 ANOS / HEITOR\r\nPAI 33 ANOS\r\nMAE 35 ANOS / SAF: + / CLEXANE SC\r\nPRÉ NATAL COMPLETO SEM INTERCORRENCIAS\r\nTN ALTERADA\r\nPARTO CESAREANA HMSJ\r\nPESO: 3450\r\nALT: 51 CM \r\nFENDA PALPEBRAL OBLIQUA PARA CIMA\r\nHIPOTONIA\r\n ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 16:06:20', '0000-00-00 00:00:00', '3', '', 'A'),
(31, 'RAFAEL ABREU NUNES DA SILVA', '18/07/2016', '000.000.000-00', '', 'M', '', 'DIMORFISMOS COMPATÍVEIS COM SINDROME DE DOWN\r\nFILHO DE CASAL NÃO CONSANGUINEO - SEGUNDO\r\nO PRIMEIRO FILHO TEM 03 ANOS / HEITOR\r\nPAI 33 ANOS\r\nMAE 35 ANOS / SAF: + / CLEXANE SC\r\nPRÉ NATAL COMPLETO SEM INTERCORRENCIAS\r\nTN ALTERADA\r\nPARTO CESAREANA HMSJ\r\nPESO: 3450\r\nALT: 51 CM \r\nFENDA PALPEBRAL OBLIQUA PARA CIMA\r\nHIPOTONIA\r\n CARIÓTIPO 47, XY, + 21 ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 16:32:51', '0000-00-00 00:00:00', '3', '', 'A'),
(32, 'RAFAELA LIMA DA SILVA', '15/01/1988', '065.665.934-38', '', 'M', '', 'HISTORICO DE TVP / ABORTOS\r\nEM USO DE CLEXANE NA 13 SEMANA DE GRAVIDEZ\r\nTN NORMAL!\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 16:34:14', '0000-00-00 00:00:00', '3', '', 'A'),
(33, 'RAFAELA LIMA DA SILVA', '15/01/1988', '065.665.934-38', '', 'F', '', 'HISTORICO DE TVP / ABORTOS\r\nEM USO DE CLEXANE NA 13 SEMANA DE GRAVIDEZ\r\nTN NORMAL!\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 16:34:53', '0000-00-00 00:00:00', '3', '', 'A'),
(34, 'THAYS MORGANA ', '05/06/1992', '101.544.224-28', 'thaysmbrandao@gmail.com', 'F', '', 'Evolui bem após medicação \r\nSem febre\r\nTonturas eventuais\r\nFEN\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 16:38:46', '0000-00-00 00:00:00', '3', '', 'A'),
(35, 'THAYS MORGANA ', '05/06/1992', '101.544.224-28', 'thaysmbrandao@gmail.com', 'F', '', 'Evolui bem após medicação \r\nSem febre\r\nTonturas eventuais\r\nFEN\r\nPEÇO EDA / AJUSTE DE FERRO ORAL \r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 16:42:39', '0000-00-00 00:00:00', '3', '', 'A'),
(36, 'CARLOS ALBERTO DA SILVA', '13/03/1948', '075.208.404-68', '', 'M', '', 'FERRITINA ELEVADA\r\nPEÇO PERFIL DO FERRO SÉRICO / NUTRICIONISTA\r\nRNM DE FÍGADO COM MEDIÇÃO DE CARGA DE FERRO ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 16:46:26', '0000-00-00 00:00:00', '3', '', 'A'),
(37, 'CARLOS ALBERTO DA SILVA', '13/03/1948', '075.208.404-68', '', 'M', '', 'FERRITINA ELEVADA\r\nPEÇO PERFIL DO FERRO SÉRICO / NUTRICIONISTA\r\nRNM DE FÍGADO COM MEDIÇÃO DE CARGA DE FERRO - APOS USG FIGADO E VB\r\nEM BREVE C282Y e H63D', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 16:49:20', '0000-00-00 00:00:00', '3', '', 'A'),
(38, 'DIRCEU GOMES ', '01/01/1960', '000.000.000-00', '', 'M', '', 'LMC \r\nCARIÓTIPO NORMAL\r\nBCR / ABL1  14 ,5 %\r\n5%', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-09-14 17:24:16', '2016-11-14 16:24:19', '3', '3', 'A'),
(39, 'ISABELLA DE MEDEIROS VALERA ', '01/08/2002', '000.000.000-00', 'rena_varella@hotmail.com', 'F', '', 'GENE CACNAS1\r\nPARA MIOTONIA NEGATIVO\r\n----\r\nSOLICITO SEQUENCIAMENTO COMPLETO DO EXOMA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-14 17:26:44', '0000-00-00 00:00:00', '3', '', 'A'),
(40, 'MARIA FRANCISCA DA SILVA', '08/05/1927', '000.000.000-00', '', 'M', '', 'RETORNA PARA AVALIAÇÃO DE EXAMES \r\nPRESCREVO ACIDO FÓLICO / CITONEURIN E ALOPURINOL ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-21 10:32:24', '0000-00-00 00:00:00', '3', '', 'A'),
(41, 'MARIA MADALENA DA SILVA AMORIM', '31/08/1951', '000.000.000-00', '', 'F', '', 'AVALIAÇÃO DE EXAMES \r\nHb: 11,6\r\nHT: 75,2\r\nGB: 6,139\r\nPLT: 185000\r\nPRESCREVO LUFTAL E USG (CHECAR HERNIA)', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-21 10:42:38', '0000-00-00 00:00:00', '3', '', 'A'),
(42, 'JOSÉ EVANGELISTA SILVA NETO ', '16/01/1968', '000.000.000-00', '', 'M', '', 'LINFOMA NÃO HODGKIN CD 20 +\r\nAGUARDO MABTHERA \r\nBACTRIM POR 02 ANOS ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-21 10:58:54', '0000-00-00 00:00:00', '3', '', 'A'),
(43, 'ZENILDA MARTINS', '09/10/1960', '000.000.000-00', '', 'F', '', 'EXAMES NORMAIS \r\nUSG ABDOME TOTAL - ESEATOSE\r\nFARÁ COLONOSCOPIA\r\nFEZ EDA \r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-21 15:16:03', '0000-00-00 00:00:00', '3', '', 'A'),
(44, 'MARIA DE LOURDES DE AMORIM ', '16/05/1954', '000.000.000-00', '', 'F', '', 'MIELOGRAMA \r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-21 15:34:37', '0000-00-00 00:00:00', '3', '', 'A'),
(45, 'DEBORAH ELENA LARA', '11/03/1992', '000.000.000-00', '', 'F', '', 'FILHA DE  LUCIANO DAVID\r\nTEM MTHFR HOMOZIGOTO MUTANTE\r\n\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-21 15:59:39', '0000-00-00 00:00:00', '3', '', 'A'),
(46, 'LUCIANO DAVID ', '29/12/1958', '000.000.000-00', '', 'M', '', 'TVP \r\nPAI DE DEBORAH ELENA (MTHFR - HOMOZIGOSE)', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-21 16:11:20', '0000-00-00 00:00:00', '3', '', 'A'),
(47, 'ZULEIDE ELIZIA DA SILVA ', '20/06/1974', '000.000.000-00', '', 'F', '', 'ATAXIA ESPINOCEREBELAR ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-21 16:34:14', '0000-00-00 00:00:00', '3', '', 'A'),
(48, 'ANA CAROLINA TERRA ', '29/05/1973', '000.000.000-00', '', 'F', '', 'LMC\r\nPEDIDO FEITO DE BCR / ABL QUANTITATIVO \r\nPACIENTE ESQUIZOFRÊNICA ', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-09-21 16:35:17', '2016-09-21 16:36:05', '3', '3', 'A'),
(49, 'ALANA LEITE SOUZA', '24/11/1993', '000.000.000-00', '', 'F', '', 'MÃE FALECIDA DE CANCER DE MAMA / ARACAJU - SE\r\nTIA COM CA DE MAMA\r\nFAMILIA MATERNA COM ESQUIZOFRENIA\r\n----------------\r\nFAMILIA PATERNA COM ESQUIZOFRENIA\r\n\r\n---------------\r\nPRÉ NATAL COMPLETO\r\nPARTO NORMAL\r\nADNMP\r\n--------------\r\nPEÇO CARIÓTIPO ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-21 16:50:53', '0000-00-00 00:00:00', '3', '', 'A'),
(50, 'LUANA DE OLIVEIRA SILVA', '28/04/1987', '000.000.000-00', '', 'F', '', 'ANEMIA HÁ ANOS.\r\nCOLECISTECTOMIA EM 2003.\r\nESFEROCITOSE HEREDITÁRIA\r\nMÃE 52 ANOS SAUDÁVEL / PAI 54 ANOS ANOS\r\nNÃO CONSAGUINEOS\r\nMENARCA AOS 11 ANOS\r\nTEM FLUXO MENSTRUAL ALTO \r\nEM USO DE TAMISA 30 /  POR 21 DIAS  \r\nHb: 11,9 / HT: 33\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-21 17:18:50', '0000-00-00 00:00:00', '3', '', 'A'),
(51, 'DJANIRA CANDIDA DOS SANTOS ', '05/07/1948', '000.000.000-00', '', 'F', '', 'DORES PELO CORPO\r\nLEUCOPENIA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-21 17:30:56', '0000-00-00 00:00:00', '3', '', 'A'),
(52, 'THIAGO ALVES MAGNO ', '18/04/1998', '000.000.000-00', '', 'M', '', 'PACIENTE EM USO DE ACIDO FÓLICO\r\nPESO 50 KG\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-21 17:34:41', '0000-00-00 00:00:00', '3', '', 'A'),
(53, 'RAUL HENRIQUE CARVALHO PALACIO FERREIRA ', '04/01/2007', '000.000.000-00', '', 'M', '', 'BAIXA ESTATURA', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-21 17:36:15', '0000-00-00 00:00:00', '3', '', 'A'),
(54, 'SEVERINO SOARES DE LIMA ', '29/05/1951', '000.000.000-00', '', 'M', '', 'HIPOPLASIA MEDULAR\r\nREVISÃO PARA DOSAGEM DE ACIDO FÓLICO / VIT B12\r\nMARCADORES TUMORAIS', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-27 09:31:23', '0000-00-00 00:00:00', '3', '', 'A'),
(55, 'VERA LUCIA SABRA', '20/06/1960', '291.382.804-34', '', 'F', '', 'SANGRAMENTO VAGINAL CONTINUO\r\nHAS - EM USO DE CORUS\r\nTVP - AAS ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-27 09:37:57', '0000-00-00 00:00:00', '3', '', 'A'),
(56, 'AMANDA BARBOSA DE LIMA ', '06/09/1978', '000.000.000-00', '', 'F', '', 'TVP - MTHFR / HETEROZIGOTO\r\nRECOMENDAÇÕES:\r\n- USO DE MEIAS\r\n- PESO ADEQUADO\r\n- CIRURGAIS\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-27 09:47:08', '0000-00-00 00:00:00', '3', '', 'A'),
(57, 'NATALI BARBOSA DE LIMA ', '02/06/1988', '000.000.000-00', '', 'F', '', 'AVALIAÇÃO DE TROMBOFILIA\r\nMTHFR - HETEROZIGOTO', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-27 09:53:55', '0000-00-00 00:00:00', '3', '', 'A'),
(58, 'CLAUDIA DA SILVA GOMES', '15/06/1976', '000.000.000-00', '', 'F', '', 'EM USO DE ACO \r\n- ANEMIA FERROPRIVA\r\nPRESCREVO TRANSAMIN E EXAMES PERFIL DO FERRO \r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-27 10:07:42', '0000-00-00 00:00:00', '3', '', 'A'),
(59, 'LARA KATARINA LIMA SOUZA ', '29/10/2015', '000.000.000-00', '', 'F', '', 'FRAGILIDADE ÓSSEA?\r\n- IDADE DA MÃE 28 ANOS / PAI 45 ANOS - NÃO CONSANGUINEOS / SEM CASOS SIMILARES NA FAMILIA \r\n- TEM UMA FILHA DE 08 ANOS - MONIQUE - NORMAL \r\n- PRE - NATAL / NANISMO / RUBEOLA NA GRAVIDEZ\r\n- AO NASCIMENTO APRESENTOU FRATURAS - OMBRO / COSTELA / MID \r\n- OSTEOGENESE IMPERFEITA ?\r\n- NÃO TEVE NOVAS FRATURAS\r\n- TESTE DA ORELHINHA /  OLHINHO NORMAL\r\n- AVALIAÇÃO CARDIOLOGICA / NEUROLOGICA \r\n', '', '', '', '', '', '', '', '', 'PETROLINA ', '', '', 'Brasil', NULL, NULL, '2016-09-27 10:37:44', '0000-00-00 00:00:00', '3', '', 'A'),
(60, 'SOFIA PEREIRA COLAÇO BITTENCOURT', '03/09/2012', '000.000.000-00', '', 'F', '', 'TEA \r\nEM USO - \r\nPAIS NÃO CONSANGUÍNEOS\r\nPRÉ - NATAL COMPLETO\r\nADNPM  \r\nTERAPIAS: TO / FONO / PSICOMOTRICIDADE RELACIONAL / ABA    ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-27 11:06:00', '0000-00-00 00:00:00', '3', '', 'A'),
(61, 'NADIRA DINIZ DE ALMEIDA ', '31/05/1946', '000.000.000-00', '', 'F', '', 'TROBOCITOSE ESSENCIAL \r\nDEPRESSÃO ANSIOSA', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-27 11:18:20', '0000-00-00 00:00:00', '3', '', 'A'),
(62, 'JOSEFA JOSILEIDE', '01/01/1___', '000.000.000-00', '', 'F', '', 'DISTONIA COM ALTERAÇÃO EM CGH ARRAY', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-27 11:48:20', '0000-00-00 00:00:00', '3', '', 'A'),
(63, 'FABIANA MARIA RIBEIRO DO NASCIMENTO', '20/08/1985', '000.000.000-00', '', 'F', '', 'LAUDO DA AMIL \r\nCA DE MAMA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-27 11:49:21', '0000-00-00 00:00:00', '3', '', 'A'),
(64, 'LAILSON BEZERRA MORENO ', '20/01/1964', '000.000.000-00', '', 'M', '', 'BIOPSIA ÓSSEA / GEAP \r\nLINFOMA HODGKIN \r\nHIV +', '(81) 9.8692-2520', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-27 14:40:53', '0000-00-00 00:00:00', '3', '', 'A'),
(65, 'NANCI DA SILVA FERREIRA ', '03/02/1955', '000.000.000-00', '', 'F', '', 'MIELOMA MULTIPLO IIIA IgG KAPPA \r\nFEZ 05 CICLOS DE QT\r\nSULFA / ACICLOVIR\r\nTALIDOMIDA?\r\nPRÉ - TMO AUTOLOGO', '(81) 9.8742-3447', '(81) 3371-9821', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-27 15:33:36', '0000-00-00 00:00:00', '3', '', 'A'),
(66, 'TÁSSIA SPINELLI DE LIMA ', '01/01/1930', '000.000.000-00', '', 'F', '', 'SMN1 PESQUISA FAMILIAR ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-27 16:19:21', '0000-00-00 00:00:00', '3', '', 'A'),
(67, 'NAISE DUARTE DE ALMEIDA', '17/04/1980', '000.000.000-00', '', 'F', '', 'INFERTILIDADE / UNIMED', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-09-27 16:20:31', '2016-09-27 16:21:33', '3', '3', 'A'),
(68, 'JOLINE PEREIRA SPINELI', '16/05/1943', '000.000.000-00', '', 'F', '', 'TROMBOCITEMIA ESSENCIAL\r\nHYDREA 02 CPS / DIA\r\nPLT: 819.000\r\nAAS\r\nALOPURINOL\r\nHYDREA 2X/DIA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-28 10:00:31', '0000-00-00 00:00:00', '3', '', 'A'),
(69, 'PAULO TOMÉ  ', '30/10/1956', '000.000.000-00', '', 'M', '', 'PTI - RESOLVIDA \r\nTEVE CHIKUNGUNYA \r\nDORES ARTICULARES \r\nEXAMES GERAIS PARA DEZEMBRO ', '(81) 9.8618-7811', '', '', 'AMIL ', '', '', '', '', '', '', '', '', NULL, NULL, '2016-09-28 10:04:55', '2016-09-28 10:06:30', '3', '3', 'A'),
(70, 'MARCOS NEVES DE QUEIROZ ', '19/05/1963', '000.000.000-00', '', 'M', '', 'LMC\r\nFASE CR?NICA\r\nGLIVEC 400 mg VO / DIA\r\nHEMOGRAMA: OK\r\nMIELOGRAMA: OK\r\nCITOGEN?TICA: REALIZADO \r\nBIOLOGIA MOLECULAR: REALIZAR NOVAMENTE ', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-09-28 10:23:37', '2016-09-28 10:36:21', '3', '3', 'A'),
(71, 'MIRIAN MARIA SILVA LIMA ', '27/04/1970', '000.000.000-00', '', 'M', '', 'TVP ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-28 10:47:12', '0000-00-00 00:00:00', '3', '', 'A'),
(72, 'MARECELA FALCÃO DOS SANTOS', '06/07/1990', '000.000.000-00', '', 'F', '', 'PTI \r\nEM USO DE CTC - 30 mg / dia ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-28 10:54:45', '0000-00-00 00:00:00', '3', '', 'A'),
(73, 'DIVANILDO JOSÉ', '01/01/2010', '000.000.000-00', '', 'M', '', 'SMD / DHC', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-28 11:17:38', '0000-00-00 00:00:00', '3', '', 'A'),
(74, 'ARNALDO CATALDI ', '23/12/1955', '000.000.000-00', '', 'M', '', 'FERRITINA ELEVADA\r\nPEÇO CINÉTICA DO FERRO E USG DE FIGADO E VIAS BILIAREES', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-28 11:21:15', '0000-00-00 00:00:00', '3', '', 'A'),
(75, 'MARLI CAETANA DE LIMA ', '04/12/1959', '000.000.000-00', '', 'F', '', 'ANEMIA FERROPRIVA\r\nINCENTIVAR GINECOLOGISTA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-28 11:32:06', '0000-00-00 00:00:00', '3', '', 'A'),
(76, 'MARIA ANGELA FERREIRA ', '28/10/1964', '000.000.000-00', '', 'F', '', 'ANVESTIGAÇÃO CLINICA\r\nANEMIA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-28 11:36:03', '0000-00-00 00:00:00', '3', '', 'A'),
(77, 'HEITOR CHALEGA DE MENEZES', '19/12/2013', '000.000.000-00', '', 'M', '', 'PAI 33 ANOS / MÃE 26 ANOS\r\nADNPM / PTOSE PALPEBRAL EM OLHO E \r\nFISIOTERAPIA 2X/SEMANA\r\nTO 1X\r\nFONOAUDIOLOGIA 1X\r\nHIDROTERAPIA 2X\r\n\r\nCARIÓTIPO 46,XY\r\nCGH ARRAY  - DELEÇÃO CROMOSSOMO 17q 21.31 - Síndrome de Koolen de Vries \r\n\r\n\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-28 11:51:51', '0000-00-00 00:00:00', '3', '', 'A'),
(78, 'MARTHA VANESSA', '13/04/1983', '000.000.000-00', '', 'F', '', 'ABORTOS DE REPETIÇÃO  /  ANDERSON ESPOSO\r\nCASAL NÃO CONSANGUÍNEO \r\nTEM A MÃE COM HISTORIA DE ABORTO E TIA MATERNA\r\nIRMÃ DO ANDERSON COM HISTORIA DE ABORTO\r\nPEÇO EXAMES DA TROMBOFILIA', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-28 12:20:49', '0000-00-00 00:00:00', '3', '', 'A'),
(79, 'ILKA CLEA DE PAULA ', '19/05/1984', '000.000.000-00', '', 'F', '', 'PERDA GESTACIONAL AOS 08 MESES\r\nDURANTE HDA  - ESOFAGO ', '', '', '', 'ASSITENTE FINANCEIRA', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-28 16:06:54', '0000-00-00 00:00:00', '3', '', 'A'),
(80, 'URACIRA CORDEIRO ', '01/01/2016', '000.000.000-00', '', 'F', '', 'MIELOGRAMA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-28 16:07:28', '0000-00-00 00:00:00', '3', '', 'A'),
(81, 'CLEIDE MARIA ', '01/01/2016', '000.000.000-00', '', 'F', '', 'TVP \r\nMTHFR\r\nPAI 1 ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-28 16:08:00', '0000-00-00 00:00:00', '3', '', 'A'),
(82, 'SARA MONTEIRO ', '03/03/2016', '000.000.000-00', '', 'F', '', '47,XX + MAR ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-28 16:08:40', '0000-00-00 00:00:00', '3', '', 'A'),
(83, 'IRINALVA MARIA DA SILVA', '28/10/1969', '000.000.000-00', '', 'F', '', 'MIELOMA MULTIPLO / PRÉ TMO AUTÓLOGO ', '', '', '', '', '', '', '', '', '', 'CASA AMARELA ', '', 'Brasil', NULL, NULL, '2016-09-28 16:23:46', '0000-00-00 00:00:00', '3', '', 'A'),
(84, 'CARLA GABRIELA PORTAL PERES ', '13/03/1985', '000.000.000-00', '', 'F', '', '- INVESTIGAÇÃO DE TROMBOFILIA FAMILIAR \r\n- SAF ?\r\n- PAI 4G/5G \r\n- COLESTEROL ALTO\r\nVAI ENGRAVIDAR / O QUE FAZER \r\n\r\n\r\n', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-09-28 16:43:00', '2016-10-17 17:33:23', '3', '3', 'A'),
(85, 'ANDERSON DANIEL BARBOSA DE ARAUJO ', '11/10/1980', '000.000.000-00', '', 'M', '', 'TVP - EM MSD - VEIA AXILAR E SUB - CLÁVIA D\r\nHÁ CERCA DE 15 DIAS ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-09-28 16:59:28', '0000-00-00 00:00:00', '3', '', 'A'),
(86, 'CAIO WLADEMIR DE BARROS AIRES ', '20/07/2001', '000.000.000-00', '', 'M', '', 'PRESENÇA DE LINFONODOMEGALIAS EM REGIÃO CERVICAL / MMSS E INGUINAL \r\nSEM FEBRE\r\nEXAMES DE SANGUE E USG DE ABDOME NORMAL \r\nPEÇO SOROLOGIAS E TAC TX / ABDOMINO / PÉLVICO ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-03 09:47:42', '0000-00-00 00:00:00', '3', '', 'A'),
(87, 'MARIA DE LOURDES SOARES DE MENEZES ', '10/11/1958', '000.000.000-00', '', 'F', '', 'MANCHAS PELO CORPO, ESCURAS, COMO HEMATOMA, SEM TRAUMAS.\r\nEM USO DE COMBIRON.\r\nINVESTIGAÇÃO DE FERRITINA ELEVADA.\r\nHAS + / ABLOK 50 mg \r\nREALIZA DIETA SOB ORIENTAÇÃO DE NUTRICIONISTA\r\nPEÇO COAGULOGRAMA E FERRITINA \r\nSUSPENDER O COMBIRON \r\nFAZER ECOCARDIOGRAMA / TEM BRADICARDIA FC: 56 bpm ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-03 10:10:21', '0000-00-00 00:00:00', '3', '', 'A'),
(88, 'HELIENE MARIANO BONIFÁCIO ', '31/10/1985', '000.000.000-00', '', 'F', '', 'PACIENTE VEM AO SERVIÇO PARA AVALIAÇÃO DE MENSTRUÇÃO POR FLUXO INTENSO.\r\nTEM HEMATOMAS PELO CORPO COM FREQUENCIAS.\r\nNEGA CASOS FAMILIARES; PAIS NÃO CONSAGUINEOS.\r\nMÃE COM CANCER DE TIREOIDE / CA MAMA\r\nTIA MATERNA FALECIDA DE CA DE MAMA\r\nNÃO TEM FILHOS.\r\nEM USO DE SERTRALINA / REPOSIÇÃO DE VITAMINA D  ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-03 10:39:53', '0000-00-00 00:00:00', '3', '', 'A'),
(89, 'VALDEREZ PESSOA DE ARÚJO BARRETO ', '12/03/1962', '000.000.000-00', '', 'F', '', 'DM + \r\nSTENT 2\r\nDORES NA CABEÇA COM SINCOPE E LIPOTÍMIA\r\n- INVESTIGAR TVP \r\n- CASOS FAMILIARES DE TVP \r\nIRMÃO COM AVCi\r\nIRMÃO COM AVCi\r\nTRABALHA COMO SECRETÁRIA \r\n', '', '', '', 'HAPVIDA ', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-03 10:50:10', '0000-00-00 00:00:00', '3', '', 'A'),
(90, 'THAIS SPINELI DE LIMA ASANO ', '25/06/1983', '000.000.000-00', '', 'F', '', 'VEM AO SERVIÇO PARA AVALIAÇÃO DE SMN1', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-03 11:00:07', '0000-00-00 00:00:00', '3', '', 'A'),
(91, 'GLAUBER HEITOR DE SOUZA ', '17/01/1980', '081.152.157-59', 'glauber.hs@gmail.com ', 'M', '', 'BRCA 1 / BRCA 2 ', '(81) 9.8500-1234', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-10-03 11:20:36', '2016-10-03 11:38:10', '3', '3', 'A'),
(92, 'GEOVANNA TARGINO DE ARAUJO ', '16/07/1999', '000.000.000-0_', '', 'F', '', 'PROTOCOLO 300 801 ', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-10-03 12:33:20', '2016-10-03 12:43:54', '3', '3', 'A'),
(93, 'GABRIEL TARGINOD DE ARAUJO ', '20/06/2007', '000.000.000-00', '', 'M', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-03 12:33:45', '0000-00-00 00:00:00', '3', '', 'A'),
(94, 'NEILSON ABDON DE ANDRADE', '05/02/1944', '065.348.564-68', '', 'M', '', 'MIELOGRAMA\r\nANEMIA A/E ', '(81) 9.8645-3166', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-03 15:25:54', '0000-00-00 00:00:00', '3', '', 'A'),
(95, 'MARIA JOSÉ DE OLIMPIO ', '01/12/1949', '000.000.000-00', '', 'F', '', 'TVP ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-03 15:27:33', '0000-00-00 00:00:00', '3', '', 'A'),
(96, 'ADRIANO LIBERALINO DA SILVA ', '16/12/1985', '000.000.000-00', '', 'M', '', 'IMUNIDADE BAIXA \r\nPEDIR USG DE ABDOME / MIELOGRAMA\r\nEM PRÓXIMA CONSULTA FAZER EXAMES DO COLÁGENO ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-03 15:39:48', '0000-00-00 00:00:00', '3', '', 'A'),
(97, 'HOZANA JOSÉ DOS SANTOS COSTA', '28/02/1967', '042.675.514-64', '', 'F', '', 'Pericia médica\r\nUso de Talidomida VO durante a gravidez da genitora\r\nMal formação de MSE \r\nFALTA RX DE MÃOS\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-03 15:57:56', '0000-00-00 00:00:00', '3', '', 'A'),
(98, 'MICHELLE CARDOSO', '19/06/1985', '000.000.000-00', '', 'F', '', 'AJUSTAR GRAVIDEZ / PARA USO DE CLEXANE\r\n\r\nTROMBOFILIA  \r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-03 16:25:45', '0000-00-00 00:00:00', '3', '', 'A'),
(99, 'EULÁLIA SANTOS DE ARAÚJO ', '08/09/1958', '000.000.000-00', '', 'F', '', 'TVP \r\n4 EPISÓDIOS \r\n-\r\nFAZER TESTES PARA TROMBOFILIA\r\nAVC / IAM - FAMILIAR', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-03 16:28:29', '0000-00-00 00:00:00', '3', '', 'A'),
(100, 'MARIA DA CONCEIÇÃO DE OLIVEIRA ', '06/12/1973', '950.393.924-00', '', 'F', '', 'EVOLUI BEM \r\nCHECK UP \r\nANEMIA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-03 16:39:10', '0000-00-00 00:00:00', '3', '', 'A'),
(101, 'DARLENE SOUZA DE OLIVEIRA ', '22/01/1988', '065.138.074-09', '', 'F', '', 'APRESENTOU TRÊS ABORTOS ESPONTÂNEOS\r\nTEM ESTUDO PRÉVIO \r\nCOMPLETO EXAMES \r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-03 16:53:16', '0000-00-00 00:00:00', '3', '', 'A'),
(102, 'IRINALVA MARIA DA SILVA ', '28/10/1969', '659.637.174-34', '', 'F', '', 'MIELOMA MÚLTIPLO PARA REREME ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-03 17:01:59', '0000-00-00 00:00:00', '3', '', 'A'),
(103, 'RILARY MASCARENHAS GABRIEL DOS SANTOS ', '23/07/2009', '000.000.000-00', '', 'F', '', 'TDAH\r\nSINDROME DE X FRAGIL  NEGATIVO\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-03 17:21:09', '0000-00-00 00:00:00', '3', '', 'A'),
(104, 'JOSENITA VICENTE PEREIRA ', '24/08/1958', '000.000.000-00', '', 'F', '', 'EXAMES PARA PLAQUETOPENIA \r\nREVER CASO', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-10-04 09:51:46', '2016-10-04 10:04:20', '3', '3', 'A'),
(105, 'GLORIA LIMA A LUNA', '15/03/1971', '000.000.000-00', '', 'F', '', 'TROMBOSE / INVESTIGAÇÃO DE TROMBOFILIA\r\nTEM HISTÓRICO FAMILIAR\r\nCITA ABORTO ESPONTANEO - 2014\r\nHAS -\r\nDM - \r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-04 10:13:45', '0000-00-00 00:00:00', '3', '', 'A'),
(106, 'CREMILDA T S LIMA ', '06/07/1957', '000.000.000-00', '', 'F', '', 'MIELOGRAMA \r\nANEMIA MEGALOBLÁSTICA\r\nHISTÓRICO DE LINFOMA - SMD?', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-10-04 10:30:32', '2016-10-04 10:43:23', '3', '3', 'A'),
(107, 'MARIA DALVA DOS SANTOS ', '27/02/1945', '000.000.000-00', '', 'F', '', 'FRAQUEZA DE MMII. \r\nTONTURAS EVENTUAIS.\r\nANEMIA FERROPRIVA, ESTAVA EM USO DE NORIPURUM.\r\nHAS - \r\nDM + EM USO DE GLUCOFORMIN E DIAMICRON \r\nPEÇO EXAMES PARA AVALIAÇÃO DA CINÉTICA DO FERRO', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-04 10:54:20', '0000-00-00 00:00:00', '3', '', 'A'),
(108, 'EMILAYNE IVIA RODRIGUES DE ASSIS DE MELO ', '02/08/1992', '000.000.000-00', '', 'F', '', 'LEVE ANEMIA\r\nPEÇO - ELETROFORESE DE Hb', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-04 10:58:42', '0000-00-00 00:00:00', '3', '', 'A'),
(109, 'ALEXSANDRA SERRA ', '03/05/1971', '000.000.000-00', '', 'F', '', 'PTI \r\nCTC 30 mg - 04.10 / 2016\r\nCTC 20 mg - 16/10\r\n---------------------------\r\nDIA 18.10:\r\nCTC 15mg / dia \r\n---------------------------\r\nexames ', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-10-04 11:03:57', '2016-10-18 12:22:31', '3', '3', 'A'),
(110, 'ANNIELE  FERRER DA SILVA BRAGA ', '07/10/1982', '000.000.000-00', '', 'F', '', 'TROMBOFILIA / TVP \r\nEM USO DE PROGESTERONA E VITAMINAS\r\nEDEMA DE MMII \r\n\r\nEXAME MTHFR - 1 HETEROZIGOTO\r\n\r\nPARTO EM DEZEMBRO ', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-10-04 11:28:14', '2016-10-04 11:31:57', '3', '3', 'A'),
(111, 'RENAN SANTANA COELHO DE LIMA ', '10/12/2009', '000.000.000-00', '', 'M', '', 'TEA\r\nEM USO DE RISPERIDONA 0,5  / 0 / 1,5 \r\nEM TERAPIAS NO NOVO RUMO / CEFOPE E CIS\r\nTO / FONO / PSICOMOTRICIDADE / PSICOLOGA / PSICOPEDAGOGA\r\n----------------------------------------------------------------------------------------------------\r\nPEDIR CGH ARRAY \r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-04 15:07:08', '0000-00-00 00:00:00', '3', '', 'A'),
(112, 'MONICA CRISTINA DE SANTANA BATISTA', '21/05/1981', '000.000.000-00', '', 'F', '', 'GENITORA DE RENAN\r\nPAI E MAE COM 35 ANOS\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-04 15:10:48', '0000-00-00 00:00:00', '3', '', 'A'),
(113, 'MIGUEL JOSÉ SANTOS ', '24/03/2015', '000.000.000-00', '', 'M', '', 'CRANIOSSINOSTOSE / DISMORFIAS FACIAIS / ADNPM\r\nCARIÓTIPO NORMAL INVERSÃO 9\r\nPEÇO CGH ARRAY ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-04 16:12:28', '0000-00-00 00:00:00', '3', '', 'A'),
(114, 'JOANA PAULA PEREIRA E SILVA ', '15/06/1988', '000.000.000-00', '', 'F', '', 'TEVE FILHA COM MAL FORMAÇÃO E FALECIDA 55 HORAS APOS NASCIMENTO PREMATURO - 6 MESES E MEIO\r\nNEGA CONSANGUINIDADE\r\nPEÇO CGH ARRAY DO MARIDO QUE TEVE INVERSÃO DO 9 ', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-10-04 16:31:12', '2016-10-04 16:42:16', '3', '3', 'A'),
(115, 'FERNANDO ANTONIO DA SILVA ', '20/06/1988', '000.000.000-00', '', 'M', '', 'TEM CARIÓTIPO 46, XY , inv (9) (p12q13)\r\nTEVE FILHA COM MALFORMAÇÕES SEVERAS\r\nPEÇO CGH ARRAY ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-04 16:40:13', '0000-00-00 00:00:00', '3', '', 'A'),
(116, 'CÉLIA MARIA VIEIRA DE SOUZA ', '12/09/1960', '000.000.000-00', '', 'F', '', 'LINFOMA NÃO HODGKIN \r\nCD 20 + \r\nSEM FEBRE \r\nMELHOR DO QUADRO CLINICO\r\nRCHOP\r\nCICLOS 5 \r\nPESO 42Kg\r\nDIURESE +\r\nAVALIAÇÃO CARDIOLÓGICA / HAS / ATENOLOL ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-04 17:15:19', '0000-00-00 00:00:00', '3', '', 'A'),
(117, 'SELMA FRANCISCA P NAPOLEAO', '05/04/1960', '000.000.000-00', '', 'F', '', 'CURSA COM ANEMIA\r\nCHECAR EXAMES ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-10 09:59:56', '0000-00-00 00:00:00', '3', '', 'A'),
(118, 'LENILDA SANTIAGO DO MONTE', '29/09/1972', '000.000.000-00', '', 'F', '', 'TROMBOSE - 02 EPIS?DIOS\r\nMAREVAN \r\nANTIDEPRESSIVO\r\n', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-10-10 10:04:01', '2016-10-10 10:04:21', '3', '3', 'A'),
(119, 'MANUEL DE ANDRADE LIMA', '13/02/2015', '000.000.000-00', '', 'M', '', 'PREMATURO \r\nPRIMEIRO FILHO DO CASAL\r\nADELIA NEUROLOGISTA INVESTIGA MICROCEFALIA\r\nCARIÓTIPO 46,XY\r\nPAI 34 ANOS\r\nMAE 34 ANOS\r\nPRE NATAL COMPLETO \r\nPREMATURIDADE - 7 MESES / ARTERIA UMBILICAL / MEDICADA COM AAS\r\nRM NORMAL \r\nPC MENOR \r\nSOLICITO CGH ARRAY ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-10 10:19:56', '0000-00-00 00:00:00', '3', '', 'A'),
(120, 'DANIELA DE MELO CUNHA ', '20/07/1982', '000.000.000-00', '', 'F', '', 'LEUCOPENIA \r\nINVESTIGAÇÃO SOROLOGIA / LES / MIELOGRAMA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-10 10:54:18', '0000-00-00 00:00:00', '3', '', 'A'),
(121, 'MARIA CLARA LEMOINE ', '04/06/1958', '000.000.000-00', '', 'F', '', 'REPOSIÇÃO HORMONAL \r\nABORTO 1X NÃO ESCLARECIDO\r\nINVESTIGAR TROMBOFILIA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-10 11:01:13', '0000-00-00 00:00:00', '3', '', 'A'),
(122, 'LINDOIA BONFIM ', '01/01/1940', '000.000.000-00', '', 'F', '', 'SMD?\r\nMIELOGRAMA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-10 11:24:26', '0000-00-00 00:00:00', '3', '', 'A'),
(123, 'PAULA DE ARRUDA PEREIRA ', '21/04/1978', '000.000.000-00', '', 'F', '', 'GRAVIDA  / 26 SEMANAS\r\nCLEXANE \r\nACIDO FÓLICO', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-10 14:43:57', '0000-00-00 00:00:00', '3', '', 'A'),
(124, 'JOÃO JOSE CANDIDO', '05/12/1934', '000.000.000-00', '', 'M', '', 'FERRITINA ELEVADA > 1000 \r\nSANGRIA TERAPEUTICA EM 30/09/2016\r\nTEM HAS\r\nCOMBODART E SINERGIM', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-10 15:00:49', '0000-00-00 00:00:00', '3', '', 'A'),
(125, 'WAGNER RUBIS COSTA ', '20/04/1983', '000.000.000-00', '', 'M', '', '- FRAQUEZA MUSCULAR / INTOLERANCIA AO FRIO \r\n- A  MÃE TEM SINTOMAS MAIS LEVES \r\n- DOENÇA DE THOMSEN\r\nPESQUISA DO GENE CLCN1', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-10 15:23:39', '0000-00-00 00:00:00', '3', '', 'A'),
(126, 'NAARA SOUZA DE OLIVEIRA MELO', '11/10/1982', '000.000.000-00', '', 'F', '', 'PACIENTE COM HISTÓRIA DE ABORTOS ESPONTANEOS.\r\nDUAS VEZES\r\nJUNHO 2015 / JULHO 2016\r\nCASAL NÃO CONSAGUÍNEO\r\nTÊM UMA FILHA DE 09 ANOS', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-10 16:23:56', '0000-00-00 00:00:00', '3', '', 'A'),
(127, 'TEREZA CRISTINA ', '14/07/1965', '000.000.000-00', '', 'F', '', 'TVP ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-10 16:31:06', '0000-00-00 00:00:00', '3', '', 'A'),
(128, 'NEIDE MARIA DE FREITAS ', '31/12/1958', '000.000.000-00', '', 'F', '', '58 ANOS\r\nHAS / COLESTEROL ALTO\r\nDOMÉSTICA\r\nPTI ???', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-11 09:56:27', '0000-00-00 00:00:00', '3', '', 'A'),
(129, 'MARCELA FALCAO DOS SANTOS', '06/07/1990', '000.000.000-00', '', 'F', '', 'PTI REFRAT?RIA\r\nCTC 25 mg / dia \r\n20 + 5 \r\n---------------------------------------\r\nEXAME PLT DIA 15.10.2016: \r\n82.000\r\nEM USO DE ANTI - ALERGICO E MULTIGRIPE\r\nPE?O RAIO X DE T?RAX / SEIOS DE FACE\r\n\r\n---------------------------------------------------------\r\nBAIXAR DOSE PARA 20 mg /dia\r\nSOLICITO EXAMES PARA 31.10 EVER DOSAGEM ', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-10-11 10:12:07', '2016-10-18 10:26:28', '3', '3', 'A'),
(130, 'MARIA DE LOURDES ARAUJO CORREIA ', '30/06/1964', '000.000.000-00', '', 'F', '', 'HAS - \r\nDM - \r\nAPRESENTOU CANCER URETER / RIM E 2009 \r\nAPRESENTOU CANCER BEXIGA 2010 \r\nAPRESENTOU 2013 - FILHO - THIAGO / 25 ANOS - TEVE TUMOR DE LYNCH - CIRURGIA E QUIMIOTERAPIA\r\nPAI COM CA COLON\r\nTEM 9 IRMÃOS\r\nIRMÃO ANTONIO CARLOS COM CA DE COLON\r\nIRMÃO ROBERTO COM CA DE COLON E JOSÉ ROBERTO - CA DE COLON (FILHO - SOBRINHO)\r\nIRMÃ MARIA ANALIA - HISTERECTOMIA - ENDOMÉTRIO / UMA DAS FILHAS COM LINFOMA HODGKIN \r\nTIO PATERNO COM CA DE COLON\r\nAVÓ PATERNA COM CANCER (N SABE) \r\n \r\n ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-11 10:23:15', '0000-00-00 00:00:00', '3', '', 'A'),
(131, 'NATAN PORTILHO ', '01/11/1970', '000.000.000-00', '', 'M', '', 'DORES MUSCULARES / ARTICULARES  - MM II E BACIA \r\nLMC PÓS TMO ALOGENICO\r\nSEM FEBRE\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-11 11:10:28', '0000-00-00 00:00:00', '3', '', 'A'),
(132, 'CRISTINA MARIA DO NASCIMENTO VIANA ', '08/01/1947', '000.000.000-00', '', 'F', '', 'TVP 2 X ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-11 11:35:39', '0000-00-00 00:00:00', '3', '', 'A'),
(133, 'ARTHUR MAGALHÃES VIANA DE MENDONÇA CANUTO', '10/12/2012', '000.000.000-0_', '', 'M', '', 'PACIENTE CURSA COM ADNPM \r\nNEUROLOGISTA - REALIZOU CGH ARRAY / EXOMA\r\nPARTO CESAREANA\r\nPAI / MAE - N?O COSNSAGUINEO\r\nPEÇO EXAME PARA DE VIVO - GENE ESPECIFICO \r\nSOB TERAPIA\r\nFONO / FISIOTERAPIA E TO ', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-10-11 11:39:38', '2016-10-11 11:40:54', '3', '3', 'A'),
(134, 'GLAUCIKELLY DOS SANTAOS ', '03/08/1989', '000.000.000-00', '', 'F', '', 'TVP ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-11 12:24:53', '0000-00-00 00:00:00', '3', '', 'A'),
(135, 'ELISANGELA CARDOSO DOS SANTOS', '23/04/1989', '000.000.000-00', '', 'F', '', 'MIELOGRAMA \r\nPTI?', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-11 15:18:51', '0000-00-00 00:00:00', '3', '', 'A'),
(136, 'MARLENE COUTO AZEVEDO ', '19/05/1943', '000.000.000-00', '', 'F', '', 'HAS+  / DM+\r\nANEMIA HEMOLITICA AUTO IMUNE EM USO DE CORTICOIDE\r\nBICITOPENIA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-11 15:29:42', '0000-00-00 00:00:00', '3', '', 'A'),
(137, 'ANA REGINA CARNEIRO DE LUCENA', '19/09/1958', '000.000.000-00', '', 'F', '', 'HAS -\r\nDM - \r\nEM USO DE PATOPRAZOL / RGE \r\nMICROCALCIFICAÇÃO NO SEIO.\r\nTEM HISTÓRICO FAMILIAR - MÃE FALECIDA / TIA PATERNA FALECIDA / PRIMA TAMBÉM COM HISTÓRICO DE CANCER DE MAMA\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-11 16:42:38', '0000-00-00 00:00:00', '3', '', 'A'),
(138, 'KATIA REGINA PEREIRA DE ASSIS', '01/01/1990', '000.000.000-00', '', 'F', '', 'HAS -\r\nDM - \r\nPERDAS GESTACIONAIS 2015 / 2016 ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-11 16:58:44', '0000-00-00 00:00:00', '3', '', 'A'),
(139, 'GABRIEL RENNAN ZUMBA CADIZ', '09/05/2002', '000.000.000-00', '', 'M', '', '14 ANOS \r\nNONO ANO \r\nFILHO DE CASAL NÃO CONSANGUINEO \r\nTEM IRMÃ ALEXANDRA, 06 ANOS\r\nSEM CRISES CONVULSIVAS \r\nTAC / EEG / RM\r\nENMG\r\nFLUOXETINA \r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-11 17:17:48', '0000-00-00 00:00:00', '3', '', 'A'),
(140, 'FRANCILENE DA SILVA SOUZA', '04/05/1989', '000.000.000-00', '', 'F', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-11 17:30:05', '0000-00-00 00:00:00', '3', '', 'A'),
(141, 'SUELY EDNA LIMA DO NASCIMENTO', '05/07/1961', '000.000.000-00', '', 'F', '', 'SMD?\r\nBIOPSIA MEDULAR: HIPOPLASIA MEDULAR\r\nCARIÓTIPO NORMAL\r\nMIELOGRAMA - NORMAL \r\nSANGRAMENTO NASAL E ORAL', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-17 10:29:19', '0000-00-00 00:00:00', '3', '', 'A'),
(142, 'NIELSON BARBOSA', '20/01/1965', '000.000.000-00', '', 'M', '', 'MOSTRAR EXAMES \r\nTEM MIELOGRAMA NORMAL \r\nUSG TEM ESPLENOMEGALIA \r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-17 10:51:24', '0000-00-00 00:00:00', '3', '', 'A'),
(143, 'JOSÉ LINDMILSON DA SILVA', '05/03/1965', '000.000.000-00', '', 'M', '', 'EXAMES PARA VIROSE:\r\n- CAXUMBA IgG +\r\n- MONONUCLEOSE: - ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-17 10:58:41', '0000-00-00 00:00:00', '3', '', 'A'),
(144, 'CLEONICE NUNES DE MELO', '19/02/1964', '000.000.000-00', '', 'F', '', 'LEUCOPENIA \r\nTRABALHA COM PRODUTOS GERAIS E EXPOSIÇÃO A PRODUTOS DE GERAIS. ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-17 11:18:04', '0000-00-00 00:00:00', '3', '', 'A'),
(145, 'MARIA DA CONCEIÇÃO SILVA TORRES ', '31/08/1959', '000.000.000-00', '', 'F', '', 'QUINTO EVENTO DE TROMBOSE VENOSA PROFUNDA - MIE - ÚLTIMO EPISÓDIO EM JUNHO / 2016\r\nEM USO DE MAREVAN 01 CP / DIA - 05 mg\r\nHAS + LOSARTANA  \r\nDM -\r\n\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-17 11:23:08', '0000-00-00 00:00:00', '3', '', 'A'),
(146, 'MARIA DANIELA SILVA TORRES', '07/10/1985', '000.000.000-00', '', 'F', '', 'INVESTIGAÇÃO DE TVP \r\nPROTEINA S FUNCIONAL / 28.0 ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-17 11:32:44', '0000-00-00 00:00:00', '3', '', 'A'),
(147, 'DIEGO DE PAULA ', '10/05/1987', '000.000.000-00', '', 'M', '', 'SINDROME DE DOWN\r\nEXAMES MOSTRAR \r\nEM USO DE VITAMINA VO \r\nEXAMES GERAIS (HEMOGRAMA / BIOQUIMICA ) ADEQUADOS\r\nLENTES CORRETIVAS\r\nSEM QUEIXAS PULMONARES / CARDIOLÓGICAS / SEM QUEIXAS DIGESTIVAS\r\nNEGA DORES OSTEO - ARTICULARES\r\nDORME BEM', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-17 11:49:11', '0000-00-00 00:00:00', '3', '', 'A'),
(148, 'JULIA CAVALCANTI DANTAS ', '04/06/2016', '000.000.000-00', '', 'F', '', 'PACIENTE EVOLUINDO BEM, REALIZANDO FISOTERAPIA E FONOAUDIOLOGIA.\r\nRECEBEU ALTA DO RHP EM 30.08 / 2016.\r\nPÉ TORTO CONGENITO - CIRURGIA EM BREVE.\r\nOFTALMO E OTORRINO EM ACOMPANHAMENTO.\r\nSINDROME DE MOEBIUS. \r\nFALTA REALIZAR ENMG DE FACE.\r\n\r\n\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-17 11:59:41', '0000-00-00 00:00:00', '3', '', 'A'),
(149, 'NATALÍCIA MARIA BARBOSA EVANGELISTA ', '24/12/1948', '000.000.000-00', '', 'F', '', 'LEUCOPENIA\r\nHAS + \r\nDM - ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-17 12:18:16', '0000-00-00 00:00:00', '3', '', 'A'),
(150, 'PATRICIA KARLA GOMES', '13/02/1977', '000.000.000-00', '', 'F', '', 'GRAVIDEZ EM 2015 - 28 SEMANAS / PRIMEIRO FILHO\r\nNASCIDO MORTO\r\nMARIDO COM 47 ANOS\r\nNÃO FEZ EXAMES DE GENÉTICA MÉDICA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-17 12:33:13', '0000-00-00 00:00:00', '3', '', 'A'),
(151, 'JOSÉ ANDERSON SCHWAB', '22/05/1959', '000.000.000-00', '', 'M', '', 'MIELOGRAMA\r\nDIFICULDADE DE ANDAR / HT\r\nSEM MEDICAÇÕES ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-17 12:39:14', '0000-00-00 00:00:00', '3', '', 'A'),
(152, 'REJANE MARIA', '13/03/1975', '000.000.000-00', '', 'F', '', 'TRAZENDO EXAMES \r\nCLINICOS DE ROTINA / HEMOGRAMA NORMAL\r\nACIDO FÓLICO BAIXO\r\nHDA GASTRITE ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-17 15:41:29', '0000-00-00 00:00:00', '3', '', 'A'),
(153, 'CLOVIS DA SILVA BANDEIRA ', '28/03/1950', '000.000.000-00', '', 'M', '', 'PTI ?\r\nTEM HEPATITE C EM TTO  CONSERVADOR - AGUARDA BX HEPATICA \r\nBANHOS DE RIO / PAULO AFONSO - BA \r\nHAS + \r\nDM  - ', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-10-17 16:09:43', '2016-10-17 16:18:07', '3', '3', 'A'),
(154, 'JACK MEIRE SILVA DO NASCIMENTO', '12/11/1980', '000.000.000-00', '', 'F', '', 'INFERTILIDADE\r\nCARIÓTIPO 46,XX\r\nTROBOFILIA A INVESTIGAR', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-17 16:38:40', '0000-00-00 00:00:00', '3', '', 'A'),
(155, 'JOSÉ ROBERTO SANTOS MARQUES ', '29/05/1969', '000.000.000-00', '', 'M', '', 'LEUCOPENIA E EOSINOFILIA\r\nTEM RINITE ALERGICA / PARASITOSE\r\nFALTA INVESTIGAR SOROLOGIAS', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-17 16:55:31', '0000-00-00 00:00:00', '3', '', 'A'),
(156, 'ADILSON SANTIAGO ', '23/07/1970', '000.000.000-00', '', 'M', '', 'DIABETES MELLITUS TIPO II\r\nTEM HISTORIA FAMILIAR DE FERRITINA ALTA\r\nTEM ASCENDENCIA PORTUGUESA\r\nEM USO DE GLIFAGE / TRAIENTA 1X/DIA - CEDO ', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-10-17 17:05:29', '2016-10-17 17:14:01', '3', '3', 'A'),
(157, 'EMILLY CRISTINA DA SILVA  ', '19/05/1990', '000.000.000-00', '', 'F', '', '- GRAVIDA 5 MESES / SEGUNDA GRAVIDEZ - PRIMEIRO TEM 05 ANOS\r\n- PTI? / 100 . 000 / mm3 \r\n- PRE - NATAL COMPLETO \r\n -EM USO DE MATERNA / ACIDO FÓLICO\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-17 17:29:53', '0000-00-00 00:00:00', '3', '', 'A'),
(158, 'MARIA CAROLINE NICEIAS BORGES ', '09/04/1992', '000.000.000-00', '', 'F', '', '- NÃO FAZ USO DE MEDICAÇÃO\r\n- FEZ USO DE NEUTROFER / MANTEM DEFICIT DE FERRO\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-17 18:06:29', '0000-00-00 00:00:00', '3', '', 'A'),
(159, 'CARLOS ROBERTO VIANA DA SILVA ', '17/05/1952', '000.000.000-00', '', 'M', '', 'AVALIAÇÃO CLÍNICO GERAL, SEM ALTERAÇÕES.', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-18 10:18:39', '0000-00-00 00:00:00', '3', '', 'A');
INSERT INTO `paciente` (`pac_id`, `pac_nome`, `pac_nasc`, `pac_cpf`, `pac_email`, `pac_sexo`, `pac_img`, `pac_obs`, `pac_tel_celular`, `pac_tel_residencia`, `pac_tel_trabalho`, `pac_tel_ramal`, `pac_end_cep`, `pac_end_endereco`, `pac_end_numero`, `pac_end_complemento`, `pac_end_cidade`, `pac_end_bairo`, `pac_end_estado`, `pac_end_pais`, `pac_id_convenio`, `pac_id_plano`, `pac_dt_add`, `pac_dt_alt`, `pac_user_add`, `pac_user_alt`, `pac_status`) VALUES
(160, 'NATHALI DE OLIVEIRA COSTA ', '05/08/1988', '000.000.000-00', '', 'F', '', 'INVESTIGAÇÃO PRÉ - CONCEPCIONAL\r\nCASAL NÃO CONSANGUINEO \r\nMARIDO: TEM DISMORFIAS + BAIXA ESTATURA / SILVER RUSSEL \r\nTHIAGO ALMEIDA FERREIRA \r\n--------\r\nPEÇO CARIÓTIPO DO CASAL\r\n-------\r\nDEPOIS PEDIR CGH ARRAY DE THIAGO \r\n------\r\nAVALIAÇÃO NO GENE  - SERGIO PENA \r\n', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-10-18 11:06:30', '2016-10-18 11:35:12', '3', '3', 'A'),
(161, 'ELITA JULIANA FERNANDES BRAGA', '13/07/1977', '000.000.000-00', '', 'F', '', 'EM USO DE LOSARTANA E EUTHYROX\r\nHAS +\r\nDM +', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-18 11:36:23', '0000-00-00 00:00:00', '3', '', 'A'),
(162, 'FLAVIO OLIVEIRA de MEDEIROS', '02/04/1984', '000.000.000-00', '', 'M', '', 'LMC - DESDE JUNHO / 2016\r\nGLIVEC - 400 mg / dia (Sul América)\r\nTeste quantitativo por PCR\r\nbcr/abl', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-18 11:45:52', '0000-00-00 00:00:00', '3', '', 'A'),
(163, 'JAMERSSON FERNANDO FERREIRA DE MELO ', '09/01/1983', '000.000.000-00', '', 'M', '', 'TROMBOCITEMIA ESSENCIAL \r\nMIELOGRAMA \r\nEM USO DE AAS / ALOPURINOL\r\nHAS +\r\nDM - ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-18 12:00:15', '0000-00-00 00:00:00', '3', '', 'A'),
(164, 'NANCI DA SILVA FERREIRA ', '03/02/1955', '000.000.000-00', '', 'M', '', 'MIELOMA MULTIPLO\r\nEXAMES\r\nEM USO DE ZOMETA  \r\nAGUARDA TAMO ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-18 12:16:11', '0000-00-00 00:00:00', '3', '', 'A'),
(165, 'KAREN MERIDIANA RODRIGUES DA SILVA ', '25/07/1992', '000.000.000-00', '', 'F', '', 'ANEMIA \r\nANTICORPO ANTI MICROSSOMAL \r\nENCAMINHAR A ENDOCRINOLOGISTA', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-18 12:31:53', '0000-00-00 00:00:00', '3', '', 'A'),
(166, 'QUEREN APUK RODRIGUES DA SILVA ', '06/06/1970', '000.000.000-00', '', 'F', '', 'HAS + ATENOLOL 50 mg /dia\r\nDM + GLIFAGE 01 A NOITE', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-18 12:33:00', '0000-00-00 00:00:00', '3', '', 'A'),
(167, 'BETANIA MARIA CORDEIRO', '30/09/1967', '000.000.000-00', '', 'F', '', 'CHECAR EXAMES \r\nCONSULTA COM GASTRO / NUTICIONISTA / MAMOGRAFIA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-18 12:42:30', '0000-00-00 00:00:00', '3', '', 'A'),
(168, 'HENRIQUE AMORIN REIS RODRIGUES ', '18/05/2005', '000.000.000-00', '', 'M', '', 'EM USO DE: -\r\nID: RABDOMIOL?SE \r\nTEM RMN NORMAL / ENMG NORMAL \r\nFILHO DE PAIS CONSANGUINEOS \r\n', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-10-18 13:34:34', '2016-10-18 13:39:22', '3', '3', 'A'),
(169, 'MARCOS VINICIUS JOSÉ CARDOSO DE MELO', '10/04/1997', '000.000.000-00', '', 'M', '', 'SINDROME GILBERT \r\nTESTE GENÉTICA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-18 16:18:45', '0000-00-00 00:00:00', '3', '', 'A'),
(170, 'EMANUEL RODRIGUES DE ALMEIDA ', '15/06/2016', '000.000.000-00', '', 'M', '', 'FILHO DE CASAL N?O CONSANGUINEO\r\nM?E 23 ANOS / PAI 24 ANOS \r\nPR? NATAL COMPLETO / TEVE DPP PRIMEIRO TRIMESTRE / PARTO PREMATURA AMEA?A - 28 SEMANAS\r\nHEMODI?LISE IRC - DESDE OS 21 ANOS\r\nEM FASE PR? TX RENAL \r\nPARTO CESAREANA - 35 SEMANAS \r\nCRANIOESTONE  TURRICEFALIA/ FENDA PALATINA ELEVADA PALATO OGIVAL / PROPTOSE OCULAR / EST: 47 CM / PC: 32 CM / APGAR 6-8\r\nCARI?TIPO \r\n46,XY - NORMAL \r\nTESTE DO PEZINHO NORMAL\r\nECOCARDIOGRAMA NORMAL\r\nHIDROCEFALIA -\r\nID: SINDROME CROUZON  / PFEIFER \r\n', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-10-18 16:34:51', '2016-10-18 16:49:30', '3', '3', 'A'),
(171, 'MARILANDA CAMPOS ', '16/05/1958', '000.000.000-00', '', 'F', '', 'PTI \r\nITR?\r\nUSA CTC / PREDNISONA 5 mg em dias alternados\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-18 17:10:55', '0000-00-00 00:00:00', '3', '', 'A'),
(172, 'MARIA IONARA FERREIRA SILVA', '05/08/1984', '000.000.000-00', '', 'F', '', 'TRÊS ABORTOS DE REPETIÇÃO\r\nEM ESTUDO PARA TROMBOFILIA COM ÉRIKA \r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-18 17:22:53', '0000-00-00 00:00:00', '3', '', 'A'),
(173, 'MAURICEIA SOUZA DOS SANTOS', '11/10/1927', '000.000.000-00', '', 'F', '', 'EM USO DE NORIPURUN MASTIGAVEL\r\nPEÇO EXAMES CONTROLES \r\n', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-10-18 17:44:04', '2016-10-18 17:48:06', '3', '3', 'A'),
(174, 'FABRÍCIO MAGALHÃES RIBEIRO ', '12/11/1999', '000.000.000-00', '', 'M', '', 'LEUCOPENIA\r\nMIELOGRAMA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-18 17:51:50', '0000-00-00 00:00:00', '3', '', 'A'),
(175, 'ZENAIDE EMERENCIANO BRANDÃO', '07/06/1932', '000.000.000-00', '', 'F', '', 'MIELOGRAMA \r\nPOR CONTA DA LEUCOPENIA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-18 18:09:12', '0000-00-00 00:00:00', '3', '', 'A'),
(176, 'SEBASTIANA SANTINA DA SILVA', '16/05/1953', '000.000.000-00', '', 'F', '', 'MIELOGRAMA COM FERRO MEDULAR \r\nPLAQUETOPENIA\r\nSEM EXAMES ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-20 10:58:14', '0000-00-00 00:00:00', '3', '', 'A'),
(177, 'CLEODON BARBOSA DE LIMA ', '09/11/1957', '000.000.000-00', '', 'M', '', 'PANCITOPENIA\r\n- SMD ?\r\n- ESQUISTOSSOMOSE ?\r\n- CALAZAR ?\r\n\r\nFAZER GRANULOKINE\r\nMANTENHO BACTRIM E ACICLOVIR\r\nREPITO SOROLOGIAS / SOROLOGIAS CALAZAR\r\nLAUDO INSS\r\nPEDIR VOUCHER GENOMIKA\r\nPEDIR USG DE ABDOME\r\n\r\nEM NOVEMBRO AGENDAR HEMOPE ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-10-20 11:20:43', '0000-00-00 00:00:00', '3', '', 'A'),
(178, 'ARTHUR VINICIUS DA SILVA', '14/10/2010', '000.000.000-00', '', 'M', '', 'AUTISMO \r\nPEÇO CGH ARRAY', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-09 10:09:37', '0000-00-00 00:00:00', '3', '', 'A'),
(179, 'CESARIO APRIGIO DE FARIAS FILHO', '26/01/1945', '000.000.000-00', '', 'M', '', 'GASTRITE  / ANEMIA LEVE\r\n- ENCAMINHO AO GASTRO ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-09 10:45:01', '0000-00-00 00:00:00', '3', '', 'A'),
(180, 'SEVERINA JERONIMO DE OLIVEIRA ', '26/11/1959', '000.000.000-00', '', 'F', '', 'CANCER DE MAMA AOS 39 ANOS\r\nTEM UMA IRMÃ COM CA DE MAMA (42 ANOS) - LUCIA JERONIMO \r\nPAIS NÃO CONSANGUINEOS\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-09 10:54:45', '0000-00-00 00:00:00', '3', '', 'A'),
(181, 'CIRLENE ALVES MEIRELLES ', '07/06/1973', '000.000.000-00', '', 'F', '', 'TVP \r\nANSIEDADE \r\nDISCUTIR DIU\r\nDISCUTIR PSIQUIATRIA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-09 11:21:04', '0000-00-00 00:00:00', '3', '', 'A'),
(182, 'LUCAS CAUÃ DE SOUZA SÁ ', '07/05/2013', '000.000.000-00', '', 'M', '', 'AUTISMO\r\n- NEGA CRISES CONVULSIVAS\r\n- PRE NATAL COMPLETO / PARTO CESAREANO / 40 SEMANAS\r\n\r\n- PAIS NÃO CONSANGUINEOS (PAI 24 ANOS / MAE 25 ANOS)\r\n- ADNPM \r\n- ATRASO DE LINGUAGEM / NÃO É AGRESSIVO\r\n- COME E DORME BEM\r\n- ESTRABISMO CONVERGENTE ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-09 11:25:45', '0000-00-00 00:00:00', '3', '', 'A'),
(183, 'VALDENIO EMIDIO MOTA ', '22/03/1963', '000.000.000-00', '', 'M', '', 'C 282 Y NEGATIVO\r\nPEÇO NOVOS EXAMES ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-09 11:42:45', '0000-00-00 00:00:00', '3', '', 'A'),
(184, 'GILVADETE QUEIROZ ', '21/11/1969', '000.000.000-00', '', 'F', '', 'CANCER FAMILIAR ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-09 11:50:53', '0000-00-00 00:00:00', '3', '', 'A'),
(185, 'JAQUELINE PESSOA LOURENÇO', '06/12/1968', '000.000.000-00', '', 'F', '', 'EXAMES CLINICOS NORMAIS ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-09 12:01:26', '0000-00-00 00:00:00', '3', '', 'A'),
(186, 'pedro lourenço dos santos neto ', '15/01/1959', '000.000.000-00', '', 'M', '', 'FERRITINA ELEVADA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-09 12:14:21', '0000-00-00 00:00:00', '3', '', 'A'),
(187, 'PAULIVAN FERREIRA SOARES ', '21/09/1961', '000.000.000-00', '', 'M', '', 'EXAMES \r\nFERRITINA ELEVADA ', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '2016-11-09 12:17:28', '2016-11-09 12:31:55', '3', '3', 'A'),
(188, 'JOSELMA MARTA DE SANTANA ', '28/12/1979', '000.000.000-00', '', 'F', '', 'ABORTOS DE REPETIÇÃO\r\n46,XX\r\nCHECAR MEIA ELASTICA \r\nCHECAR MARIDO COM CARIOTIPO ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-09 12:37:33', '0000-00-00 00:00:00', '3', '', 'A'),
(189, 'HELENA VITORIA SANTOS NOGUEIRA DE HOLANDA ', '08/03/2013', '000.000.000-00', '', 'F', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-09 13:29:01', '0000-00-00 00:00:00', '3', '', 'A'),
(190, 'ANA PAULA CRISTINA DOS ANJOS', '04/07/1975', '000.000.000-00', '', 'F', '', 'CANCER DE MAMA\r\nE METASTASE CEREBRAL                                                                                                                                                                                                                                                                                                                                         \r\nANEMIA / PEÇO ELETROFORESE DE Hb', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-09 16:37:33', '0000-00-00 00:00:00', '3', '', 'A'),
(191, 'CELLI APARECIDA PONTES DE BRITTES SANTOS ', '18/08/1974', '000.000.000-00', '', 'F', '', 'ANEMIA A/E \r\nHB: 9,2 / VCM NORMAL \r\nPEDIDO CINETICA DO FERRO / VIT B12 E ACIDO FOLICO', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-09 17:43:22', '0000-00-00 00:00:00', '3', '', 'A'),
(192, 'MARIA LAURA CARNEIRO DE SOUZA ', '03/04/1987', '000.000.000-00', '', 'F', '', 'HISTÓRICO FAMILIAR DE CANCER DE INTESTINO E MAMA', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-09 17:45:51', '0000-00-00 00:00:00', '3', '', 'A'),
(193, 'CREMILDA TAVARES SATIRO DE LIMA ', '06/07/1957', '000.000.000-00', '', 'F', '', 'HEMATO / ROTINA ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-21 15:02:57', '0000-00-00 00:00:00', '3', '', 'A'),
(194, 'VALDEREZ LYRA CASTRO E SILVA', '01/10/1955', '000.000.000-00', '', 'F', '', 'MÃE COM CA DE MAMA\r\nPAI COM HISTÓRICO DE MESTASTASE SEM SABER TU PRIMÁRIO\r\nTEM 3 IRMÃOS SAUDÁVEIS \r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-21 15:06:00', '0000-00-00 00:00:00', '3', '', 'A'),
(195, 'WALDETE MARIA ALVES CRUZ', '29/04/1956', '000.000.000-00', '', 'F', '', 'DISTROFIA MUSCULAR A/E\r\nTEM PAIS CONSANGUINEOS\r\nDISTROFIA MUSCULAR DAS CINTURAS\r\nTEM DUAS IRMÃS COM O MESMO PROBLEMAS MUSCULARES', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-21 16:14:31', '0000-00-00 00:00:00', '3', '', 'A'),
(196, 'GUSTAVO VILELA BARRETO', '17/11/2013', '000.000.000-00', '', 'M', '', '3 ANOS\r\nPACIENTE CURSANDO COM 30 KILOS E TEM 1,03 / SOBREPESO\r\nPRADDER WILLI \r\nHIPERATIVO\r\nCASAL NÃO CONSANGUINEO ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-21 16:16:45', '0000-00-00 00:00:00', '3', '', 'A'),
(197, 'MARCIELY BEZERRA DA SILVA ', '23/05/1982', '000.000.000-00', '', 'F', '', 'DOIS ABORTOS DE REPETIÇÃO\r\nCURSA COM NATIMORTO \r\nESTUDO DO CONCEPTO\r\n----\r\nREVISAR TROMBOFILIA HEREDITÁRIA', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-23 11:24:40', '0000-00-00 00:00:00', '3', '', 'A'),
(198, 'LARA CORREIRA ARAUJO ', '25/06/2013', '000.000.000-00', '', 'F', '', 'PACIENTE CURSA COM EPILEPSIAS / AGRESSIVIDADE.\r\nSAF ?\r\nARISTAB / DEPAKENE\r\nCARIOTIPO NORMAL 46,XX, 9qh+\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', NULL, NULL, '2016-11-24 10:46:12', '0000-00-00 00:00:00', '3', '', 'A'),
(199, 'MATHEUS AUGUSTO ', '10/04/2014', '', '', 'M', '', 'DEFICIENCIA DE G6PD\r\nHIPERAGITAÇÃO', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2016-12-05 12:34:46', '0000-00-00 00:00:00', '3', '', 'A'),
(200, 'TALITA ALVES DE SOUZA OLIVEIRA', '02/12/1996', '', '', 'M', '', 'PORFIRIA \r\nREALIZANDO INFUSÕES NO HC\r\nPRÓXIMAS INFUSÕES 19, 20, 21 E 22/12\r\nPRESCREVO DIMORF', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2016-12-06 10:27:54', '0000-00-00 00:00:00', '3', '', 'A'),
(201, 'ANNA CLARA MELO DA SILVA', '19/10/2007', '', '', 'M', '', 'CURSA COM ADNPM \r\nTEM MAIS UMA FILHA\r\nNÃO CONSAGUÍNEO\r\nSEM CASOS SIMILARES NA FAMÍLIA \r\nEM USO DE AMATO 50 mg x2\r\nEM USO DE FRISIUN 1X/DIA\r\nVITMAINA D 05 GOTAS\r\nSULFATO FERROSO NO ALMOÇO - 08 GOTAS \r\nTEA\r\nDÇ DEGENERATIA \r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2016-12-14 10:46:04', '0000-00-00 00:00:00', '3', '', 'A'),
(202, 'KAMILA BARBOSA FERREIRA ', '17/02/1989', '', '', 'F', '', 'PACIENTE CURSANDO COM CA DE MAMA E DIAGNOSTICADO EM NOVEMBRO 2015 / USG - JULHO 2016 \r\nHISTORIA FAMILIAR NEGATIVA\r\nQUIMIOTERAPIA 7X\r\nRADIOT - \r\nRETIRADA DE NÓDULOS\r\nMAMA E\r\n-----------\r\nNÓDULO MAMA D\r\n---------- \r\nPAIS NÃO CONSANGUINEOS\r\nKLEBER 39ANOS\r\nKLEA 36ANOS\r\n---------', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2016-12-14 11:06:28', '0000-00-00 00:00:00', '3', '', 'A'),
(203, 'Adson MORAIS SIQUEIRA ', '01/01/2009', '', '', 'M', '', '- TEA\r\n- NEGA CRISES CONVULSIVAS\r\nEM USO RISPERIDONA E VIVANCE\r\nTERAPIAS FONO / TO / PSICOPEDAGOGA\r\nEEG E RNM ENCEFALO NORMAL \r\nTEM ATRASO DE LINGUAGEM\r\nINTERAÇÃO DIFICIL\r\nPAIS NÃO CONSANGUINEO\r\n- PAI TEM 02 FILHOS\r\n- MÃE, 43 ANOS, COM HIST DE ABORTO PROVOCADO\r\n- PAI, 61 ANOS, SAUDÁVEL', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2016-12-14 17:02:58', '2016-12-14 17:04:09', '3', '3', 'A'),
(204, 'EMILY VICTORIA DA SILVA ', '03/03/2006', '', '', 'F', '', 'PAIS CONSANGUINEOS\r\nMÃE 47 ANOS\r\nPAI 47 ANOS\r\nTEM IRMÃ DE 08 ANOS SAUDÁVEL\r\nTEA\r\nMOVIMENTOS DE REPETIÇÃO\r\n- EM USO DE RISPERIDON\r\n1mg 12/12h\r\n\r\nNEGA CRISES CONVULSIVAS\r\nEM TERAPIAS \r\nFISOTERAPIA MOTORA\r\nNÃO CONSEGUE FONO / TO \r\n\r\n- FAZER CARIOTIPO\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2016-12-14 17:30:56', '0000-00-00 00:00:00', '3', '', 'A'),
(205, 'MARIA JESUINA DE O0LIVEIRA BEZERRA ', '10/09/1961', '', '', 'F', '', 'TVP POR USO DE ACO ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2016-12-21 09:47:35', '0000-00-00 00:00:00', '', '', 'A'),
(206, 'MARIA JESUINA DE O0LIVEIRA BEZERRA ', '10/09/1961', '', '', 'F', '', 'TVP POR USO DE ACO ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2016-12-21 09:47:58', '0000-00-00 00:00:00', '', '', 'A'),
(207, 'MARIA JESUINA DE O0LIVEIRA BEZERRA ', '10/09/1961', '000.000.000-00', '', 'F', '', 'TVP POR USO DE ACO EM 2014', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2016-12-21 09:48:30', '0000-00-00 00:00:00', '', '', 'A'),
(208, 'MARTHA DE MELO PAES BARRETO ', '04/10/1983', '', '', 'F', '', 'CASAL 33 E 37 ANOS \r\nABORTOS DE REPETIÇÃO - MAIO E N0VEMBRO / 2016\r\nNÃO INVESTIGADO, NEM TROMBOFILIA OU GENÉTICA', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-09 16:25:39', '0000-00-00 00:00:00', '3', '', 'A'),
(209, 'MARIA ANTONIETTA SILVA GALVÃO', '17/04/1937', '', '', 'F', '', 'CANCER DE MAMA EM INVESTIGAÇÃO\r\n\r\nMAMOGRAFIA / USG DE MAMA', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-09 16:53:30', '0000-00-00 00:00:00', '3', '', 'A'),
(210, 'ROSANGELA  DE MELO CABRAL ', '05/10/1970', '', '', 'F', '', 'HISTORICO FAMILIAR DE CANCER, ESPECIALMENTE CANCER DE MAMA.\r\nFILHA DE CASAL NÃO CONSANGUINEO.\r\nTEM MÃE E IRMÃ COM CANCER DE MAMA.\r\nTEM PRIMA MATERNA E PATERNA COM CA DE MAMA.', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2017-01-09 17:19:46', '2017-01-09 17:20:31', '3', '3', 'A'),
(211, 'MARCOS VINICIUS MEDEIROS NASCIMENTO', '21/10/2015', '', '', 'M', '', 'CRANIOSINOSTOSE / LARINGOMALACIA / DISTROFIA MUSCULAR (DRA VANESSA)\r\nPEDIDO DE SEQUENCIAMENTO COMPLETO DO EXOMA', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-11 15:44:37', '0000-00-00 00:00:00', '3', '', 'A'),
(212, 'CELSON RICARDO DA SILVA SOUZA', '29/04/1982', '', '', 'M', '', 'TEVE FILHO COM ANENCEFALIA', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-11 16:06:31', '0000-00-00 00:00:00', '3', '', 'A'),
(213, 'ISABEL CRISTINA FAUSTINO DE LIRA DA SILVA', '21/04/1988', '', '', 'M', '', 'TEM TRAÇO DE FALCIFORME E ESTÁ GRAVIDA DO SEGUNDO FILHO. ESTÁ COM 20 SEMANAS E 06 DIAS.\r\nTEM UM FILHO DE 04 ANOS.\r\nMARIDO NÃO TEM.\r\nTEM IRMAOS POR PARTE DE PAI COM A DOENÇA.', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-11 16:17:11', '0000-00-00 00:00:00', '3', '', 'A'),
(214, 'KAROLINA KNEIP DE SÁ', '26/09/1988', '', '', 'M', '', 'LINFOMA DE HODGKIN AOS 17 ANOS\r\nTRATADA NO OSWALDO CRUZ\r\nVEM PARA SEGUIMENTO\r\nTRAZER PET SCAN ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-11 16:54:16', '0000-00-00 00:00:00', '3', '', 'A'),
(215, 'IZAURA MUNIZ AZEVEDO', '13/05/1973', '', '', 'F', '', 'PACIENTE REFERE TRÊS FAMILIARES COM HISTÓRIA DE CANCER DE MAMA.\r\nAVÓ MATERNA E DUAS TIAS MATERNAS.\r\nFAZ ANUALMENTE MAMOGRAFIA E USG.\r\nTEM MAMA DENSA COM CALCIFICAÇÕES DEVERSAS DE CARATER BENIGNO.', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-11 17:00:43', '0000-00-00 00:00:00', '3', '', 'A'),
(216, 'FRANCELINO PEDRO SILVA SENA ANDRADE ', '02/06/1978', '', '', 'M', '', 'CASO DE ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-17 10:39:37', '0000-00-00 00:00:00', '3', '', 'A'),
(217, 'ANA PAULA GOMES GONÇALVES DA CUNHA ', '25/10/1984', '', '', 'F', '', 'TROMBOFILIA POS GRAVIDEZ \r\nEM USO DE CLEXANE ATÉ 06 SEMANAS APÓS GRAVIDEZ \r\nPESO 4Kg\r\nEST: 49 CM\r\nAMAMENTAÇÃO +\r\nPARTO CESAREANA SEM INTERCORRENCIAS / ALTA COM 48 HORAS\r\nVASCO LUCENA\r\nEM USO DE CLEXANE E NIMESULIDA 02X/DIA\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-17 11:31:26', '0000-00-00 00:00:00', '3', '', 'A'),
(218, 'CYBELLE GREICE RAMOS GALVÃO DE FARIAS ', '06/06/1982', '', '', 'F', '', 'DRA TEREZA CRISTINA\r\n- PERGUNTA POR QUE O CASAL NÃO ENGRAVIDA?\r\nPEDIR CARIÓTIPO DO CASAL \r\nDEPOIS DEL Y ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-17 18:10:33', '0000-00-00 00:00:00', '3', '', 'A'),
(219, 'VALÉRIA FONTES DE LIMA ', '03/04/1983', '', '', 'F', '', 'VEM DE MÉDICA DE FERTILIDADE, COM HISTÓRIA DE ABORTOS - 06 X , INCLUSIVE EM VIGÊNCIA.\r\nSEM FILHOS.\r\nCASAL INVESTIGADO COMO UM TODO.\r\nMARIDO PEÇO MICRODELAÇÃO DO CROMOSSOMO Y\r\nESPOSA PEÇO PESQUISA DE X-FRAGIL', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-18 10:19:37', '0000-00-00 00:00:00', '3', '', 'A'),
(220, 'ANDREIA ABREU DE CARVALHO', '14/03/1963', '', '', 'F', '', 'BRCA 1 / BRCA 2 ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-18 18:11:12', '0000-00-00 00:00:00', '3', '', 'A'),
(221, 'ADRIANA ABREU DE CARVALHO', '27/10/1961', '', '', 'F', '', 'BRCA 1 / BRCA 2', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-18 18:18:20', '0000-00-00 00:00:00', '3', '', 'A'),
(222, 'GILLIANA GOUVEIA MORAIS ', '21/05/1983', '', '', 'F', '', 'HISTORICO DE CA DE MAMA, EM GENITORA. \r\nMOROU EM BELO JARDIN - PE / BATERIAS MOURA.  \r\nTEM IRMÃ, GABRIELLY, 32 ANOS, SAUDÁVEL.', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-23 15:53:56', '0000-00-00 00:00:00', '3', '', 'A'),
(223, 'ROSILENE RODRIGUES DOS SANTOS', '07/07/1961', '', '', 'F', '', 'FEZ EM NOVEMBRO EXAMES POR CONTA DE DENGUE / ARBOVIROSE\r\nCURSOU COM PLAQUETOPENIA / LEUCOPENIA \r\nPEDIDO MIELOGRAMA E HEMOGRAMA MARCELO AMGALHAES / MARCIO MELO\r\nPEDIR SOROLOGIAS E DOENÇAS DO COLAGENO ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2017-01-24 12:05:40', '2017-01-24 12:12:05', '3', '3', 'A'),
(224, 'MARIA DE FATIMA ALVES ', '10/02/1954', '', '', 'F', '', 'MIELOGRAMA / IMF SEM ALTERAÇÕES ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-24 12:21:38', '0000-00-00 00:00:00', '3', '', 'A'),
(225, 'ADRIANA MATOS PESSOA', '21/08/1974', '', '', 'F', '', 'ANEMIA FERROPRIVA POR PERDA EXCESSIVA MENSTRAUÇÃO\r\nEM PROXIMA OPORTUNIDADE FAREI ELETROFORESE DE HEMOGLOBINA', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2017-01-24 16:14:12', '2017-01-24 16:21:32', '3', '3', 'A'),
(226, 'JULIA MORAIS CERON', '29/06/1997', '', '', 'F', '', 'ANEMIA DESDE A INFANCIA\r\nPAIS NÃO CONSANGUINEOS\r\nTEM UMA IRMA DE 22 ANOS (BARBARA)\r\nTOPIRAMATO HÁ 02 ANOS ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-24 16:32:13', '0000-00-00 00:00:00', '3', '', 'A'),
(227, 'MAGNO RAFAEL MARQUES AIRES FERREIRA', '21/06/2016', '', '', 'M', '', 'SINDROME DE DOWN \r\nTRISSOMIA LIVRE DO 21 \r\nPROTOCOLO DE EXAMES \r\nCARDIO OK\r\nORL OK\r\nOFTALMO OK \r\nPEDIATRA OK ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-24 17:04:02', '0000-00-00 00:00:00', '3', '', 'A'),
(228, 'GABRIEL AQUILES DE ALMEIDA ', '10/02/2006', '', '', 'M', '', 'MÁ FORMAÇÃO EM CÉREBRO ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-24 18:01:15', '0000-00-00 00:00:00', '3', '', 'A'),
(229, 'LUCAS MARQUES CORREIA DA CRUZ  ', '30/06/2010', '', '', 'M', '', 'EM USO DE: HOMEOPATIA\r\nIDADE DO CASAL\r\n- PAI: 37 ANOS\r\n- MÃE: 44 ANOS\r\nTEM UMA MEIA IRMÃ - 27 ANOS - STEPHANIE.\r\nSEM CARIÓTIPO\r\nSEM CGH ARRAY \r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-24 18:39:38', '0000-00-00 00:00:00', '3', '', 'A'),
(230, 'MARIANA SOPHIA RIBEIRO DE PAULA', '26/01/2014', '', '', 'F', '', 'PACIENTE CURSANDO COM ANEMIA DESDE O NASCIMENTO.\r\nSEM RESOLUÇÃO. \r\nTEM FAMILIAR COM ANEMIA FALCIFORME.\r\nPEÇO ELETROFORESE DE Hb\r\nPROXIMA CONSULTA FAREI EXAMES GERAIS ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2017-01-24 18:59:13', '2017-01-24 19:04:32', '3', '3', 'A'),
(231, 'ROBSON CAVALCANTI NEGROMENTE', '13/09/1972', '', '', 'M', '', 'MIELOGRAMA\r\nNEUROPENIA PERSISTENTE\r\nHEMOGRAMA:\r\n- 840 NEUTRÓFILOS \r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-25 11:05:13', '0000-00-00 00:00:00', '3', '', 'A'),
(232, 'LUCIANA BASSAN DA SILVA ', '06/11/1988', '', '', 'F', '', 'FURUNCULOSE DE REPETIÇÃO DESDE NOVEMBRO / 2017 \r\nPREDOMINANTEMENTE EM NÁDEGAS E AXILAS; JOELHOS E COXAS', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-25 11:38:21', '0000-00-00 00:00:00', '3', '', 'A'),
(233, 'ARTHUR ALMEIDA REGIS', '16/11/2016', '', '', 'M', '', 'NASCEU DE PARTO PREMATURO, 34 SEMANAS.\r\nUTI POR DUAS SEMANAS.  \r\nSUSPEIÇÃO DE SINDROME DE DOWN.\r\nEM USO DE: VITAMINA D E NORIPURUN\r\nEXAMES GERAIS NORMAIS\r\nCARIÓTIPO: +21\r\nDADOS POSITIVOS:\r\n- NUCA COM PELE EM SOBRA\r\n- FP OBLIQUA\r\n- PREGA UNICA PALMAR\r\n- HIPOTONIA\r\n- LINGUA PROTUSA \r\nIDADE PAI: 33 a / MÃE: 31 a\r\nPRIMEIRO FILHO DO CASAL\r\nESTIMULAÇÃO?\r\nPEÇO CARIOTIPO DO CASAL\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-25 11:55:22', '0000-00-00 00:00:00', '3', '', 'A'),
(234, 'YTALAINE DA SILVA CARVALHO', '09/09/1995', '', '', 'M', '', 'CURSA COM ANEMIA, JÁ AMPLAMENTE ESTUDADA, SEM ETIOLOGIA CLARA.\r\nPESO: 39 Kg\r\nSENTE DISMENORREIA IMPORTANTE.\r\nESTÁ COM GINECOLOGISTA / FEZ USG, SEM RESULTADO - COBRAR\r\nNEGA DOENÇA FALCIFORME OU TALASSEMIA NA FAMILIA.', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-25 16:06:56', '0000-00-00 00:00:00', '3', '', 'A'),
(235, 'ALEXANDRE MARCOS LIMA SANTOS ', '29/10/2002', '', '', 'M', '', 'LONGILINEO, MÃOS E PÉS DELGADOS E LONGOS, MIOPE, VALVA MITRAL NO CORAÇÃO.\r\nSEM ALTERAÇÕES NA AORTA.\r\nALEXANDRO 08 MESES, IRMÃO SAUDÁVEL.', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-25 16:51:42', '0000-00-00 00:00:00', '3', '', 'A'),
(236, 'MANOEL EZEQUIEL BELTRAO DE LIMA ', '01/01/1950', '', '', 'M', '', 'POLICITEMIA VERA \r\nHYDREA E ALOPURINOL E AAS ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-26 11:22:48', '0000-00-00 00:00:00', '3', '', 'A'),
(237, 'VALERIA ALVES SILVA ', '26/08/1959', '', '', 'F', '', 'ALTERAÇÕES DE DHL / CPK / CK ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-26 11:23:34', '0000-00-00 00:00:00', '3', '', 'A'),
(238, 'ERASMO BENEDITO FONSECA CAMPELO DOS SANTOS ', '10/07/1937', '', '', 'M', '', '- LINFOMA CUTANEO\r\nPÓS 02 BLOCOS DE 08 CICLOS DE CHOP\r\nFEZ 01 CICLO DE FLUDARABINA\r\nFAREI MAIS 03  A 05 CICLOS DE FLUDARABINA\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-26 11:55:41', '0000-00-00 00:00:00', '3', '', 'A'),
(239, 'LUCAS GABRIEL ALIXANDRE BARBOSA', '12/12/2016', '', '', 'M', '', 'RN EM HMEMORIAL PETROLINA\r\nFAZ USO DE MEDICAÇÃO PARA TIREOIDE\r\nPAI: 40 ANOS\r\nMÃE: 45 ANOS \r\n(QUIMICA - AGUA)\r\nMÃE/PAI TEM DOIS FILHO\r\nTEM UMA FILHA NORMAL 09 ANOS - MARINA \r\nPRE NATAL COMPLETO\r\nSUSPEITA DE SINDROME DE DOWN\r\nCESAREANA\r\n\r\n\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-26 13:34:14', '0000-00-00 00:00:00', '3', '', 'A'),
(240, 'ROSEANE MARIA LINS DE ALBUQUERQUE  ', '13/08/1956', '', '', 'F', '', 'CANCER DE PERITONEO FAMILIAR\r\nA PACIENTE E MAE (NERICE FALECIDA HÁ 03 ANOS) \r\nPEÇO BRCA 2 / FEITO BRCA 1 ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-31 12:42:31', '0000-00-00 00:00:00', '3', '', 'A'),
(241, 'ROSSELINE BARBOSA ACIOLI', '20/02/1979', '', '', 'F', '', 'ANEMIA PÓS CIRURGIA DE REDUÇÃO HÁ 11 ANOS ', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-31 13:14:03', '0000-00-00 00:00:00', '3', '', 'A'),
(242, 'DIRCEU GOMES DA SILVA ', '06/11/1965', '', '', 'M', '', 'LMC EM USO DE GLIVEC\r\nAGUARDO CROMOSSOMO FILADELFIA\r\nSOLICITAR BCR ABL QUANTITATIVO\r\nCARIOTIPO 46,XY\r\nBCR / ABL 14,5%\r\n(IDEAL 1,5%)\r\n\r\nTEM BOM HEMOGRAMA\r\nHB: 12,00\r\nGB: 5.500\r\nPLT: 100.000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2017-01-31 15:54:06', '2017-01-31 15:56:30', '3', '3', 'A'),
(243, 'SILVIA ALVES BRITO BENTO FRAGA ', '18/09/1987', '', '', 'M', '', 'PACIENTE FEZ USO DE CORTICOTERAPIA POR 04 SEMANAS, TERMINANDO HÁ 02 SEMANAS.\r\nANEMIA HEMOLITICA AUTO  - IMUNE.\r\nPEÇO HEMOGRAMA EM MARCELO MAGALHÃES.', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-31 16:09:34', '0000-00-00 00:00:00', '3', '', 'A'),
(244, 'GILDA FRANCISCA DA SILVA', '09/09/1959', '', '', 'F', '', 'ESPLENOMEGALIA  /  PLAQUETOPENIA \r\nCURSA COM 57 A 70 MIL PLAQUETAS. \r\nTEM EDA COM VARIZES ESOFAGIANAS\r\nRESIDIU SÃO JOSÉ DO ITAIPU   /   PB - ESQUISTOSSOMOSE', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-31 16:22:34', '0000-00-00 00:00:00', '3', '', 'A'),
(245, 'AGNES SWAMY DE OLIVEIRA CAMPOS', '11/11/2003', '', '', 'F', '', 'SANGRAMENTO VAGINAL / AUMENTO DE FLUXO\r\nANEMIA, CHEGOU A FAZER USO DE CH 02 UNIDADES\r\nEM USO DE ANTI - CONCEPCIONAL (TAMISAN) E COMBIRON; \r\nEVOLUI UM POUCO MELHOR.\r\n\r\nCONDUTA:\r\n- MANTER ACO \r\n- COMBIRON 02 X \r\n\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-31 16:37:02', '0000-00-00 00:00:00', '3', '', 'A'),
(246, 'JOSÉ APOLONIO DE OLIVEIRA ', '04/09/1943', '', '', 'M', '', 'EM USO DE RIVOTRIL\r\nNEGA  HAS / DM \r\nVEM POR PLAQUETOPENIA 100.000 - TEM ZONA ENDEMICA DE EHE\r\nNÃO FEZ USG / EDA \r\nFEZ CIRURGIA DE VESICULA / PROSTATA E INTESTINO DELGADO', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-31 16:54:40', '0000-00-00 00:00:00', '3', '', 'A'),
(247, 'ANA CLAUDIA FRANKLIN GUERRERA ', '25/05/1971', '', '', 'F', '', 'CANCER DE MAMA EM 2012, CANCER DE OVARIO - REDUZIU COM QT.\r\nCIRURGIA HISTERECTOMIA (COM OVARIO D / E) E MANUTENÇÃO.\r\nEM 2016, HOUVE RETORNO, SANGUE - CA 125.\r\nFOI REALIZADA CIRURGIA, 2016, PARA ALCANCE DE CÚPULA VAGINAL.\r\nBRAQUITERAPIA / QT 06 CICLOS; EM USO  AVASTINA.\r\nNEGA DM / HAS\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-31 17:21:09', '0000-00-00 00:00:00', '3', '', 'A'),
(248, 'PAULO DE TARSO DE AMORIM', '29/08/1961', '', '', 'M', '', 'PACIENTE VEM AO SERVIÇO POR CONTA DE ESTEATOSE HEPATICA, COLELITIASE E CISTO RENAL.\r\nPASSADO DE SÍFILIS', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-31 17:51:25', '0000-00-00 00:00:00', '3', '', 'A'),
(249, 'FERNANDA SILVA DE MELO', '14/02/1996', '', '', 'F', '', 'EM USO DE ACO\r\nNEGA HAS / DM\r\nANEMIA / NEUTROFER VIA ORAL\r\nPÉSSIMA ALIMENTAÇÃO\r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-01-31 18:17:50', '0000-00-00 00:00:00', '3', '', 'A'),
(250, 'MATEUS VIDAL DOBBIN', '19/08/2013', '', '', 'M', '', 'EM USO DE: -\r\nCRISES CONVULSIVAS: - \r\nPARTO CESAREANO / PRÉ NATAL COMPLETO\r\nNÃO TEM IRMÃOS\r\nPAI: 39 ANOS\r\nMÃE: 30 ANOS\r\nFEZ RMN CEREBRAL E X - FRÁGIL.\r\nTUMOR BENIGNO DE MAXILA D / E', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2017-02-01 11:21:12', '2017-02-01 11:23:14', '3', '3', 'A'),
(251, 'PIRRE MARCEL LIMEIRA DOS SANTOS ', '12/08/2011', '', '', 'M', '', 'TRISSOMIA DE 21\r\nE\r\nAUTISMO\r\n- PAI: 47 ANOS\r\n- MÃE: 47 ANOS\r\n\r\nMELATONINA / PURAN T4\r\nMONTILER\r\n\r\nCEFOPE ( TERAPIAS)', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-02-01 12:09:52', '0000-00-00 00:00:00', '3', '', 'A'),
(252, 'VALLESKA DE OLIVEIRA MELO', '18/01/1991', '', '', 'F', '', 'EM AVALIAÇÃO SEMESTRAL\r\nTEM AVÓ PATERNA COM CANCER DE MAMA (91 ANOS)\r\nGENITORA COM CANCER DE MAMA (53 ANOS)\r\nA PACIENTE EM TELA TEM CISTOS E ADENOMAS.\r\nSEMPRE BENIGNOS', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-02-01 17:07:54', '0000-00-00 00:00:00', '3', '', 'A'),
(253, 'JOAQUIM HOLANDA LACERDA ', '12/05/2015', '', '', 'M', '', 'PACIENTE EM USO: -\r\nPARTO CESAREANA / MACEIO - AL\r\nPRE NATAL COMPLETO\r\nSEM CRISES CONVULSIVAS\r\nFEZ RMN - SEM ALTERAÇÕES\r\nPAI: 21 ANOS\r\nMÃE:  21 ANOS \r\nAGENESIA DE POLEGAR / INCLINAÇÃO DE PESCOÇO\r\nSEM CIRURGIAS\r\nCONSULTA COM DRA VANESSA VAN DER LIN DEN \r\n', '', '', '', '', '', '', '', '', '', '', '', 'Brasil', '', '', '2017-02-01 17:33:45', '0000-00-00 00:00:00', '3', '', 'A');

-- --------------------------------------------------------

--
-- Estrutura para tabela `plano`
--

CREATE TABLE IF NOT EXISTS `plano` (
  `plan_id` int(10) NOT NULL,
  `plan_conv_id` int(10) DEFAULT NULL,
  `plan_nome` varchar(100) DEFAULT NULL,
  `plan_status` varchar(10) DEFAULT NULL,
  `plan_dt_add` datetime DEFAULT NULL,
  `plan_dt_alt` datetime DEFAULT NULL,
  `plan_user_add` varchar(10) DEFAULT NULL,
  `plan_user_alt` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `profissional`
--

CREATE TABLE IF NOT EXISTS `profissional` (
  `prof_id` int(10) NOT NULL,
  `prof_tipo` varchar(10) NOT NULL,
  `prof_nome` varchar(100) NOT NULL,
  `prof_email` varchar(100) NOT NULL,
  `prof_sexo` varchar(1) NOT NULL,
  `prof_img` varchar(100) NOT NULL,
  `prof_senha` varchar(100) NOT NULL,
  `prof_obs` varchar(500) NOT NULL,
  `prof_tel_celular` varchar(15) NOT NULL,
  `prof_tel_residencia` varchar(15) NOT NULL,
  `prof_tel_trabalho` varchar(15) NOT NULL,
  `prof_tel_ramal` varchar(10) NOT NULL,
  `prof_saud_conselho` varchar(10) NOT NULL,
  `prof_saud_registro` varchar(10) NOT NULL,
  `prof_saud_profissao` varchar(100) NOT NULL,
  `prof_saud_cbo` varchar(100) NOT NULL,
  `prof_dt_add` datetime NOT NULL,
  `prof_dt_alt` datetime NOT NULL,
  `prof_user_add` int(10) NOT NULL,
  `prof_user_alt` int(10) NOT NULL,
  `prof_status` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Tabela de cadastro de usuarios e profissinaisde saude';

--
-- Fazendo dump de dados para tabela `profissional`
--

INSERT INTO `profissional` (`prof_id`, `prof_tipo`, `prof_nome`, `prof_email`, `prof_sexo`, `prof_img`, `prof_senha`, `prof_obs`, `prof_tel_celular`, `prof_tel_residencia`, `prof_tel_trabalho`, `prof_tel_ramal`, `prof_saud_conselho`, `prof_saud_registro`, `prof_saud_profissao`, `prof_saud_cbo`, `prof_dt_add`, `prof_dt_alt`, `prof_user_add`, `prof_user_alt`, `prof_status`) VALUES
(1, '0', 'adm', 'adm@ightc.com', '', '', 'leoneoliveira', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 'A'),
(3, '0', 'Rodrigo', 'rodrigo.florencio@ightc.com.br', 'M', '', '123', '', '', '', '', '', '', '', '', '', '2016-09-12 22:55:48', '2016-09-22 08:16:12', 0, 3, 'A');

-- --------------------------------------------------------

--
-- Estrutura para tabela `prontuario`
--

CREATE TABLE IF NOT EXISTS `prontuario` (
  `pront_id` int(11) NOT NULL,
  `pront_agd_id` int(10) NOT NULL,
  `pront_laudo` blob NOT NULL,
  `pront_prof_id` int(10) NOT NULL,
  `pront_dt_add` datetime NOT NULL,
  `pront_dt_alt` datetime NOT NULL,
  `pront_user_add` varchar(10) NOT NULL,
  `pront_user_alt` varchar(10) NOT NULL,
  `pront_status` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='Tabela de Prontuario de paciente';

--
-- Fazendo dump de dados para tabela `prontuario`
--

INSERT INTO `prontuario` (`pront_id`, `pront_agd_id`, `pront_laudo`, `pront_prof_id`, `pront_dt_add`, `pront_dt_alt`, `pront_user_add`, `pront_user_alt`, `pront_status`) VALUES
(1, 2, 0x3c703e6f6b3c2f703e, 3, '2016-09-22 07:52:36', '0000-00-00 00:00:00', '3 ', '', 'A'),
(2, 3, 0x3c703e6f6b20323c2f703e, 3, '2016-09-22 08:27:58', '0000-00-00 00:00:00', '3 ', '', 'A'),
(3, 9, 0x3c703e6f6b3c2f703e, 3, '2016-11-21 14:04:13', '0000-00-00 00:00:00', '3 ', '', 'A'),
(4, 10, 0x3c703e54414c4954413c2f703e0d0a3c703e504f5246495249413c2f703e0d0a3c703e50524553435245564f2044494d4f52463c2f703e, 3, '2016-12-06 10:28:42', '0000-00-00 00:00:00', '3 ', '', 'A'),
(5, 11, 0x3c703e54565020454d20323031343c2f703e0d0a3c703e266e6273703b3c2f703e0d0a3c703e266e6273703b3c2f703e, 3, '2016-12-21 09:49:40', '0000-00-00 00:00:00', '3 ', '', 'A');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `agendamento`
--
ALTER TABLE `agendamento`
  ADD PRIMARY KEY (`agd_id`);

--
-- Índices de tabela `convenio`
--
ALTER TABLE `convenio`
  ADD PRIMARY KEY (`conv_id`);

--
-- Índices de tabela `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`pac_id`);

--
-- Índices de tabela `plano`
--
ALTER TABLE `plano`
  ADD PRIMARY KEY (`plan_id`);

--
-- Índices de tabela `profissional`
--
ALTER TABLE `profissional`
  ADD PRIMARY KEY (`prof_id`);

--
-- Índices de tabela `prontuario`
--
ALTER TABLE `prontuario`
  ADD PRIMARY KEY (`pront_id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `agendamento`
--
ALTER TABLE `agendamento`
  MODIFY `agd_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de tabela `convenio`
--
ALTER TABLE `convenio`
  MODIFY `conv_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `paciente`
--
ALTER TABLE `paciente`
  MODIFY `pac_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=254;
--
-- AUTO_INCREMENT de tabela `plano`
--
ALTER TABLE `plano`
  MODIFY `plan_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `profissional`
--
ALTER TABLE `profissional`
  MODIFY `prof_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `prontuario`
--
ALTER TABLE `prontuario`
  MODIFY `pront_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
