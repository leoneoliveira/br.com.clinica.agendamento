<?php include 'head.php'; ?>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white " >
    <!-- BEGIN HEADER -->
    <?php include 'nav-top.php'; ?>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php include 'nav-left.php'; ?>
        <!-- END SIDEBAR -->
        <?php
        include 'head.php';
        include 'conexao/config.php';
        $query = "SELECT * FROM convenio order by 1 desc";
        $result = mysqli_query($conn, $query);
        $total_num_rows = mysqli_num_rows($result);
        function situacao($args){
            switch ($args) {
                case 'A':
                $situacao =  '<span class="label label-success">Ativo</span>';
                break;
                case 'I':
                $situacao = '<span class="label label-info">Inativo</span>';
                break;
                case 'E':
                $situacao = '<span class="label label-danger">Excluído</span>';
                break;
            }
            echo $situacao;
        }
        ?>
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE BAR -->
                <?php include 'breadcrumb.php'; ?>
                <!-- END PAGE BAR -->
                <!-- BEGIN PAGE TITLE-->
                <h3 class="page-title"> CONVÊNIOS
                </h3>
                <!-- END PAGE TITLE-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <button class="btn green" type="btn btn-success" data-toggle="modal" data-target="#cadastrar">
                                <i class="fa fa-plus-circle"></i>
                                &nbsp;ADICIONAR
                            </button>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="tb_lists">
                            <thead>
                                <tr>
                                    <th width="5%"> CÓDIGO </th>
                                    <th width="30%"> NOME </th>
                                    <th width="10%"> REGISTRO </th>
                                    <th width="10%"> CARÊNCIA </th>
                                    <th width="10%"> SITUAÇÃO </th>
                                    <th width="2%"> CONFIGURAÇÕES</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php while($row = mysqli_fetch_array($result)){ ?>
                                <tr>
                                    <td> <?php echo $row['conv_id']; ?> </td>
                                    <td><a href="c_convenio.php?conv_id=<?php echo $row['conv_id']; ?>"> <?php echo $row['conv_nome']; ?> </a></td>
                                    <td> <?php echo $row['conv_registro']; ?></td>
                                    <td> <?php echo $row['conv_carencia']; ?></td>
                                    <td> <?php situacao($row['conv_status']); ?></td>
                                    <td align="center">
                                        <!-- opções button -->
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                Opções
                                                <i class="fa fa-cog"></i>
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu" style="text-align: left;">
                                                <li data-cod      ="<?php echo $row['conv_id']; ?>"
                                                    data-nome     ="<?php echo $row['conv_nome']; ?>"
                                                    data-registro ="<?php echo $row['conv_registro']; ?>"
                                                    data-carencia ="<?php echo $row['conv_carencia']; ?>"
                                                    class="aletar_conv">
                                                    <a href="#" data-toggle="modal" data-target="#editar">
                                                        <i class="fa fa-edit icon-circle icon-info"></i>
                                                        Alterar
                                                    </a>
                                                </li>
                                                <li data-cod="<?php echo $row['conv_id']; ?>" class="inativar_conv"><a href="#"> <i class="fa fa-ban icon-circle icon-warning"></i> Inativar </a></li>
                                                <li data-cod="<?php echo $row['conv_id']; ?>" class="ativar_conv"><a href="#"> <i class="glyphicon glyphicon-ok icon-circle icon-success"></i> Ativar </a></li>
                                                <li class="divider"></li>
                                                <li data-cod="<?php echo $row['conv_id']; ?>" class="excluir_conv"><a href="#"> <i class="fa  fa-trash-o icon-circle icon-danger"></i> Excluir </a></li>
                                            </ul>
                                        </div>
                                        <!-- end opções button -->
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- Modal -->
                <div class="modal fade" id="cadastrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">
                                    ADICIONAR CONVÊNIO
                                </h4>
                            </div>
                            <div class="modal-body" align="">
                                <form action="#" class="horizontal-form" id="form_sample_1">
                                    <div class="form-group">
                                        <label class="control-label"><b>Nome</b></label>
                                        <input type="text" name="conv_nome" id="conv_nome" class="form-control" placeholder="Infome o nome do plano">
                                    </div>
                                    <div class="row">
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label"><b>Número de registro</b></label>
                                                <input type="text"  name="conv_registro" id="conv_registro" class="form-control" placeholder="Infome o numero de regristo">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label"><b>Período de carência</b></label>
                                                <input class="form-control" id="mask_date" name="conv_carencia" type="text" placeholder="__/__/____" />
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="tipoForm" value="I">
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="atualizar()">Fechar</button>
                                <button type="button" class="btn btn-success" onclick="enviarForm()">Cadastrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">
                                    ALTERAR CONVÊNIO
                                </h4>
                            </div>
                            <div class="modal-body" align="">
                                <form action="#" class="horizontal-form" id="form_sample_2">
                                    <div class="form-group">
                                        <label class="control-label"><b>Nome</b></label>
                                        <input type="text" name="conv_nome" id="conv_nomeEdt" class="form-control conv_nomeEdt" placeholder="Infome o nome do plano">
                                    </div>
                                    <div class="row">
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label"><b>Número de registro</b></label>
                                                <input type="text"  name="conv_registro" id="conv_registro" class="form-control conv_registroEdt" placeholder="Infome o numero de regristo">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label"><b>Período de carência</b></label>
                                                <input class="form-control conv_carenciaEdt" id="mask_date2" name="conv_carencia" type="text" placeholder="__/__/____" />
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="tipoForm" value="MUSER">
                                    <input type="hidden" name="conv_id" id="conv_id" value="MUSER">
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="atualizar()">Fechar</button>
                                <button type="button" class="btn btn-success" onclick="enviarForm2()">Cadastrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="mCadastrado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">
                                    ADICIONAR CONVÊNIO
                                </h4>
                            </div>
                            <div class="modal-body" align="center">
                                <h2>Cadastrado com sucesso !</h2>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="atualizar()">Fechar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                 <!-- Modal -->
                <div class="modal fade" id="mEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">
                                    ALTERAR CONVÊNIO
                                </h4>
                            </div>
                            <div class="modal-body" align="center">
                                <h2>Alterado com sucesso !</h2>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="atualizar()">Fechar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="Malterado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">
                                CONVÊNIOS
                            </h4>
                        </div>
                        <div class="modal-body" align="center">
                            <h2>Situação alterada com sucesso !</h2>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="atualizar()">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->
    <?php include 'footer.php'; ?>
    <script src="assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
    <script src="assets/apps/scripts/form-input-mask.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="js/model/convenio.js" type="text/javascript"></script>
    <script src="js/model/convenio_edt.js" type="text/javascript"></script>
    <script>
    $(document).ready(function() {
        $(".nav-item").removeClass('start active open');
        $("#m_configuracao").addClass('start active open');
        $("#m_convenio").addClass('start active open');
        $('#tb_lists').dataTable( {
            "pageLength": 10,
            "lengthChange": false,
    "order": [[ 3, "desc" ]], //ordena por coluna
    "language": {
    "url": "js/data-tables/dataModificado/Portuguese-Brasil.json" //tradução para português
},
    stateSave: true, //salvar pesquisa em tempo
    "searching": true //oculta ou mostra
});
    //inativar Prestador
    $('.inativar_conv').click(function(){
        var cod = $(this).attr('data-cod');
        var formAction = 'M';
        var status = 'I';
        console.log('inativar_conv');
        if (cod!='' && cod!=null && cod!=undefined ) {
            $.ajax({
                method: "POST",
                url: "model/convenio.php",
                data: { conv_id: cod , tipoForm: formAction, conv_status:status},
                success: function( data ) {
                    $('#Malterado').modal('show');
                },
                error: function (){
                }
            });
        }
    });
    //Ativar Prestador
    $('.ativar_conv').click(function(){
        var cod = $(this).attr('data-cod');
        var formAction = 'M';
        var status = 'A';
        console.log('ativar_conv');
        if (cod!='' && cod!=null && cod!=undefined ) {
            $.ajax({
                method: "POST",
                url: "model/convenio.php",
                data: { conv_id: cod , tipoForm: formAction, conv_status:status},
                success: function( data ) {
                    $('#Malterado').modal('show');
                },
                error: function (){
                }
            });
        }
    });
    //Excluir Prestador
    $('.excluir_conv').click(function(){
        var cod = $(this).attr('data-cod');
        var formAction = 'M';
        var status = 'E';
        console.log('excluir_conv');
        if (cod!='' && cod!=null && cod!=undefined ) {
            $.ajax({
                method: "POST",
                url: "model/convenio.php",
                data: { conv_id: cod , tipoForm: formAction, conv_status:status},
                success: function( data ) {
                    $('#Malterado').modal('show');
                },
                error: function (){
                }
            });
        }
    });
    //alterar medico
    $('.aletar_conv').click(function(){
        var cod      = $(this).attr('data-cod');
        var nome     = $(this).attr('data-nome');
        var registro = $(this).attr('data-registro');
        var carencia = $(this).attr('data-carencia');

        $('#conv_id').val(cod);
        $('.conv_nomeEdt').val(nome);
        $('.conv_registroEdt').val(registro);
        $('.conv_carenciaEdt').val(carencia);
    });
    
});
function atualizar(){
    location.reload();
}
function enviarForm(){
    $('#form_sample_1').submit();
}
function enviarForm2(){
    $('#form_sample_2').submit();
}
</script>
</body>
</html>